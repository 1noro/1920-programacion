package separarVocales;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class SepararVocales {
    
    public static final int nMaxOpt = 4;
    public static final String finAltas = "fin";
    public static final String dir = "/opt/jarchivos/";
    public static final String filenameIn = "nombres.txt";
    public static final String filenameOutV = "nombresVocal.txt";
    public static final String filenameOutC = "nombresConsonante.txt";
    
    // --- INTERFAZ DE USUARIO -------------------------------------------------

    public static void printMenu() {
        System.out.println();
        System.out.println("--- MENU ---");
        System.out.println(" 1 - Altas");
        System.out.println(" 2 - Separar por vocal");
//        System.out.println(" 3 - Listar nombres");
//        System.out.println(" 4 - Listar nombres vocales");
//        System.out.println(" 5 - Listar nombres consonantes");
        System.out.println();
        System.out.println(" 0 - Salir");
        System.out.println();
    }
    
    public static int readOpt(Scanner s, int max) {
        int out = 0;
        do {
            System.out.print("Escribe una opción: ");
            out = s.nextInt();
            s.nextLine();
        } while (out < 0 || out > max);
        return out;
    }
    
    // --- UTILIDADES ----------------------------------------------------------
    
    public static void limpiarArchivo(String fulldir) {
        try {
            //TRUE para que no sobreescriba
            FileWriter fw = new FileWriter(fulldir, false);
            fw.write("");
            fw.close();
        } catch(IOException ioe) {}
    }
    
    public static boolean esVocal(char c) {
        boolean out = false;
        c = Character.toLowerCase(c);
        if (c == 'a' ||
                c == 'e' ||
                c == 'i' ||
                c == 'o' ||
                c == 'u' ||
                c == 'á' ||
                c == 'é' ||
                c == 'í' ||
                c == 'ó' ||
                c == 'ú' ||
                c == 'ü' 
        )
        out = true;
        return out;
    }
    
    public static void saveAlta(String linea, String fullDir) {
        try {
            //TRUE para que no sobreescriba
            FileWriter fw = new FileWriter(fullDir, true);
            linea += '\n';
            fw.write(linea); 
            fw.close();
            System.out.println(" ### GUARDANDO: " + linea);
        } catch(IOException ioe) {
            System.out.println(" ### Guardado fallido");
        }
    }
    
    // --- Penasmiento ---------------------------------------------------------
    
    public static void separar(String fulldirV, String fulldirC) {
        String linea = "";
        // LEER ARCHIVO
        try {
            FileReader fr = new FileReader(dir + filenameIn);
            BufferedReader br = new BufferedReader(fr);
            linea = br.readLine();
            while (linea != null) {
                if (esVocal(linea.charAt(0))) 
                    saveAlta(linea, fulldirV);
                else
                    saveAlta(linea, fulldirC);
                linea = br.readLine();
            }
            br.close();
            fr.close();
        } catch(IOException ioe) {}
        
    }

    // --- ALTAS ---------------------------------------------------------------
    
    public static String readNombre(Scanner s) {
        String nombre = "";
        
        do {
            System.out.print(" Nombre (15 char max); '" + finAltas + "' para acabar: ");
            nombre = s.nextLine();
        } while(!nombre.equals(finAltas) && nombre.length() > 15);
        
        return nombre;
    }

    public static void altas(Scanner s, String fullDir) {
        String nombre = "";
        
        do {
            System.out.println("\n>> Nueva alta:");
            nombre = readNombre(s);
            
            if (!nombre.equals(finAltas)) {
                saveAlta(nombre, fullDir);
            }
            
        } while(!nombre.equals(finAltas));
    }
    
    // --- MAIN ----------------------------------------------------------------
    
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int opt = 0;
        
        do {
            printMenu();
            opt = readOpt(s, nMaxOpt);
            if (opt != 0) {
                switch (opt) {
                    case 1:
                        altas(s, dir + filenameIn);
                        break;
                    case 2:
                        separar(dir + filenameOutV, dir + filenameOutC);
                        break;
                }
            }
        } while (opt != 0);
        
        System.out.println("Bye (;︵;)");
        
        s.close();
        
    }

}
