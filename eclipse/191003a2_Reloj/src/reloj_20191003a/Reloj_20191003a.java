package reloj_20191003a;

import java.lang.Thread;

public class Reloj_20191003a {
	public static void main(String[] args) throws InterruptedException {
		int h = 0, m = 0, s = 0;
		for (h = 0; h < 24; h++) 
			for (m = 0; m < 60; m++) 
				for (s = 0; s < 60; s++) {
						System.out.print(h + ":" + m + ":" + s + "\n");
						Thread.sleep(1000);
				}
	}
}
