package comprobarDNI;
import java.util.Scanner;
public class ComprobarDNI {
	
	static char calcularLetra(int num) {
		String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
		return letras.charAt(num % 23);
	}
	
	static char sustraerLetra(String dni) {
		return Character.toUpperCase(dni.charAt(dni.length() - 1));
	}
	
	static int sustraerNumero(String dni) {
		return Integer.parseInt(dni.substring(0, 8));
	}
	
	static boolean compobarDNI(int num, char letra) {
		boolean out = false;
		if (calcularLetra(num) == letra) out = true;
		return out;
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String dni = "";
		
		do {
			System.out.print("Escribe un DNI a comprobar: ");
			dni = s.next(); // así me aseguro de que no hay espacios
		} while (dni.length() < 9 || dni.length() > 9);
		
		if (compobarDNI(sustraerNumero(dni), sustraerLetra(dni)))
			System.out.println("La letra es correcta.");
		else {
			System.out.println("La letra introducida no es correcta.");
			System.out.println("La correcta es '" + calcularLetra(sustraerNumero(dni)) + "'.");
		}
		s.close();
	}

}
