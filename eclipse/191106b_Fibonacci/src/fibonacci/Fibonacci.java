package fibonacci;

import java.io.IOException;
import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		Scanner e = new Scanner(System.in);
		int b = 0, n1 = 0, n2 = 0, aux = 0;
		char entrada = ' ';
		
		do {
			do {
				System.out.print("Límite superior de la secuencia de Fibonacci: ");
				b = e.nextInt();
			} while (b < 0);
			
			n1 = 0;
			n2 = 1;
			aux = 0;
			while (aux <= b) {
				System.out.print(aux + " ");
				if (aux == 1) System.out.print(aux + " ");
				aux = n1 + n2;
				n1 = n2;
				n2 = aux;
			}
			
			do {
				System.out.print("\nDesea volver a calcular con un límite diferente? (s/n): ");
				try {
					entrada = Character.toLowerCase((char) System.in.read());
					while(System.in.read()!='\n'); //leer hasta pulsar enter '\n'
				} catch (IOException ioe) {System.out.println("error1");}
			} while (entrada != 'n' && entrada != 's');
			
			System.out.print('\n');

		} while (entrada == 's');
		e.close();
	}

}
