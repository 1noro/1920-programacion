package metodosConVectores;
import java.util.InputMismatchException;
import java.util.Scanner;
public class MetodosConVectores {
	
	static void mostrarMenu() {
		System.out.println("\n MENU");
		System.out.println("=====================");
		System.out.println("1 - Llenar vector");
		System.out.println("2 - Visualizar vector");
		System.out.println("3 - Fin");
	}
	
	static int leerOpcion(Scanner s) {
		int opcion = 0; 
		do {
			try {
				System.out.print("Teclea una opción: ");
				opcion = s.nextInt();
			} catch (InputMismatchException ime) {opcion = 0; s.nextLine();}
		} while (opcion < 1 || opcion > 3);
		return opcion;
	}
	
	static int[] llenarVector(Scanner s, int v[]) {
		int i = 0;
		int vOut[] = new int[v.length];
		System.out.println();
		for (i = 0; i < v.length; i++) {
			System.out.print("Valor para la posición " + i + " :");
			vOut[i] = s.nextInt();
		}
		return vOut;
	}
	
	static void visualizarVector(int v[]) {
		int i = 0;
		System.out.print("\nv = [");
		for (i = 0; i < (v.length - 1); i++) System.out.print(v[i] + ", ");
		System.out.println(v[v.length - 1] + "]");
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int opcion = 0;
		int v[] = new int[7];
		
		do {
			mostrarMenu();
			opcion = leerOpcion(s);
			switch (opcion) {
				case 1:
					v = llenarVector(s, v);
					break;
				case 2:
					visualizarVector(v);
					break;
			}
		} while (opcion != 3);
		
		s.close();

	}

}
