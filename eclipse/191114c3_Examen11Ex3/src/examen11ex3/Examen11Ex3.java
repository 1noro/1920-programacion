package examen11ex3;

import java.util.Scanner;

public class Examen11Ex3 {

	public static void main(String[] args) {
		// medias de productos por mes ¿o era al revés?
		
		Scanner s = new Scanner(System.in);
//		int meses = 12, deleg = 4, prod = 6;
		int meses = 2, deleg = 2, prod = 3;
		int im = 0, id = 0, ip = 0;
		
		int vP = 0; //venta producto
		
		// matriz predefinifda
		int m[][][] = {
				{{23, 234, 45}, {123, 324, 46}},
				{{76, 24, 453}, {2133, 34, 91}}
		};
		
		// visualizar matriz
		for (im = 0; im < meses; im++) {
			System.out.print("\n>mes: " + im + "\n");
			for (id = 0; id < deleg; id++) {
				System.out.print("\n\t#deleg: " + id + "\n\t  ");
				for (ip = 0; ip < (prod - 1); ip++) {
					System.out.print(m[im][id][ip] + " (" + ip + "), ");
				}
				System.out.print(m[im][id][prod - 1] + " (" + ip + ")");
			}
		}
		
		for (ip = 0; ip < prod; ip++) {
			vP = 0;
			for (im = 0; im < meses; im++) {
				for (id = 0; id < deleg; id++) {
					vP += m[im][id][ip];
				}
			}
			System.out.println("Media mensual del producto producto " + ip + ": " + vP/meses);
		}
		
		s.close();

	}

}
