package comprobarDNI;
import java.util.Scanner;
public class ComprobarDNI {
	
	static char letraDNI(int n) {
		String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
		return letras.charAt(n % 23);
	}
	
	static char sustraerLetra(String dni) {
//		System.out.println(dni.charAt(8));
		return dni.charAt(8);
	}
	
	static int sustraerNumero(String dni) {
		String strnum = "";
		int i = 0;
		for (i = 0; i < 8; i++) strnum += dni.charAt(i);
		return Integer.parseInt(strnum);
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String dni = "";
		
		do {
			System.out.print("Escribe un DNI a comprobar: ");
			dni = s.nextLine();
		} while (dni.length() < 9 || dni.length() > 9);
		
		if (sustraerLetra(dni) == letraDNI(sustraerNumero(dni)))
			System.out.println("La letra es correcta.");
		else {
			System.out.println("La letra introducida no es correcta.");
			System.out.println("La correcta es '" + letraDNI(sustraerNumero(dni)) + "'.");
		}
		s.close();
	}

}
