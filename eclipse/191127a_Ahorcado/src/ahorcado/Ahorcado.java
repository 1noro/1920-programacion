package ahorcado;

import java.io.IOException;
import java.util.Scanner;

public class Ahorcado {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String palabra = "";
		int i = 0, e = 0, contF = 0, contA = 0;
		char letra = ' ', salida = ' ';
		boolean encontrado = false;
		int tam = 0;
		char aciertos[] = new char[tam];
		char fallos[] = new char[tam];
		char vpalabra[] = palabra.toCharArray();
		
		do {
			System.out.print("Palabra: ");
			palabra = entrada.nextLine();
			palabra = palabra.toLowerCase();
			
			tam = palabra.length();
			
			aciertos = new char[tam];
			fallos = new char[tam];
			
			vpalabra = palabra.toCharArray();
			
			aciertos[0] = vpalabra[0];
			fallos[0] = vpalabra[0];
			for (i = 1; i < tam; i++) {
				aciertos[i] = '_';
				fallos[i] = '_';
			}
			aciertos[tam-1] = vpalabra[tam-1];
			fallos[tam-1] = vpalabra[tam-1];
	
			contF = 1;
			contA = 1;
			while (contF < (tam - 1) && contA < (tam - 1)) {
				encontrado = false;
				
				System.out.print("Aciertos: ");
				for (i = 0; i < tam; i++)
					System.out.print(aciertos[i] + " ");
				System.out.println();
				
				System.out.print("Fallos:   ");
				for (i = 0; i < tam; i++)
					System.out.print(fallos[i] + " ");
				System.out.println();
				
				System.out.print("Letra: ");
				try {
					letra = (char) System.in.read();
					while(System.in.read()!='\n'); //leer hasta pulsar enter
				} catch (IOException ioe) {}
				letra = Character.toLowerCase(letra);
				
				for (e = 0; e < tam; e++) {
					if (letra == vpalabra[e]) {
						aciertos[e] = letra;
						encontrado = true;
						contA++;
					}
				}
				
				if (!encontrado) {
					fallos[contF] = letra;
					contF++;
				}
				
			}
			
			if (contA > contF) System.out.println("Ganaste :)");
				else System.out.println("Perdiste :(");
			System.out.println();
			
			do {
				System.out.print("Salir? (S/N): ");
				try {
					salida = (char) System.in.read();
					while(System.in.read()!='\n'); //leer hasta pulsar enter
				} catch (IOException ioe) {}
				salida = Character.toLowerCase(salida);
			} while (salida != 's' && salida != 'n');
		
		} while (salida != 's');
		
		entrada.close();

	}

}
