package cajero_20190926a;

import java.util.Scanner;
// versión emilio (mod desde 20190926a2
public class Cajero_20190926a {

	public static void main(String[] args) {
		int in = 0, tot = 0;
		Scanner e = new Scanner(System.in);
		
		do {
			System.out.print("Precio producto: ");
			in = e.nextInt();
			tot += in;
		} while (in != 0);
		
		System.out.print("Total a pagar: " + tot + "€\n");
		
		do {
			System.out.print("Precio producto: ");
			in = e.nextInt();
			if (tot > in) {
				System.out.print("Faltan: " + (tot - in) + "€\n");
			}
		} while (in < tot);

		System.out.print("Cambio: " + (in - tot) + "€\n");
		
		e.close();
	}

}
