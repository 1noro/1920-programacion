package ordenar3;

import java.util.Scanner;

public class Ordenar3 {

	public static void main(String[] args) {
		int n1 = 0, n2 = 0, n3 = 0;
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.print("Número 1: ");
		n1 = entrada.nextInt();
		System.out.print("Número 2: ");
		n2 = entrada.nextInt();
		System.out.print("Número 3: ");
		n3 = entrada.nextInt();
		
		entrada.close();
		
		if (n1 <= n2) {
			if (n2 <= n3) {
				System.out.print(n1 + ", " + n2 + ", " + n3);
			} else {
				if (n1 <= n3) {
					System.out.print(n1 + ", " + n3 + ", " + n2);
				} else {
					System.out.print(n3 + ", " + n1 + ", " + n2);
				}
			}
		} else {
			if (n1 <= n3) {
				System.out.print(n2 + ", " + n1 + ", " + n3);
			} else {
				if (n1 <= n2) {
					System.out.print(n2 + ", " + n3 + ", " + n1);
				} else {
					System.out.print(n3 + ", " + n2 + ", " + n1);
				}
			}
		}
		
	}

}
