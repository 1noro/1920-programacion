package factorialMetodo;

import java.util.Scanner;

public class FactorialMetodo {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int num = 0, i = 2;
		
		do {
			System.out.print("Numero distinto a 0: ");
			num = s.nextInt();
		} while (num == 0);
		
		i = num;
		while (i > 0) {
			if ((num % i) == 0) {
				System.out.println(num + " | " + i);
				num /= i;
			}
			i--;
		}
		
		s.close();
	}

}
