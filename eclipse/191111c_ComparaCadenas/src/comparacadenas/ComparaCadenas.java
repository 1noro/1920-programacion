package comparacadenas;

import java.util.Scanner;

public class ComparaCadenas {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String a = "", b = "";
		
		System.out.print("Frase a: ");
		a = entrada.nextLine();
		
		System.out.print("Frase b: ");
		b = entrada.nextLine();
		entrada.close();
		
		System.out.print("a.equals(b): ");
		if (a.equals(b)) System.out.println("Benne.");
			else System.out.println("Male.");
		
		// sirve para ordenar un string de menor a mayor
		System.out.print("a.compareTo(b) == 0: ");
		if (a.compareTo(b) == 0) System.out.println("Benne.");
			else System.out.println("Male.");
		
		System.out.print("a.equalsIgnoreCase(b): ");
		if (a.equalsIgnoreCase(b)) System.out.println("Benne.");
			else System.out.println("Male.");
		
		System.out.print("a.compareToIgnoreCase(b) == 0: ");
		if (a.compareToIgnoreCase(b) == 0) System.out.println("Benne.");
			else System.out.println("Male.");

	}

}
