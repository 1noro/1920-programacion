package examenRec11;
import java.util.Scanner;
public class ExamenRec11 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int i = 0, e = 0, tam = 4, aciertos = 0, intentos = 3;
		int v1[] = new int[tam];
		int v2[] = new int[tam];
		
		System.out.println("Llenar v1:");
		for (i = 0; i < tam; i++) {
			do {
				System.out.print("Valor " + i + " (0-9): ");
				v1[i] = s.nextInt();
			} while(v1[i] < 0 || v1[i] > 9);
		}
		
		do {
			System.out.println("\n## Intentos restantes: " + intentos);
			aciertos = 0;
			System.out.println("\nLlenar v2:");
			for (i = 0; i < tam; i++) {
				do {
					System.out.print("Valor " + i + " (0-9): ");
					v2[i] = s.nextInt();
				} while(v2[i] < 0 || v2[i] > 9);
			}
			
			//comparamos v1 y v2
			System.out.print("\nCoincidencias: ");
			for (i = 0; i < tam; i++) {
				for (e = 0; e < tam; e++) {
					if (v2[i] == v1[e]) {
						if (i == e) {
							System.out.print("B");
							aciertos++;
						} else {
							System.out.print("A");
						}
					}
				}
			}
			
			intentos--;
		} while (intentos > 0 && aciertos < 4);
		
		System.out.println();
		if (aciertos == 4) System.out.println("\n## GANASTE :)"); 
			else System.out.println("\n## PERDISTE :("); 
		
		s.close();
		
	}

}
