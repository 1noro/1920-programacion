package sumaOresta;

import java.util.Scanner;

public class SumaOresta {
	public static void main(String[] args) {
		int n1 = 0; // 4bytes
		int n2 = 0; 
		int resultado = 0;
		Scanner entrada = new Scanner(System.in);
		
		System.out.print("Número 1: ");
		n1 = entrada.nextInt();
		System.out.print("Número 2: ");
		n2 = entrada.nextInt();
		
		if (n1 > n2) {
			resultado = n1 - n2;
			System.out.print(n1 + " - " + n2 + " = " + resultado + "\n");
		} else {
			resultado = n1 + n2;
			System.out.print(n1 + " + " + n2 + " = " + resultado + "\n");
		}

		entrada.close();
	}
}
