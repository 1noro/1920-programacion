package paisGuerra;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/*
 * TODO:
 * Euniciado básico - OK
 * Comprobar que no se repita el ide de pais y el de guerra - OK
 * Comprobar que los ides de los contrincantes estén dados de alta - OK
 * Guardar los arrays en un archivo - OK
 * Llenar los arrays a partir de los archivos - OK
 * Hacer el tabulado en los listados - 
 * Hacer bajas - 
 * 
 * */

public class paisGuerra {
    
    public static final int nMaxOpt = 9;
    public static final String finAltas = "fin";
    public static final String dir = "/opt/jarchivos/";
    public static final String filenameP = "paises.txt";
    public static final String filenameG = "gueras.txt";
    
    // --- UTILIDADES ----------------------------------------------------------
    public static int getLastI(Object[] objArr) {
        int i = 0;
        while (i < objArr.length && objArr[i] != null) i++;
        return i;
    }
    
    public static boolean existeId(int id, Instancia[] arr) {
        boolean out = false;
        int i = 0;
        while (i < arr.length && arr[i] != null) {
            if (id == arr[i].getId()) {
                out = true;
                break;
            }
            i++;
        }
        return out;
    }
    
    public static void limpiarArchivo(String fulldir) {
        try {
            //TRUE para que no sobreescriba
            FileWriter fw = new FileWriter(fulldir, false);
            fw.write("");
            fw.close();
        } catch(IOException ioe) {}
    }

    // --- INTERFAZ DE USUARIO -------------------------------------------------
    public static void printMenu() {
        System.out.println();
        System.out.println("--- MENU ---");
        System.out.println(" 1 - Alta paises");
        System.out.println(" 2 - Alta guerras");
        System.out.println(" 3 - Listar paises");
        System.out.println(" 4 - Listar guerras");
        System.out.println(" 5 - Buscar paises por guerra");
        System.out.println(" 6 - Guardar paises en archivo");
        System.out.println(" 7 - Guardar guerras en archivo");
        System.out.println(" 8 - Cargar paises desde archivo");
        System.out.println(" 9 - Cargar guerras desde archivo");
        System.out.println();
        System.out.println(" 0 - Salir");
        System.out.println();
    }
    
    public static int readOpt(Scanner s, int max) {
        int out = 0;
        do {
            System.out.print("Escribe una opción: ");
            out = s.nextInt();
            s.nextLine();
        } while (out < 0 || out > max);
        return out;
    }
    
    // --- ALTAS ---------------------------------------------------------------
    public static String readNombre(Scanner s, int limite) {
        String in = "";
        do {
            System.out.print("Nombre (" + limite + " char max); '" + finAltas + "' para acabar: ");
            in = s.nextLine();
        } while (!in.equals(finAltas) && in.length() > limite);
        return in;
    }
    
    public static int readIdNoRepetido(Scanner s, String motivo, Instancia[] arr) {
        String in_str = "";
        int in = 0;
        boolean isInt = false;
        while (true) {
            in = 0;
            do {
                System.out.print("Id " + motivo + " (numérico): ");
                in_str = s.nextLine();
                try {
                    in = Integer.parseInt(in_str);
                    isInt = true;
                } catch (NumberFormatException nfe) {
                    System.out.println("# ERROR: No es un número.");
                }
            } while (!isInt);
            if (existeId(in, arr)) {
                System.out.println("# ERROR: Id " + in + " duplicado.");
            } else {
                break;
            }
        }
        return in;
    }
    
    public static int readIdComprobandoExistencia(Scanner s, String motivo, Instancia[] arr) {
        String in_str = "";
        int in = 0;
        boolean isInt = false;
        while (true) {
            in = 0;
            do {
                System.out.print("Id " + motivo + " (numérico): ");
                in_str = s.nextLine();
                try {
                    in = Integer.parseInt(in_str);
                    isInt = true;
                } catch (NumberFormatException nfe) {
                    System.out.println("# ERROR: No es un número.");
                }
            } while (!isInt);
            if (!existeId(in, arr)) {
                System.out.println("# ERROR: El id " + in + " no existe.");
            } else {
                break;
            }
        }
        return in;
    }
    
    public static int readIdSimple(Scanner s, String motivo) {
        String in_str = "";
        int in = 0;
        boolean isInt = false;
        do {
            System.out.print("Id " + motivo + " (numérico): ");
            in_str = s.nextLine();
            try {
                in = Integer.parseInt(in_str);
                isInt = true;
            } catch (NumberFormatException nfe) {
                System.out.println("# ERROR: No es un número.");
            }
        } while (!isInt);
        return in;
    }

    public static Pais[] altasPaises(Scanner s, Pais[] paises) {
        int i = getLastI(paises);
        String nombre = "";
        int id = 0;
        while (!nombre.equals(finAltas) && i < paises.length) {
            System.out.println("\n>> Nueva alta:");
            nombre = readNombre(s, 10);
            if (!nombre.equals(finAltas)) {
                id = readIdNoRepetido(s, "de país", paises);
                paises[i] = new Pais(id, nombre);
                i++;
            }
        }
        return paises;
    }
    
    public static Guerra[] altasGuerras(Scanner s, Guerra[] guerras, Pais[] paises) {
        int i = getLastI(guerras);
        String nombre = "";
        int id = 0;
        int idPais1 = 0;
        int idPais2 = 0;
        while (!nombre.equals(finAltas) && i < guerras.length) {
            System.out.println("\n>> Nueva alta:");
            nombre = readNombre(s, 20);
            if (!nombre.equals(finAltas)) {
                id = readIdNoRepetido(s, "de guerra", guerras);
                idPais1 = readIdComprobandoExistencia(s, "del primer contrincante", paises);
                idPais2 = readIdComprobandoExistencia(s, "del segundo contrincante", paises);
                guerras[i] = new Guerra(id, nombre, idPais1, idPais2);
                i++;
            }
        }
        return guerras;
    }
    
    // --- LISTADOS ------------------------------------------------------------
    public static void listar(Object[] arr) {
        int i = 0;
        System.out.println("\n# --- Listado --- ");
        while (i < arr.length && arr[i] != null) {
            System.out.println(arr[i].toString());
            i++;
        }
    }
    
    public static void listarNull(Object[] arr) {
        int i = 0;
        System.out.println("\n# --- Listado --- ");
        while (i < arr.length) {
            if (arr[i] != null) System.out.println(arr[i].toString());
                else System.out.println("null");
            i++;
        }
    }
    
    // --- CONSULTAS -----------------------------------------------------------
    public static int[] getIdContrincantes(int idGuerra, Guerra[] guerras) {
        int[] out = {0, 0};
        int i = 0;
        while (i < guerras.length) {
            if (guerras[i].getId() == idGuerra) {
                out[0] = guerras[i].getIdPais1();
                out[1] = guerras[i].getIdPais2();
                break;
            }
            i++;
        }
        return out;
    }
    
    public static Pais getPaisFromId(int idPais, Pais[] paises) {
        Pais out = null;
        int i = 0;
        for (i = 0; i < paises.length; i++) {
            if (paises[i].getId() == idPais) {
                out = paises[i];
                break;
            }
        }
        return out;
    }
    
    public static void listarPaisesPorGuerra(Scanner s, Pais[] paises, Guerra[] guerras) {
        int idGuerra = readIdSimple(s, "de la guerra a seleccionar");
        int[] idContrincantes = getIdContrincantes(idGuerra, guerras);
        int idPais1 = idContrincantes[0];
        int idPais2 = idContrincantes[1];
        System.out.println("\n--- PAISES EN LA GUERRA " + idGuerra + " ---");
        System.out.println(getPaisFromId(idPais1, paises).toString());
        System.out.println(getPaisFromId(idPais2, paises).toString());
    }
    
    // --- GUARDAR EN ARCHIVOS -------------------------------------------------
    public static void saveInFile(Instancia[] arr, String fullDir) {
        int i = 0;
        limpiarArchivo(fullDir);
        try {
            //TRUE para que no sobreescriba
            FileWriter fw = new FileWriter(fullDir, true);
            while (i < arr.length && arr[i] != null) {
                fw.write(arr[i].toStringMultiline()); 
                i++;
            }
            fw.close();
        } catch(IOException ioe) {
            System.out.println("### Guardado fallido");
        }
        System.out.println("\n# ARCHIVO GUARDADO ");
    }
    
    // --- CARGAR DESDE ARCHIVOS -----------------------------------------------
    public static void readPaisesFromFile(Pais[] paises, String fullDir) {
        int i = 0;
        String linea = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(fullDir));
            linea = br.readLine();
            while (linea != null) {
                paises[i] = new Pais();
                paises[i].setId(Integer.valueOf(linea));
                linea = br.readLine();
                paises[i].setNombre(linea);
                linea = br.readLine();
                i++;
            }
            br.close();
        } catch(IOException ioe) {
            System.out.println("### Lectura fallida");
        }
        System.out.println("\n# ARCHIVO LEIDO");
    }
    
    public static void readGuerrasFromFile(Guerra[] guerras, String fullDir) {
        int i = 0;
        String linea = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(fullDir));
            linea = br.readLine();
            while (linea != null) {
                guerras[i] = new Guerra();
                guerras[i].setId(Integer.valueOf(linea));
                linea = br.readLine();
                guerras[i].setNombre(linea);
                linea = br.readLine();
                guerras[i].setIdPais1(Integer.valueOf(linea));
                linea = br.readLine();
                guerras[i].setIdPais2(Integer.valueOf(linea));
                linea = br.readLine();
                i++;
            }
            br.close();
        } catch(IOException ioe) {
            System.out.println("### Lectura fallida");
        }
        System.out.println("\n# ARCHIVO LEIDO");
    }
    
    // --- MAIN ----------------------------------------------------------------
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int opt = 0;
        Pais paises[] = new Pais[4];
        Guerra guerras[] = new Guerra[2];
        
        do {
            printMenu();
            opt = readOpt(s, nMaxOpt);
            if (opt != 0) {
                switch (opt) {
                    case 1:
                        paises = altasPaises(s, paises);
                        break;
                    case 2:
                        guerras = altasGuerras(s, guerras, paises);
                        break;
                    case 3:
                        listarNull(paises);
                        break;
                    case 4:
                        listarNull(guerras);
                        break;
                    case 5:
                        listarPaisesPorGuerra(s, paises, guerras);
                        break;
                    case 6:
                        saveInFile(paises, dir + filenameP);
                        break;
                    case 7:
                        saveInFile(guerras, dir + filenameG);
                        break;
                    case 8:
                        readPaisesFromFile(paises, dir + filenameP);
                        break;
                    case 9:
                        readGuerrasFromFile(guerras, dir + filenameG);
                        break;
                }
            }
        } while (opt != 0);
        
        System.out.println("Bye (;︵;)");
        s.close();
    }

}
