package contadores_acumuladores_191023a;

import java.util.Scanner;

public class Contadores_acumuladores_191023a {

	public static void main(String[] args) {
		int numero = 0;
		int acc = 0, cant = 0;
		Scanner e = new Scanner(System.in);
		
		// numero = numero + 1
		System.out.print("> numero = numero + 1\n");
		numero = 0;
		while (numero < 5) {
			System.out.println("Número: " + numero);
			numero = numero + 1;
//			numero += 1;
//			numero++;
//			++numero;
		}
		System.out.println("Número salida: " + numero);
		
		// ++numero
		System.out.print("\n> ++numero\n");
		numero = 0;
		while (numero < 5) {
			System.out.println("Número: " + (++numero));
		}
		System.out.println("Número salida: " + numero);
		
		// numero++
		System.out.print("\n> numero++\n");
		numero = 0;
		while (numero < 5) {
			System.out.println("Número: " + (numero++));
		}
		System.out.println("Número salida: " + numero);
		
		// --numero
		System.out.print("\n> --numero\n");
		numero = 5;
		while (numero > 0) {
			System.out.println("Número: " + (--numero));
		}
		System.out.println("Número salida: " + numero);
		
		// numero--
		System.out.print("\n> numero--\n");
		numero = 4;
		while (numero >= 0) {
			System.out.println("Número: " + (numero--));
		}
		System.out.println("Número salida: " + numero);
		
		/* ------------------------------------------------------------------ */
		
		System.out.print("\n");
		do {
			System.out.print("Acumulado: " + acc + "\n");
			System.out.print("Cantidad: ");
			cant = e.nextInt();
			acc += cant;
		} while (acc <= 500);
		
		System.out.print("Acumulado: " + acc + "\n");
		
		e.close();
		
	}

}
