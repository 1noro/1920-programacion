package buscarNumeroEnFicheroBin;

import java.util.Scanner;
import java.io.IOException;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class BuscarNumeroEnFicheroBin {
	
	public static final String dir = "/opt/archivosJava/";
	public static final String filename = "numbin02.dat";
	public static final int fin = 999;

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int in = 0, busqueda = 0, ocurrencias = 0, num = 0;
		boolean continuar = true;
		char c = ' ';
		
		do {
		
			try {
				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(dir + filename));
				while (true) {
					do {
						System.out.print("Numero: ");
						in = s.nextInt();
					} while (in < 0 || in > 500 && in != 999);
					if (in == 999) break;
					oos.writeInt(in);
					System.out.println("> [saved]");
				}
				oos.close();
			} catch (IOException ioe) {};
			
			System.out.println();
			System.out.print("Número a buscar: ");
			busqueda = s.nextInt();
			
			try {
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(dir + filename));
				while (true) {
					try {
						num = ois.readInt();
						System.out.println("¿" + busqueda + " == " + num + "?");
						if (num == busqueda) ocurrencias++;
				    } catch (EOFException eof) {break;}
				}
				ois.close();
			} catch (IOException ioe) {};
			
			if (ocurrencias == 0) System.out.println("> No se ha encontrado: " + busqueda + ".");
			else System.out.println("> " + busqueda + " se ha encintrado " + ocurrencias + " veces.");
			
			System.out.println();
			do {
				System.out.print("Volver a empezar (S/N)? ");
				try {
					c = (char) System.in.read();
					while(System.in.read()!='\n');
				} catch (IOException ioe) {}
				c = Character.toLowerCase(c);
			} while (c != 's' && c != 'n');
			
			if (c == 'n') continuar = false;
			
		} while (continuar);
		
		s.close();
	}

}
