package ficheroFrases;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FicheroFrases {
	
	// --- CLI -----------------------------------------------------------------
	public static void printMenu() {
		System.out.println("");
		System.out.println("--- MENU ---");
		System.out.println("");
		System.out.println("1 - Crear fichero frases");
		System.out.println("2 - Crear ficheros vocales y conosonantes");
		System.out.println("3 - Visualizar fichero frases");
		System.out.println("4 - Visualizar fchero vocales");
		System.out.println("5 - Visualizar fichero consonantes");
		System.out.println("");
		System.out.println("0 - Fin");
		System.out.println("");
	}
	
	public static int readOpt(Scanner s, int max) {
		int out = 0;
		do {
			System.out.print("> ");
			out = s.nextInt();
			s.nextLine();
		} while (out > max || out < 0);
		return out;
	}

	// --- UTILS ---------------------------------------------------------------
	public static boolean isVocal(char c) {
		boolean out = false;
		if (
			c == 'a' || 
			c == 'e' ||
			c == 'i' ||
			c == 'o' ||
			c == 'u' ||
			c == 'á' || 
			c == 'é' ||
			c == 'í' ||
			c == 'ó' ||
			c == 'ú' ||
			c == 'ü'
		) {
			out = true;
		}
		return out;
	}
	
	public static String getVocales(String f) {
		String out = "";
		int i = 0, e = 0;
		char c = ' ';
		boolean existeEnOut = false;
		for (i = 0; i < f.length(); i++) {
			c = Character.toLowerCase(f.charAt(i));
			if (isVocal(c) && Character.isLetter(c)) {
				existeEnOut = false;
				e = 0;
				while (e < out.length() && !existeEnOut) {
					if (c == out.charAt(e)) {
						existeEnOut = true;
					}
					e++;
				}
				
				if (!existeEnOut) out += c;
			}
		}
		return out;
	}
	
	public static String getFullVocales(String f) {
		String out = "";
		int i = 0;
		char c = ' ';
		for (i = 0; i < f.length(); i++) {
			c = Character.toLowerCase(f.charAt(i));
			if (isVocal(c) && Character.isLetter(c)) out += f.charAt(i);
		}
		return out;
	}
	
	public static String getConsonantes(String f) {
		String out = "";
		int i = 0, e = 0;
		char c = ' ';
		boolean existeEnOut = false;
		for (i = 0; i < f.length(); i++) {
			c = Character.toLowerCase(f.charAt(i));
			if (!isVocal(c) && Character.isLetter(c)) {
				existeEnOut = false;
				e = 0;
				while (e < out.length() && !existeEnOut) {
					if (c == out.charAt(e)) {
						existeEnOut = true;
					}
					e++;
				}
				
				if (!existeEnOut) out += c;
			}
		}
		return out;
	}
	
	public static String getFullConsonantes(String f) {
		String out = "";
		int i = 0;
		char c = ' ';
		for (i = 0; i < f.length(); i++) {
			c = Character.toLowerCase(f.charAt(i));
			if (!isVocal(c) && Character.isLetter(c)) out += f.charAt(i);
		}
		return out;
	}
	
	// --- FILE MNG ------------------------------------------------------------
	public static void saveLine(String line, String fullDir) {
		try {
			//TRUE para que no sobreescriba
			FileWriter fw = new FileWriter(fullDir, true);
			line += '\n';
			fw.write(line); 
			fw.close();
		} catch(IOException ioe) {}
	}
	
	public static void saveStringLineaALinea(String lista, String fullDir) {
		int i = 0;
		try {
			//TRUE para que no sobreescriba
			FileWriter fw = new FileWriter(fullDir, true);
			for (i = 0; i < lista.length(); i++) {
				fw.write(lista.charAt(i) + "\n"); 
			}
			fw.close();
		} catch(IOException ioe) {}
	}
	
	public static void resetFile(String fullDir) {
		try {
			//TRUE para que no sobreescriba
			FileWriter fw = new FileWriter(fullDir, false);
			fw.write(""); 
			fw.close();
		} catch(IOException ioe) {}
	}
	
	// --- USER INPUT ----------------------------------------------------------
	public static void llenarFrases(Scanner s, String fullDir) {
		String frase = "";
		System.out.println("\n#Escribe frases (*** para salir):");
		System.out.print("> ");
		frase = s.nextLine();
		while (!frase.equals("***")) {
			saveLine(frase, fullDir);
			System.out.print("> ");
			frase = s.nextLine();
		}
	}
	
	public static void splitVC(String frasesFullDir, String vocalesFullDir, String consonantesFullDir) {
		String frase = "";
		System.out.println("\n#Evaluando vocales y cononantes...");
		resetFile(vocalesFullDir);
		resetFile(consonantesFullDir);
		try {
			FileReader fr = new FileReader(frasesFullDir);
			BufferedReader br = new BufferedReader(fr);
			frase = br.readLine();
			while (frase != null) {
				saveStringLineaALinea(getFullVocales(frase), vocalesFullDir);
				saveStringLineaALinea(getFullConsonantes(frase), consonantesFullDir);
				frase = br.readLine();
			}
			br.close();
			fr.close();
		} catch(IOException ioe) {}
	}
	
	public static void viewFile(String fullDir) {
		String linea = "";
		System.out.println("\n#Contenido de: " + fullDir);
		try {
			FileReader fr = new FileReader(fullDir);
			BufferedReader br = new BufferedReader(fr);
			linea = br.readLine();
			while (linea != null) {
				System.out.println(linea);
				linea = br.readLine();
			}
			br.close();
			fr.close();
		} catch(IOException ioe) {}
	}
	
	// --- MAIN ----------------------------------------------------------------
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String dir = "/opt/archivosJava/";
		String frasesFilename = "frases00.txt";
		String vocalesFilename = "vocales00.txt";
		String consonantesFilename = "consonantes00.txt";
		int opt = 0;
		
		do {
			printMenu();
			opt = readOpt(s, 5);
			if (opt != 0) {
				switch (opt) {
					case 1:
						llenarFrases(s, dir + frasesFilename);
						break;
					case 2:
						splitVC(dir + frasesFilename, dir + vocalesFilename, dir + consonantesFilename);
						break;
					case 3:
						viewFile(dir + frasesFilename);
						break;
					case 4:
						viewFile(dir + vocalesFilename);
						break;
					case 5:
						viewFile(dir + consonantesFilename);
						break;
				}
			}
		} while(opt != 0);
		
	}

}
