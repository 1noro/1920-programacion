package cajero_multicliente_20190926b;

import java.util.Scanner;

public class Cajero_multicliente_20190926b {

	public static void main(String[] args) {

		int in = 0, tot = 0, tot_dia = 0, num_cli = 0;
		Scanner e = new Scanner(System.in);
		
		do {
		
			in = 0;
			tot = 0;
			
			System.out.print("# Cliente " + (num_cli + 1) + "\n");
			
			do {
				System.out.print("Precio producto? (0 salir): ");
				in = e.nextInt();
				tot += in;
			} while (in != 0);
			
			System.out.print("> Total a pagar: " + tot + "€\n");
			
			do {
				System.out.print("Precio entregado: ");
				in = e.nextInt();
				if (tot > in) {
					System.out.print("Faltan: " + (tot - in) + "€\n");
				}
			} while (in < tot);
	
			System.out.print("> Cambio: " + (in - tot) + "€\n");
			
			num_cli++;
			tot_dia += tot;
			
			do {
				System.out.print("Otro cliente? (0/1): ");
				in = e.nextInt();
			} while (in != 0 && in != 1);
			
		} while (in == 1);
		
		System.out.print("\n## RESUMEN DEL DÍA ###\n");
		System.out.print("> Num. clientes: " + num_cli + "\n");
		System.out.print("> Total día: " + tot_dia + "\n");
		System.out.print("> Media de gasto por cliente: " + (tot_dia / num_cli) + "\n");
		
		e.close();

	}

}
