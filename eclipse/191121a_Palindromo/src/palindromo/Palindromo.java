package palindromo;

import java.util.Scanner;

public class Palindromo {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String fraseIn = "";
		String fraseNoSpaces = "";
		char c = ' ';
		int i = 0;
		boolean esPalindromo = true;
		
		System.out.print("Frase: ");
		fraseIn = entrada.nextLine();
		entrada.close();
		fraseIn = fraseIn.toLowerCase();
		
		for (i = 0; i < fraseIn.length(); i++) {
			c = fraseIn.charAt(i);
			if (c != ' ') fraseNoSpaces += c;
		}
		
		for (i = 0; i < fraseNoSpaces.length(); i++) 
			if (fraseNoSpaces.charAt(i) != fraseNoSpaces.charAt((fraseNoSpaces.length() - 1) - i))
				esPalindromo = false;
				
		
		if (esPalindromo) System.out.println("Es palindromo.");
			else System.out.println("NO es palindromo.");
		
		// no está perfecto, da mas vueltas de las necesarias

	}

}
