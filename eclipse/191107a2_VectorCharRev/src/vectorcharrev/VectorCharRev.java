package vectorcharrev;

import java.io.IOException;

public class VectorCharRev {

	public static void main(String[] args) {
		int i = 0;
		char v[] = new char[5];
		while (i < 5) {
			System.out.print("Introduzca la letra " + (i + 1) + ": ");
			try {
				v[i] = (char) System.in.read();
				while(System.in.read()!='\n'); //leer hasta pulsar enter '\n'
			} catch (IOException ioe) {System.out.println("error1");}
			if (Character.isLetter(v[i])) i++; 
		}
		
		System.out.println();
		for (i = 5 - 1; i >= 0; i--) System.out.println(v[i]);
	}

}
