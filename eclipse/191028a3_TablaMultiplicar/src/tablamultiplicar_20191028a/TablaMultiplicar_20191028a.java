package tablamultiplicar_20191028a;

import java.util.Scanner;

public class TablaMultiplicar_20191028a {
	public static void main(String[] args) {
		int num = 0, min = 0, max = 10, i;
		Scanner e = new Scanner(System.in);
		System.out.print("Número de la tabla: ");
		num = e.nextInt();
		do {
			System.out.print("Número inicio: ");
			min = e.nextInt();
			System.out.print("Número final: ");
			max = e.nextInt();
		} while (min > max);
		for (i = min; i <= max; i++) 
			System.out.print(num + " * " + i + " = " + (num * i) + "\n");
		e.close();
	}
}
