package contarvocales;

import java.util.Scanner;

public class ContarVocales {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String frase = "";
		int i = 0, j = 0;
		char vocales[] = {'a', 'e', 'i', 'o', 'u'};
		int ocurrencias[] = {0, 0, 0, 0, 0};
		
		System.out.print("Frase: ");
		frase = entrada.nextLine();
		entrada.close();
		frase = frase.toLowerCase();
		
		for (i = 0; i < frase.length(); i++) 
			for (j = 0; j < vocales.length; j++)
				if (frase.charAt(i) == vocales[j]) ocurrencias[j]++;
		
		for (j = 0; j < vocales.length; j++) 
			System.out.println(vocales[j] + ": " + ocurrencias[j]);
	}

}
