package herencias00;

public class Alumno extends Persona {

    double nota1;
    double nota2;
    double nota3;
    int numero = 2;

    Alumno(String nombre, String apellido1, String apellido2, int edad, double nota1, double nota2, double nota3) {
        /*
        super.nombre = nombre;
        super.apellido1 = apellido1;
        super.apellido2 = apellido2;
        super.edad = edad;
        */
        
        super(nombre, apellido1, apellido2, edad); // llamada al constructor de la superclase
        
        this.nota1 = nota1;
        this.nota2 = nota2;
        this.nota3 = nota3;
    }

    Alumno(double nota1, double nota2, double nota3) {
        this.nota1 = nota1;
        this.nota2 = nota2;
        this.nota3 = nota3;
    }

    Alumno() {}

    public double getNota1() {
        return nota1;
    }

    public void setNota1(double nota1) {
        this.nota1 = nota1;
    }

    public double getNota2() {
        return nota2;
    }

    public void setNota2(double nota2) {
        this.nota2 = nota2;
    }

    public double getNota3() {
        return nota3;
    }

    public void setNota3(double nota3) {
        this.nota3 = nota3;
    }

    public void mostrarDatos() {
        System.out.println("Nota1: " + this.nota1);
        System.out.println("Nota2: " + this.nota2);
        System.out.println("Nota3: " + this.nota3);
    }

    public void mostrarDatos1() {
        System.out.println(" -- mostrarDatos1 -- ");
//        System.out.println("Nombre: " + super.nombre);
//        System.out.println("Apellido1: " + super.apellido1);
//        System.out.println("Apellido2: " + super.apellido2);
//        System.out.println("Edad: " + super.edad);
        
        System.out.println("Nombre: " + this.nombre);
        System.out.println("Apellido1: " + this.apellido1);
        System.out.println("Apellido2: " + this.apellido2);
        System.out.println("Edad: " + this.edad);
        
        System.out.println("Nota1: " + this.nota1);
        System.out.println("Nota2: " + this.nota2);
        System.out.println("Nota3: " + this.nota3);
        
        System.out.println("Número (Alumno): " + this.numero);
        System.out.println("Número (Persona): " + super.numero);
    }
    
    public void mostrarDatos2() {
        System.out.println(" -- mostrarDatos2 -- ");
        super.mostrarDatos();
        System.out.println("Nota1: " + this.nota1);
        System.out.println("Nota2: " + this.nota2);
        System.out.println("Nota3: " + this.nota3);
    }
    
}
