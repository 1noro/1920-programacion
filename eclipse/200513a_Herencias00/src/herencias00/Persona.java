package herencias00;

public class Persona {
    
    public String nombre;
    protected String apellido1;
    protected String apellido2;
    protected int edad;
    String numero = "dos";
    
    Persona(String nombre, String apellido1, String apellido2, int edad) {
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.edad = edad;
    }
    
    Persona() {}

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    public void mostrarDatos() {
        System.out.println("Nombre: " + this.nombre);
        System.out.println("Apellido1: " + this.apellido1);
        System.out.println("Apellido2: " + this.apellido2);
        System.out.println("Edad: " + this.edad);
    }
    
}
