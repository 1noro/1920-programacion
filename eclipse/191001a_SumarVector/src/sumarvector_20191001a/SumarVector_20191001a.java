package sumarvector_20191001a;

import java.util.Scanner;

public class SumarVector_20191001a {

	public static void main(String[] args) {
		Scanner e = new Scanner(System.in);
		int i = 0, suma = 0, tam = 12;
		
//		int v[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
		int v[] = new int[tam];
		
		for (i = 0; i < tam; i++) {
			System.out.print("Teclea el valor " + (i + 1) + ": ");
			v[i] = e.nextInt();
		}
		
		for (i = 0; i < tam; i++) {
			suma += v[i];
		}

		System.out.println("Suma: " + suma);
		e.close();
	}

}
