package leerFicheroTexto;
import java.io.IOException;
import java.io.FileWriter;
import java.io.FileReader;
public class LeerFicheroTexto {

	public static void main(String[] args) {
		String dir = "/opt/archivosJava/";
		String filename = "a00.txt";
		int car = ' ';
		String texto = "";
		
		// ESCRITURA
		try {
			//TRUE para que no sobreescriba
			FileWriter fw = new FileWriter(dir + filename, true);
			System.out.println("#Escribe texto: ");
			car = (char) System.in.read();
			while (car != '*') {
				fw.write(car); 
				car = (char) System.in.read();
			}
			fw.close();
		} catch(IOException ioe) {}
		System.out.println();
		
		// LECTURA
		try {
			FileReader fr = new FileReader(dir + filename);
			car = fr.read();
			while (car != -1) { //-1 es el fin de fichero
				texto += (char) car;
				car = fr.read();
			}
			fr.close();
		} catch(IOException ioe) {}
		System.out.println("#Texto leido:");
		System.out.println(texto);
		System.out.println();
		
		System.out.println("Bye (;︵;)");
	}

}
