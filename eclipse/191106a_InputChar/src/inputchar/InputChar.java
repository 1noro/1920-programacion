package inputchar;

import java.io.IOException;

public class InputChar {

	public static void main(String[] args) {
		
		// in.read int
		int numero = 0;
		System.out.print("Teclea algo: ");
		try {
			numero = System.in.read();
			while(System.in.read()!='\n'); //leer hasta pulsar enter
		} catch (IOException ioe) {
			System.out.println("error1");
		}
		System.out.println("cosa: " + numero);
		
		
		// in.read char
		char caracter = ' ';
		System.out.println(caracter);
		System.out.print("Teclea un caracter: ");
		try {
			caracter = (char) System.in.read();
			while(System.in.read()!='\n');
		} catch (IOException ioe) {
			System.out.println("error2");
		}
		System.out.println("caracter: " + caracter);
		
		
		System.out.print("¿'" + caracter + "' == 'W'? --> " );
		if (caracter == 'W')
			System.out.println("Acertaste");
		else
			System.out.println("Fallaste");
		
		System.out.print("¿'" + Character.toUpperCase(caracter) + "' == 'W'? --> " );
		if (Character.toUpperCase(caracter) == 'W')
			System.out.println("Acertaste");
		else
			System.out.println("Fallaste");
		
		System.out.print("¿'" + caracter + "'.isLetter? --> " );
		if (Character.isLetter(caracter))
			System.out.println("Acertaste");
		else
			System.out.println("Fallaste");
		
		System.out.print("¿'" + caracter + "'.isDigit? --> " );
		if (Character.isDigit(caracter))
			System.out.println("Acertaste");
		else
			System.out.println("Fallaste");
		
	}

}
