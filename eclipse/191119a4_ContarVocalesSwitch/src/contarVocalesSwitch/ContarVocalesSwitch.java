package contarVocalesSwitch;

import java.util.Scanner;

public class ContarVocalesSwitch {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String frase = "";
		int i = 0;
		int ca = 0, ce = 0, ci = 0, co = 0, cu = 0, cc = 0;
		char c = ' ';
		
		System.out.print("Frase: ");
		frase = entrada.nextLine();
		entrada.close();
		frase = frase.toLowerCase();
		
		for (i = 0; i < frase.length(); i++) {
			c = frase.charAt(i);
			switch (c) {
				case 'a':
					ca++;
					break;
				case 'e':
					ce++;
					break;
				case 'i':
					ci++;
					break;
				case 'o':
					co++;
					break;
				case 'u':
					cu++;
					break;
				case ' ':
					break;
				default:
					cc++;
					break;
			}
		}
		
		System.out.println("Total \"a\": " + ca);
		System.out.println("Total \"e\": " + ce);
		System.out.println("Total \"i\": " + ci);
		System.out.println("Total \"o\": " + co);
		System.out.println("Total \"u\": " + cu);
		System.out.println("Total consonantes: " + cc);

	}

}
