package clases00;

public class Alumno {
	private int numero;
	private String nombre;
	private double nota1;
	private double nota2;
	private double nota3;
	
	Alumno(int _numero, String _nombre, double _nota1, double _nota2, double _nota3) {
		this.numero = _numero;
		this.nombre = _nombre;
		this.nota1 = _nota1;
		this.nota2 = _nota2;
		this.nota3 = _nota3;
	}
	
	void visualizarDatos() {
		System.out.println("Número:\t" + this.numero);
		System.out.println("Nombre:\t" + this.nombre);
		System.out.println("Nota1:\t" + this.nota1);
		System.out.println("Nota2:\t" + this.nota2);
		System.out.println("nota3:\t" + this.nota3);
	}
	
	double notaMedia() {
		double media = 0;
		media = (this.nota1 + this.nota2 + this.nota3) / 3;
		return media;
	}
	
	boolean esApto() {
		boolean out = false;
		if (this.notaMedia() >= 4.5) out = true;
		return out;
	}
	
	void setNumero(int num) {
		this.numero = num;
	}
}
