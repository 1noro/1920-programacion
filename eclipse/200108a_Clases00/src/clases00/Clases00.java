package clases00;
import java.util.Scanner;
public class Clases00 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int numero = 0;
		String nombre = "";
		double nota1 = 0, nota2 = 0, nota3 = 0;
		
		//Alumno1
		System.out.print("Número Alumno1: ");
		numero = s.nextInt();
		s.nextLine();
		System.out.print("Nombre Alumno1: ");
		nombre = s.nextLine();
		System.out.print("Nota1 Alumno1: ");
		nota1 = s.nextDouble();
		System.out.print("Nota2 Alumno1: ");
		nota2 = s.nextDouble();
		System.out.print("Nota3 Alumno1: ");
		nota3 = s.nextDouble();
		
//		Alumno alumno = new Alumno(1, "Pedro", 0, 0, 0);
		Alumno alumno1 = new Alumno(numero, nombre, nota1, nota2, nota3);
		
		//Alumno2
		System.out.println();
		System.out.print("Número Alumno2: ");
		numero = s.nextInt();
		s.nextLine();
		System.out.print("Nombre Alumno2: ");
		nombre = s.nextLine();
		System.out.print("Nota1 Alumno2: ");
		nota1 = s.nextDouble();
		System.out.print("Nota2 Alumno2: ");
		nota2 = s.nextDouble();
		System.out.print("Nota3 Alumno2: ");
		nota3 = s.nextDouble();
		
//		Alumno alumno = new Alumno(1, "Pedro", 0, 0, 0);
		Alumno alumno2 = new Alumno(numero, nombre, nota1, nota2, nota3);
		
		//Alumno1
		System.out.println();
		alumno1.visualizarDatos();
		
		System.out.println();
		System.out.println("Nota media Alumno1: " + alumno1.notaMedia());
		
		System.out.println();
		System.out.println("Es apto Alumno1? " + alumno1.esApto());
		
		//Alumno2
		System.out.println();
		alumno2.visualizarDatos();
		
		System.out.println();
		System.out.println("Nota media Alumno2: " + alumno2.notaMedia());
		
		System.out.println();
		System.out.println("Es apto Alumno2? " + alumno2.esApto());
		
//		Como igualamos direcciones, con cambiar el atributo de 1 lo cambiamos en los 2
//		alumno1 = alumno2;
//		alumno1.visualizarDatos();
//		alumno2.visualizarDatos();
//		alumno1.setNumero(111);
//		alumno1.visualizarDatos();
//		alumno2.visualizarDatos();
		
		s.close();
	}

}
