package ficheroTextoPersonaEdad;

import java.util.Scanner;
import java.io.IOException;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.BufferedReader;

public class FicheroTextoPersonaEdad {
    
    public static final int nMaxOpt = 5;
    public static final String finAltas = "fin";
    public static final String dir = "/opt/jarchivos/";
    public static final String filename = "personaEdad.txt";
    
    // --- UTILIDADES ----------------------------------------------------------
    public static String tabular(String in, int longitud) {
        String out = "";
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in.length()) out += in.charAt(i);
                else out += ' ';
        }
        return out;
    }
    
    public static String tabular(int in, int longitud) {
        String out = "";
        String in_str = Integer.toString(in);
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in_str.length()) out += in_str.charAt(i);
                else out += ' ';
        }
        return out;
    }
    
    // --- INTERFAZ DE USUARIO -------------------------------------------------
    public static void printMenu() {
        System.out.println();
        System.out.println("--- MENU ---");
        System.out.println(" 1 - Altas");
        System.out.println(" 2 - Listado");
        System.out.println(" 3 - Media edad");
        System.out.println(" 4 - Nombre del mayor y menor (sin rep.)");
        System.out.println();
        System.out.println(" " + nMaxOpt + " - Salir");
        System.out.println();
    }
    
    public static int readOpt(Scanner s, int max) {
        int out = 0;
        do {
            System.out.print("Teclee opción (1-" + max + "): ");
            out = s.nextInt();
            s.nextLine();
        } while (out < 0 || out > max);
        return out;
    }

    // --- ALTAS ---------------------------------------------------------------
    public static String readNombre(Scanner s, int limite) {
        String in = "";
        do {
            System.out.print("Nombre (" + limite + " char max); '" + finAltas + "' para acabar: ");
            in = s.nextLine();
        } while (!in.equals(finAltas) && in.length() > limite);
        return in;
    }
    
    public static int readEdad(Scanner s) {
        String in_str = "";
        int in = 0;
        boolean isInt = false;
        do {
            System.out.print("Edad (numérico): ");
            in_str = s.nextLine();
            try {
                in = Integer.parseInt(in_str);
                isInt = true;
            } catch (NumberFormatException nfe) {
                System.out.println("# ERROR: No es un número.");
            }
        } while (!isInt);
        return in;
    }
    
    public static void altas(Scanner s, String fullDir) {
        String nombre = "";
        int edad = 0;
        System.out.println("\n--- ALTAS ---");
        try {
            //TRUE para que no sobreescriba
            BufferedWriter bw = new BufferedWriter(new FileWriter(fullDir, true));
            while (!nombre.equalsIgnoreCase(finAltas)) {
                System.out.println("\n>> Nueva alta:");
                nombre = readNombre(s, 10);
                if (!nombre.equalsIgnoreCase(finAltas)) {
                    edad = readEdad(s);
                    bw.write(nombre);
                    bw.newLine();
                    bw.write(Integer.toString(edad));
                    bw.newLine();
                }
            }
            bw.close();
        } catch(IOException ioe) {
            System.out.println("# ERROR: Guardado fallido");
        }
        System.out.println("\n# ARCHIVO GUARDADO");
    }
    
    // --- LISTADO -------------------------------------------------------------
    public static void listar(String fullDir) {
        String linea = "";
        System.out.println("\n--- LISTADO ---");
        try {
            BufferedReader br = new BufferedReader(new FileReader(fullDir));
            linea = br.readLine();
            while (linea != null) {
                System.out.print(tabular(linea, 10) + " ");
                linea = br.readLine();
                System.out.print(tabular(linea,  5) + "\n");
                linea = br.readLine();
            }
            br.close();
        } catch(IOException ioe) {
            System.out.println("# ERROR: Lectura fallida");
        }
        System.out.println("\n# ARCHIVO LEIDO");
    }
    
    // --- MEDIA EDAD ----------------------------------------------------------
    public static void listarMedia(String fullDir) {
        String linea = "";
        int cuenta = 0;
        int suma = 0;
        System.out.println("\n--- LISTADO MEDIA ---");
        try {
            BufferedReader br = new BufferedReader(new FileReader(fullDir));
            linea = br.readLine();
            while (linea != null) {
                System.out.print(tabular(linea, 10) + " ");
                linea = br.readLine();
                System.out.print(tabular(linea,  5) + "\n");
                suma += Integer.parseInt(linea);
                cuenta++;
                linea = br.readLine();
            }
            br.close();
        } catch(IOException ioe) {
            System.out.println("# ERROR: Lectura fallida");
        }
        System.out.println("\n# Media de edad: " + (float) suma / (float) cuenta);
        System.out.println("\n# ARCHIVO LEIDO");
    }
    
    // --- MAYOR Y MENOR -------------------------------------------------------
    public static void verMayorMenor(String fullDir) {
        String nombre = "";
        int edad = 0;
        String minNombre = null;
        int minEdad = 0;
        String maxNombre = "";
        int maxEdad = 0;
        System.out.println("\n--- LISTADO MEDIA ---");
        try {
            BufferedReader br = new BufferedReader(new FileReader(fullDir));
            nombre = br.readLine();
            while (nombre != null) {
                edad = Integer.parseInt(br.readLine());
                if (edad < minEdad || minNombre == null) {
                    minNombre = nombre;
                    minEdad = edad;
                }
                if (edad > maxEdad) {
                    maxNombre = nombre;
                    maxEdad = edad;
                }
                nombre = br.readLine();
            }
            br.close();
        } catch(IOException ioe) {
            System.out.println("# ERROR: Lectura fallida");
        }
        
        System.out.println();
        System.out.println("# Mayor: " + tabular(maxNombre, 10) + " " + tabular(maxEdad, 5));
        System.out.println("# Menor: " + tabular(minNombre, 10) + " " + tabular(minEdad, 5));
        
        System.out.println("\n# ARCHIVO LEIDO");
    }
    
    // --- MAIN ----------------------------------------------------------------
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int opt = 0;
        
        do {
            printMenu();
            opt = readOpt(s, nMaxOpt);
            if (opt != nMaxOpt) {
                switch (opt) {
                    case 1:
                        altas(s, dir + filename);
                        break;
                    case 2:
                        listar(dir + filename);
                        break;
                    case 3:
                        listarMedia(dir + filename);
                        break;
                    case 4:
                        verMayorMenor(dir + filename);
                        break;
                }
            }
        } while (opt != nMaxOpt);
        
        System.out.println("FINAL DEL PROGRAMA");
        s.close();
    }

}
