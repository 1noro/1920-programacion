package resta2num;
import java.util.Scanner;
public class Resta2num {
	public static void main(String[] args) {
		int n1 = 0; // 4bytes
		int n2 = 0; 
		int resultado = 0;
		Scanner entrada = new Scanner(System.in);
		
		System.out.print("Número 1: ");
		n1 = entrada.nextInt();
		System.out.print("Número 2: ");
		n2 = entrada.nextInt();
		
		if (n1 > n2) {
			resultado = n1 - n2;
		} else {
			resultado = n2 - n1;
		}

		System.out.print("Resta: " + resultado + "\n");
		
		entrada.close();
	}
}
