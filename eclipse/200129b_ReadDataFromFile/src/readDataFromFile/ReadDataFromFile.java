package readDataFromFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ReadDataFromFile {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String dir = "/opt/archivosJava/";
		String filename = "personas.dat";
		String linea = "", texto = "";
		String nombre = "", edad = "";
		
		// ESCRIBIR ARCHIVO 
		try {
			//TRUE para que no sobreescriba
			FileWriter fw = new FileWriter(dir + filename, true);
			System.out.print("> Nombre ('*' para fin): ");
			nombre = s.nextLine();
			while (!nombre.equals("*")) {
				System.out.print("> Edad: ");
				edad = s.nextLine();
				fw.write(nombre + ' ' + edad + '\n');
				System.out.print("> Nombre ('*' -> fin): ");
				nombre = s.nextLine();
			}
			fw.close();
		} catch(IOException ioe) {}
		s.close();
		
		// LEER ARCHIVO
		try {
			FileReader fr = new FileReader(dir + filename);
			BufferedReader br = new BufferedReader(fr);
			linea = br.readLine();
			while (linea != null) {
				texto += linea + '\n';
				linea = br.readLine();
			}
			br.close();
			fr.close();
		} catch(IOException ioe) {}
		
		System.out.println();
		System.out.println("#Texto leido:");
		System.out.println(texto);
	}

}
