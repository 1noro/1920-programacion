package mantLibros;

public class Utils {
    public static String tabular(String in, int longitud) {
        String out = "";
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in.length()) out += in.charAt(i);
                else out += ' ';
        }
        return out;
    }
    
    public static String tabular(int in, int longitud) {
        String out = "";
        String in_str = Integer.toString(in);
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in_str.length()) out += in_str.charAt(i);
            else out += ' ';
        }
        return out;
    }
    
    public static String tabular(float in, int longitud) {
        String out = "";
        String in_str = Float.toString(in);
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in_str.length()) out += in_str.charAt(i);
            else out += ' ';
        }
        return out;
    }
    
    public static String generarLineaTabulada(Libro l) {
        return  tabular(l.getNumero(), 4) + " " +
                tabular(l.getIsbn(), 13) + " " +
                tabular(l.getTitulo(), 25) + " " +
                tabular(l.getAutor(), 25) + " " +
                tabular(l.getEdicion(), 4) + " " +
                tabular(l.getPrecio(), 8);
    }
}
