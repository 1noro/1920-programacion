package mantLibros;

import java.util.Scanner;

public class Teclado {
    
    public static String readStringLimit(Scanner s, String msg, int limite) {
        String out = "";
        do {
            System.out.print(msg + " (" + limite + " char max): ");
            out = s.nextLine();
        } while(out.length() > limite);
        return out;
    }
    
    public static String readStringLimitStrict(Scanner s, String msg, int limite) {
        String out = "";
        do {
            System.out.print(msg + " (" + limite + " char justos): ");
            out = s.nextLine();
        } while(out.length() != limite);
        return out;
    }
    
    public static int readInt(Scanner s, String msg) {
        String in_str = "";
        int in = 0;
        boolean isInt = false;
        do {
            System.out.print(msg + " (int): ");
            in_str = s.nextLine();
            try {
                in = Integer.parseInt(in_str);
                isInt = true;
            } catch (NumberFormatException nfe) {
                System.out.println("# ERROR: No es un Integer.");
            }
        } while (!isInt);
        return in;
    }
    
    public static float readFloat(Scanner s, String msg) {
        String in_str = "";
        float in = 0;
        boolean isDouble = false;
        do {
            System.out.print(msg + " (float): ");
            in_str = s.nextLine();
            try {
                in = Float.parseFloat(in_str);
                isDouble = true;
            } catch (NumberFormatException nfe) {
                System.out.println("# ERROR: No es un Float.");
            }
        } while (!isDouble);
        return in;
    }
    
    public static double readDouble(Scanner s, String msg) {
        String in_str = "";
        double in = 0;
        boolean isDouble = false;
        do {
            System.out.print(msg + " (double): ");
            in_str = s.nextLine();
            try {
                in = Double.parseDouble(in_str);
                isDouble = true;
            } catch (NumberFormatException nfe) {
                System.out.println("# ERROR: No es un Double.");
            }
        } while (!isDouble);
        return in;
    }
    
    public static char readSN(Scanner s, String msg) {
        char in = ' ';
        do {
            System.out.print(msg + " (S/N): ");
            in = s.nextLine().toUpperCase().charAt(0);
        } while (in != 'S' && in != 'N');
        return in;
    }

}
