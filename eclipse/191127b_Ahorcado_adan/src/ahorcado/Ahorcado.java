package ahorcado;

//import java.io.IOException;
import java.util.Scanner;

public class Ahorcado {

	public static void main (String[]Args) {
		Scanner entrada = new Scanner (System.in);
		//vectores de letras correctas, vector de lo que se muestra, el juego acaba cuando se llena el vector de fallos o el de aciertos
		//El vector de los fallos tiene tantas posibilidades como letras tiene la frase.
		//teclea la palabra
		String palabra = "";
		int longitud = 0, i=0;
		boolean acierto = false;
		boolean sinterminar = true;
		char afirmacion = ' ';
		boolean ganar = false;
		boolean seguir = false;
		char adivinar = ' ';
		char vpalabra [];
		int fallopos = 1;
		
		
		do {
			System.out.println("Teclea la frase secreta");
			palabra = entrada.next().toLowerCase();
			vpalabra= palabra.toCharArray();
			longitud = palabra.length();
		
			char fallos [] = new char [longitud];
			char frase_a_adivinar [] = new char [longitud];
			
			for (i=0;i<longitud;i++) {
				frase_a_adivinar[i]='_';
				fallos [i] = '_';
			}
			frase_a_adivinar[0]=palabra.charAt(0);
			frase_a_adivinar[longitud-1]=palabra.charAt(longitud-1); //Esto son las reglas que le apeteció al profe
			fallos[0]=palabra.charAt(0);
			fallos[longitud-1]=palabra.charAt(longitud-1);
			for (i=0;i<longitud;i++) {
				System.out.print(frase_a_adivinar[i]+" "); //escribo la linea de la palabra a descubrir
			}
			System.out.println("");
			for (i=0;i<longitud;i++) {
				System.out.print(fallos[i]+" "); //escribo la linea de los fallos
			} System.out.println("");
		do {
			acierto=false;
			adivinar = entrada.next().toLowerCase().charAt(0);
			for (i=0;i<longitud;i++) {
				if (vpalabra[i]==adivinar) { //la palabra que adivino esta dentro la muestro
					frase_a_adivinar[i]=adivinar;
							vpalabra[i]='_';
							acierto = true;}
			}if (i>0 && acierto==false) {
				fallos[fallopos]=adivinar;
				fallopos+=1;}
			for (i=0;i<longitud;i++) {
				if (frase_a_adivinar[i]=='_') {//CUANDO HAY _ SE SALE PARA CONTINUAR
					i=longitud+2;
					}}
			if (i==longitud) {
				sinterminar=false;
				ganar = true;} //salió porque las letras están completas
			for (i=0;i<longitud;i++) {
				if (fallos[i]=='_') {//CUANDO sigue habiendo intentos
					i=longitud+1;}
			}
			if (i==longitud) {
				sinterminar=false; //no hay más intentos
			}
			for (i=0;i<longitud;i++) {
				System.out.print(frase_a_adivinar[i]+" "); //escribo la linea de la palabra a descubrir
			}
			System.out.println("");
			for (i=0;i<longitud;i++) {
				System.out.print(fallos[i]+" "); //escribo la linea de los fallos
			} System.out.println("");
		} while (sinterminar==true);
		sinterminar=true;
		System.out.println("");
		if (ganar==true) {
			System.out.println("Bien, has averiguado la palabra");
		}else {
			System.out.println("Vuelve a intentarlo, has fallado.");
		}
		ganar= false;
		System.out.println("Quieres seguir jugando? S/N");
		afirmacion = entrada.next().toLowerCase().charAt(0);
		switch (afirmacion) {
		case 's':
			seguir = true;
			break;
		case 'n' :
			seguir = false;
			break;
			default:
				seguir = false;
		}
		System.out.println("\n\n\n\n\n");
	} while (seguir==true);
		entrada.close();
	}

}
