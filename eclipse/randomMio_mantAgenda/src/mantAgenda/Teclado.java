package mantAgenda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Teclado {
    
    public static String leerString() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str = br.readLine();
        return str;
    }
    
    public static int leerInt() throws IOException {
        int entero = 0;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        try {
            entero = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            entero = Integer.MIN_VALUE;
        }
        return entero;
    }
    
    public static char leerChar() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        char caracter = ' ';
        String s = br.readLine();
        caracter = s.charAt(0);
        return caracter;
    }
    
    public static void leerSalto() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        br.readLine();
    }
    
}
