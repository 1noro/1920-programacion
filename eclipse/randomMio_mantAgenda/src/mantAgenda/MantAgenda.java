package mantAgenda;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class MantAgenda {
    
    public static final int nMaxOpt = 5;
    public static final String dir = "/opt/jarchivos/";
    public static final String filename = "agenda.dat";
    
    // --- UTILIDADES ----------------------------------------------------------
    public static String tabular(String in, int longitud) {
        String out = "";
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in.length()) out += in.charAt(i);
                else out += ' ';
        }
        return out;
    }
    
    public static String tabular(int in, int longitud) {
        String out = "";
        String in_str = Integer.toString(in);
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in_str.length()) out += in_str.charAt(i);
            else out += ' ';
        }
        return out;
    }
    
    // --- INTERFAZ DE USUARIO -------------------------------------------------
    public static void printMenu() {
        System.out.println();
        System.out.println("# --- MENU ---");
        System.out.println(" 1 - Dar de alta");     // altas
        System.out.println(" 2 - Dar de baja");     // bajas
        System.out.println(" 3 - Editar alta");     // modificaciones
        System.out.println(" 4 - Listar altas");    // listados
        System.out.println();
        System.out.println(" " + nMaxOpt + " - Fin");
        System.out.println();
    }
    
    public static int readOpt(Scanner s, int max) {
        int out = 0;
        do {
            System.out.print("Teclee opción (1-" + max + "): ");
            out = s.nextInt();
            s.nextLine();
        } while (out < 1 || out > max);
        return out;
    }
    
    // --- ALTAS ---------------------------------------------------------------
    public static void altas(String fullDir) {
        Persona personaCheck = new Persona();
        Persona personaVacia = new Persona(); // lo creo simplemente por claridad del código
        char confirmar = 's'; // no ahy motivo, es por definir algo
        char continuar = 's'; // para que funcione el contnue
        int codigo = 0;
        String nombre;
        int edad = 0;
        int nuevaPosicion = 0;

        System.out.println("\n# --- ALTAS ---");
        try {
            RandomAccessFile f = new RandomAccessFile(fullDir, "rw");
            
            do {
                do {
                    System.out.print("Introduce un código (int): ");
                    codigo = Teclado.leerInt();
                } while (codigo == Integer.MIN_VALUE);
                
                nuevaPosicion = codigo * Persona.LONGITUD;
                f.seek(nuevaPosicion);
                personaCheck.leerDeArchivo(f);
                
                // comprobamos si la posición dada está llena o no
                if(personaCheck.getCodigo() != 0 && nuevaPosicion < f.length()) {
                    System.out.println("# ERROR: Este código ya está lleno en el archivo.");
                    continue;
                } else {
                    
                    do {
                        System.out.print("Introduce el nombre (max 20 char): ");
                        nombre = Teclado.leerString();
                    } while (nombre.length() > 20);
                    
                    System.out.print("Introduce la edad (int): ");
                    edad = Teclado.leerInt();
                    
                    System.out.println("# Se va a guardar la siguiente persona:");
                    System.out.println("# PERSONA: " + codigo + " - " + nombre + " - " + edad);
                    do {
                        System.out.print("Desea guardar esta persona (s/n)? ");
                        confirmar = Character.toLowerCase(Teclado.leerChar());
                    } while (confirmar != 's' && confirmar != 'n');
                    
                    if (confirmar == 's') {
                        // Si la nueva posición a guardar es mayor a la ultima del archivo, me posiciono al final
                        if (nuevaPosicion > f.length()) f.seek(f.length());
                        // Escribo personas vacías hasta llegar a la posición en la que voy a guardar mi nueva persona
                        while (nuevaPosicion > f.length()) personaVacia.grabarEnArchivo(f);
                        // me situo en la nueva posición (donde voy a guardar)
                        f.seek(nuevaPosicion);
                        
                        Persona personaAGuardar = new Persona(codigo, nombre, edad);
                        // guardo la nueva persona en su posición correcta
                        personaAGuardar.grabarEnArchivo(f);
                        System.out.println("# Persona guardada");
                    }
                }
                
                do {
                    System.out.print("Desea continuar (s/n)? ");
                    continuar = Character.toLowerCase(Teclado.leerChar());
                } while (continuar != 's' && continuar != 'n');
                
            } while (continuar == 's');
            
            f.close();
            
        } catch (IOException e) {
            // e.printStackTrace();
            System.out.println("# ERROR: Fallo al acceder o al guardar en el archivo.");
        }
    }
    
    // --- BAJAS ---------------------------------------------------------------
    public static void bajas(String fullDir) {
        Persona personaCheck = new Persona();
        Persona personaVacia = new Persona(); // lo creo simplemente por claridad del código
        char confirmar = 's'; // no ahy motivo, es por definir algo
        char continuar = 's'; // para que funcione el contnue
        int codigo = 0;
        int posicionDada = 0;

        System.out.println("\n# --- BAJAS ---");
        try {
            RandomAccessFile f = new RandomAccessFile(fullDir, "rw");
            
            do {
                do {
                    System.out.print("Introduce un código (int): ");
                    codigo = Teclado.leerInt();
                } while (codigo == Integer.MIN_VALUE);
                
                posicionDada = codigo * Persona.LONGITUD;
                f.seek(posicionDada);
                personaCheck.leerDeArchivo(f);
                
                // comprobamos si la posición dada está llena o no
                if(personaCheck.getCodigo() != 0 && posicionDada < f.length()) {
                    System.out.println("# Se va a borrar la siguiente persona:");
                    System.out.println("# PERSONA: " + personaCheck.getCodigo() + " - " + personaCheck.getNombre() + " - " + personaCheck.getEdad());
                    do {
                        System.out.print("Desea borrar esta persona (s/n)? ");
                        confirmar = Character.toLowerCase(Teclado.leerChar());
                    } while (confirmar != 's' && confirmar != 'n');
                    
                    if (confirmar == 's') {
                        // me situo en la nueva posición (donde voy a guardar)
                        f.seek(posicionDada);
                        personaVacia.grabarEnArchivo(f);
                        System.out.println("# Persona borrada");
                    }
                } else {
                    System.out.println("# ERROR: Este código no está dado de alta en el archivo.");
                    continue;
                }
                
                do {
                    System.out.print("Desea continuar (s/n)? ");
                    continuar = Character.toLowerCase(Teclado.leerChar());
                } while (continuar != 's' && continuar != 'n');
                
            } while (continuar == 's');
            
            f.close();
            
        } catch (IOException e) {
            // e.printStackTrace();
            System.out.println("# ERROR: Fallo al acceder o al guardar en el archivo.");
        }
    }
    
    // --- EDITAR --------------------------------------------------------------
    public static void modificar(String fullDir) {
        Persona personaCheck = new Persona();
        char confirmar = 's'; // no ahy motivo, es por definir algo
        char confirmarEditar = 's'; // no ahy motivo, es por definir algo
        char continuar = 's'; // para que funcione el contnue
        int codigo = 0;
        String nombre;
        int edad = 0;
        int nuevaPosicion = 0;

        System.out.println("\n# --- MODIFICAR ---");
        try {
            RandomAccessFile f = new RandomAccessFile(fullDir, "rw");
            
            do {
                do {
                    System.out.print("Introduce un código (int): ");
                    codigo = Teclado.leerInt();
                } while (codigo == Integer.MIN_VALUE);
                
                nuevaPosicion = codigo * Persona.LONGITUD;
                f.seek(nuevaPosicion);
                personaCheck.leerDeArchivo(f);
                
                // comprobamos si la posición dada está llena o no
                if(personaCheck.getCodigo() != 0 && nuevaPosicion < f.length()) {
                    
                    System.out.println("# Se va a editar la siguiente persona:");
                    System.out.println("# PERSONA: " + personaCheck.getCodigo() + " - " + personaCheck.getNombre() + " - " + personaCheck.getEdad());
                    do {
                        System.out.print("Desea editar esta persona (s/n)? ");
                        confirmarEditar = Character.toLowerCase(Teclado.leerChar());
                    } while (confirmarEditar != 's' && confirmarEditar != 'n');
                    
                    if (confirmarEditar == 's') {
                        do {
                            System.out.print("Introduce el nuevo nombre (max 20 char): ");
                            nombre = Teclado.leerString();
                        } while (nombre.length() > 20);
                        
                        System.out.print("Introduce la nueva edad (int): ");
                        edad = Teclado.leerInt();
                        
                        do {
                            System.out.println("# Se van a guardar los siguientes cambios:");
                            System.out.println("# PERSONA: " + codigo + " - " + nombre + " - " + edad);
                            System.out.print("Desea guardar estos cambios (s/n)? ");
                            confirmar = Character.toLowerCase(Teclado.leerChar());
                        } while (confirmar != 's' && confirmar != 'n');
                        
                        if (confirmar == 's') {
                            // me situo en la nueva posición (donde voy a guardar)
                            f.seek(nuevaPosicion);
                            
                            Persona personaAGuardar = new Persona(codigo, nombre, edad);
                            // guardo la nueva persona en su posición correcta
                            personaAGuardar.grabarEnArchivo(f);
                            System.out.println("# Los cambios se han guardado en la persona seleccionada.");
                        }
                    }
                } else {
                    System.out.println("# ERROR: Este código no está dado de alta en el archivo.");
                    continue;
                }
                
                do {
                    System.out.print("Desea continuar (s/n)? ");
                    continuar = Character.toLowerCase(Teclado.leerChar());
                } while (continuar != 's' && continuar != 'n');
                
            } while (continuar == 's');
            
            f.close();
            
        } catch (IOException e) {
            // e.printStackTrace();
            System.out.println("# ERROR: Fallo al acceder o al guardar en el archivo.");
        }
    }
    
    // --- LISTADOS ------------------------------------------------------------    
    public static void listar(String fullDir) {
        boolean finalDeArchivo = false;
        Persona personaActual = new Persona();
        System.out.println("\n# --- Listado ---");
        try {
            RandomAccessFile f = new RandomAccessFile(fullDir, "rw");  // probar solo con r
            finalDeArchivo = personaActual.leerDeArchivo(f);
            do {
                if(personaActual.getCodigo() != 0)
                    System.out.println(tabular(personaActual.getCodigo(), 4) + " - " + personaActual.getNombre() + " - " + tabular(personaActual.getEdad(), 3));
                finalDeArchivo = personaActual.leerDeArchivo(f);
            } while (!finalDeArchivo);
            f.close();
        } catch(IOException e) {
            // e.printStackTrace();
            System.out.println("# ERROR: Fallo al acceder al archivo.");
        }
    }
    
    public static void listarConVacios(String fullDir) {
        boolean finalDeArchivo = false;
        Persona personaActual = new Persona();
        System.out.println("\n# --- Listado ---");
        try {
            RandomAccessFile f = new RandomAccessFile(fullDir, "rw");  // probar solo con r
            finalDeArchivo = personaActual.leerDeArchivo(f);
            do {
                System.out.println(tabular(personaActual.getCodigo() * Persona.LONGITUD, 4) + " - " + tabular(personaActual.getCodigo(), 4) + " - " + personaActual.getNombre() + " - " + tabular(personaActual.getEdad(), 3));
                finalDeArchivo = personaActual.leerDeArchivo(f);
            } while (!finalDeArchivo);
            f.close();
        } catch(IOException e) {
            // e.printStackTrace();
            System.out.println("# ERROR: Fallo al acceder al archivo.");
        }
    }
    
    // --- MAIN ----------------------------------------------------------------
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int opt = 0;
        
        do {
            printMenu();
            opt = readOpt(s, nMaxOpt);
            if (opt != nMaxOpt) {
                switch (opt) {
                    case 1:
                        altas(dir + filename);
                        break;
                    case 2:
                        bajas(dir + filename);
                        break;
                    case 3:
                        modificar(dir + filename);
                        break;
                    case 4:
                        listarConVacios(dir + filename);
                        break;
                }
            }
        } while (opt != nMaxOpt);
        
        System.out.println("¡¡# FINAL DEL PROGRAMA #!!");
        s.close();
    }

}
