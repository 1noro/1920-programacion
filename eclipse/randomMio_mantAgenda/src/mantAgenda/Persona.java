package mantAgenda;

import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Persona {
    
    // final int LONGNOMBRE = 25;
    
    private static final int PREFIJO_STRING = 2; // PREFIJO_UTF
    private static final int LONG_INT = 4;
    // private static final int LONG_FLOAT = 4;
    
    private static final int LONG_CODIGO = LONG_INT;
    private static final int LONG_NOMBRE = 25;
    private static final int LONG_EDAD = LONG_INT;
    
    public static final int LONGITUD = LONG_CODIGO + PREFIJO_STRING + LONG_NOMBRE + LONG_EDAD;
    
    private int codigo;
    private String nombre;
    private int edad;

    Persona(int codigo, String nombre, int edad) {
        this.codigo = codigo;
        // con trim() borramos los espacios sobrantes al principio y al final del string
        this.nombre = nombre.trim();
        this.edad = edad;
    }
    
    Persona() {
        this.codigo = 0;
        // con trim() borramos los espacios sobrantes al principio y al final del string
        this.nombre = "NOMBRE VACIO".trim();
        this.edad = 0;
    }
    
    public int getCodigo() {
        return this.codigo;
    }
    
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        // con trim() borramos los espacios sobrantes al principio y al final del string
        this.nombre = nombre.trim();
    }
    
    public int getEdad() {
        return this.edad;
    }
    
    public void setEdad(int edad) {
        this.edad = edad;
    }
    
//    public String blancos(int numblancos) {
//        char[] blancos = new char[numblancos];
//        for(int i = 0; i < numblancos; i++) blancos[i] = ' ';
//        String sblancos = new String(blancos);
//        return sblancos;
//    }
    
    public String tabular(String in, int longitud) {
        String out = "";
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in.length()) out += in.charAt(i);
                else out += ' ';
        }
        return out;
    }
    
//    public String getNombreConEspacios() {
//        String out = "";
//        int longRelleno = LONG_NOMBRE - this.nombre.length();
//        out = this.nombre + blancos(longRelleno);
//        return out;
//    }
    
    public String getNombreConEspacios() {
        return this.tabular(this.nombre, Persona.LONG_NOMBRE);
    }
    
    public void grabarEnArchivo(RandomAccessFile f) {
        try {
            f.writeInt(this.codigo);
//            System.out.println(">> '1234567890123456789012345'");
//            System.out.println(">> '" + this.getNombreConEspacios() + "'");
            f.writeUTF(this.getNombreConEspacios());
            f.writeInt(this.edad);
        } catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }
    
    public boolean leerDeArchivo(RandomAccessFile f) {
        boolean finArchivo = false;
        
        try {
            this.codigo = f.readInt();
            this.nombre = f.readUTF();
            this.edad = f.readInt();
        } catch(EOFException eofe) {
            finArchivo = true;
        } catch(IOException ioe) {
            System.out.println("Error: " + ioe.getMessage());
        }
        
        return finArchivo;
    }
    
    public void mostrarDatos(int t) {
        switch(t) {
        case 0:
            System.out.println("Nombre.....................: " + this.nombre);
            System.out.println("Edad.......................: " + this.edad);
            break;
        case 1:
            System.out.println("1.- Nombre.................: " + this.nombre);
            System.out.println("2.- Edad...................: " + this.edad);
            break;
        case 2:
            System.out.println(this.codigo + "\t" + this.nombre + "\t" + this.edad);
            break;
        }
    }
    
    @Override
    public String toString() {
        return "" + this.codigo + " - '" + this.nombre + "' - " + this.edad + "";
    }

}
