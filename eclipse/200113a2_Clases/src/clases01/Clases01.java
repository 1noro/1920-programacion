package clases01;
import java.io.IOException;
import java.util.Scanner;
public class Clases01 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		Persona p = new Persona();
		char c = ' ';
		
		System.out.print("Nombre: ");
		p.setNombre(s.nextLine());
		
		System.out.print("Edad: ");
		p.setEdad(s.nextInt());
		
		do {
			System.out.print("Sexo: ");
			try {
				c = Character.toUpperCase(((char) System.in.read()));
				while(System.in.read()!='\n'); //leer hasta pulsar enter
			} catch (IOException ioe) {
				System.out.println("error1");
			}
		} while (c != 'M' && c != 'F');
		p.setSexo(c);
		
		System.out.println();
		System.out.println("Nombre:\t" + p.getNombre());
		System.out.println("Edad:\t" + p.getEdad());
		System.out.println("Sexo:\t" + p.getSexo());
		
		s.close();
	}

}
