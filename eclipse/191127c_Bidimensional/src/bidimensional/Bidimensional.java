package bidimensional;

import java.util.Scanner;

public class Bidimensional {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int m[][] = new int[2][3];
		int a = 0, e = 0;
		int max = 0;
		
		for (a = 0; a < m.length; a++) {
			for (e = 0; e < m[0].length; e++) {
				System.out.print("Numero [" + a + "][" + e + "]: ");
				m[a][e] = s.nextInt();
			}
		}
		
		for (a = 0; a < m.length; a++) {
			for (e = 0; e < m[0].length; e++) {
				System.out.print(m[a][e] + ", ");
			}
			System.out.println();
		}
		
		for (a = 0; a < m.length; a++) {
			for (e = 0; e < m[0].length; e++) {
				if (m[a][e] > max) {
					max = m[a][e];
				}
			}
		}
		
		System.out.println("Maximo: " + max);
		
		s.close();

	}

}
