package ordenarmetodoburbuja_20191002a;

public class OrdenarMetodoBurbuja_20191002a {

	public static void main(String[] args) {
		int tam = 12, i = 0, e = 0, aux = 0;
//		int v[] = { 2,  2,  3,  4,  5,  6,  7,  8,  7, 10, 11,  7};
		int v[] = { 3,  4, 66,  1,  2,  9, 88, 21, 36, 10,  1, 57};
		
		// Visualizamos el array original
		System.out.print("v[" + tam + "] = [");
		for (i = 0; i < (tam - 1); i++)
			System.out.print(v[i] + ", ");
		System.out.print(v[i] + "]\n");
		
		// Ordenamos el array con el metodo de la Burbuja
		for (i = 0; i < (tam - 1); i++) 
			for (e = (i + 1); e < tam; e++) {
				System.out.println(i + " " + e);
				if (v[i] < v[e]) { // (< menor a mayor), (> mayor a menor)
					aux = v[i];
					v[i] = v[e];
				    v[e] = aux;
				}}
		
		// Visualizamos el array ordenado
		System.out.print("v[" + tam + "] = [");
		for (i = 0; i < (tam - 1); i++)
			System.out.print(v[i] + ", ");
		System.out.print(v[i] + "]\n");
		
	}

}
