package rodriguezgraciaalfredo2;

public class Coche {
    
    private String matricula;
    private String marca;
    private String modelo;
    private String gama;
    private double precio;
    
    Coche(String matricula, String marca, String modelo, String gama, double precio) {
        this.matricula = matricula;
        this.marca = marca;
        this.modelo = modelo;
        this.gama = gama;
        this.precio = precio;
    }
    
    public String getMatricula() {
        return matricula;
    }
    
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    
    public String getMarca() {
        return marca;
    }
    
    public void setMarca(String marca) {
        this.marca = marca;
    }
    
    public String getModelo() {
        return modelo;
    }
    
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
    public String getGama() {
        return gama;
    }
    
    public void setGama(String gama) {
        this.gama = gama;
    }
    
    public double getPrecio() {
        return precio;
    }
    
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    public String toString() {
        return this.matricula + "\t" + this.marca + "\t" + this.modelo + "\t" + this.gama + "\t" + Double.toString(this.precio);
    }

}
