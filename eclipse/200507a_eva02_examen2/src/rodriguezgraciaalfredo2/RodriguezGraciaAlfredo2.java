package rodriguezgraciaalfredo2;

import java.util.Scanner;

public class RodriguezGraciaAlfredo2 {
    
    public static final int nMaxOpt = 3;

    // --- INTERFAZ DE USUARIO -------------------------------------------------
    public static void printMenu(int max) {
        System.out.println("");
        System.out.println("--- MENU ---");
        System.out.println("");
        System.out.println(" 1 - Altas");
        System.out.println(" 2 - Consultas");
        System.out.println("");
        System.out.println(" " + max + " - Fin");
        System.out.println("");
    }

    public static int readOpt(Scanner s, int max) {
        int out = 0;
        do {
            System.out.print("Teclee opción (1-" + max + "): ");
            out = s.nextInt();
            s.nextLine();
        } while (out < 1 || out > max);
        return out;
    }
    
    // --- ALTAS ---------------------------------------------------------------
    public static String readStr(Scanner s, int limiteInf, int limiteSup, String motivo) {
        String in = "";
        do {
            System.out.print(motivo + " (entre " + limiteInf + " y " + limiteSup + " caracteres): ");
            in = s.nextLine();
        } while (in.length() < limiteInf || in.length() > limiteSup);
        return in;
    }

    public static String readGama(Scanner s) {
        String out = "";
        String in_str = "";
        char in = ' ';
        
        do {
            System.out.print("Gama Alta, Media, Baja (a/m/b): ");
            in_str = s.nextLine().toUpperCase();
        } while (!in_str.equals("A") && !in_str.equals("M") && !in_str.equals("B"));
        in = in_str.charAt(0);
        switch (in) {
            case 'A':
                out = "Alta";
                break;
            case 'M':
                out = "Media";
                break;
            case 'B':
                out = "Baja";
                break;
        }
        
        return out;
    }
    
    public static double readDouble(Scanner s) {
        String in_str = "";
        double in = 0;
        boolean isDouble = false;
        do {
            System.out.print("Precio (numérico, con decimales): ");
            in_str = s.nextLine();
            try {
                in = Double.parseDouble(in_str);
                isDouble = true;
            } catch (NumberFormatException nfe) {
                System.out.println("# ERROR DE FORMATO: No es un número, con decimales.");
            }
        } while (!isDouble);
        return in;
    }
    
    public static void altas(Scanner s, Coche coches[]) {
        String matricula = "";
        String marca = "";
        String modelo = "";
        String gama = "";
        double precio = 0;
        System.out.println("\n--- ALTAS ---");
        for (int i = 0; i < coches.length; i++) {
            matricula = readStr(s, 7, 7, "Matrícula").toUpperCase();
            marca = readStr(s, 0, 10, "Marca");
            modelo = readStr(s, 0, 15, "Modelo");
            gama = readGama(s);
            precio = readDouble(s);
            coches[i] = new Coche(matricula, marca, modelo, gama, precio);
//            System.out.println("\n# GUARDADO:");
//            System.out.println(coches[i].toString());
        }
    }
    
    // --- CONSULTA ------------------------------------------------------------
    public static void consulta(Scanner s, Coche coches[]) {
        boolean encontrado = false;
        String matricula = "";
        int i = 0;
        System.out.println("\n--- CONSULTA ---");
        matricula = readStr(s, 7, 7, "Escribe la matrícula a buscar").toUpperCase();
        for (i = 0; i < coches.length; i++) {
            if (coches[i] != null && coches[i].getMatricula().equals(matricula)) {
                System.out.println("\n# RESULTADO:");
                System.out.println(coches[i].toString());
                encontrado = true;
            }
        }
        if (!encontrado) {
            System.out.println("\n# ERROR: No existe ningún coche con la matrícula " + matricula + ".");
        }
    }
    
    // --- MAIN ----------------------------------------------------------------
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int opt = 0;
        Coche coches[] = new Coche[4];
        
        do {
            printMenu(nMaxOpt);
            opt = readOpt(s, nMaxOpt);
            if (opt != nMaxOpt) {
                switch (opt) {
                    case 1:
                        altas(s, coches);
                        break;
                    case 2:
                        consulta(s, coches);
                        break;
                }
            }
        } while (opt != nMaxOpt);
        
        System.out.println("FINAL DEL PROGRAMA");
        s.close();
    }
    
}
