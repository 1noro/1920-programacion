package suma2num;

import java.util.Scanner;

public class Suma2num {

	public static void main(String[] args) {
		int n1 = 0; // 4bytes
		int n2 = 0; 
		Scanner entrada = new Scanner(System.in);
		
		System.out.print("Número 1: ");
		n1 = entrada.nextInt();
		System.out.print("Número 2: ");
		n2 = entrada.nextInt();
		
		System.out.print(n1 + " + " + n2 + " = " + (n1 + n2) + "\n");
//		System.out.printf("%d + %d = %d \n", n1, n2, n1 + n2);
		
		entrada.close(); 
	}

}
