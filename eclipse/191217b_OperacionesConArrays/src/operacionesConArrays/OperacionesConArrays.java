package operacionesConArrays;

import java.util.InputMismatchException;
import java.util.Scanner;

public class OperacionesConArrays {
	
	static void mostrarMenu() {
		System.out.println("\n MENU");
		System.out.println("=====================");
		System.out.println("1 - Llenar vector");
		System.out.println("2 - Visualizar vector");
		System.out.println("3 - Ordenar vector");
		System.out.println("4 - Búsqueda dicotómica");
		System.out.println("5 - Fin");
	}
	
	static int leerOpcion(Scanner s) {
		int opcion = 0; 
		do {
			try {
				System.out.print("Teclea una opción: ");
				opcion = s.nextInt();
			} catch (InputMismatchException ime) {opcion = 0; s.nextLine();}
		} while (opcion < 1 || opcion > 5);
		return opcion;
	}
	
	static int[] llenarVector(Scanner s, int v[]) {
		int i = 0;
		int vOut[] = new int[v.length];
		System.out.println();
		for (i = 0; i < v.length; i++) {
			System.out.print("Valor para la posición " + i + " :");
			vOut[i] = s.nextInt();
		}
		return vOut;
	}
	
	static void visualizarVector(int v[]) {
		int i = 0;
		System.out.print("\nv = [");
		for (i = 0; i < (v.length - 1); i++) System.out.print(v[i] + ", ");
		System.out.println(v[v.length - 1] + "]");
	}
	
	static int[] ordenarVector(int v[]) {
		int i = 0, e = 0, aux = 0, tam = v.length;
		for (i = 0; i < (tam - 1); i++) {
			for (e = (i + 1); e < tam; e++) {
				if (v[i] > v[e]) { // (> menor a mayor), (< mayor a menor)
					aux = v[i];
					v[i] = v[e];
				    v[e] = aux;
				}
			}
		}
		System.out.println("\n# Vector ordenado de menor a mayor.");
		return v;
	}
	
	static void busquedaDicotomica(Scanner s, int v[]) {
		boolean encontrado = false;
		int busqueda = 0, iz = 0, im = 0, id = v.length - 1;
		System.out.print("\nNúmero a buscar: ");
		busqueda = s.nextInt();
		
		do {
			im = (int) (iz + id)/2;
			if (busqueda == v[im]) 
				encontrado = true;
			else {
				if (busqueda < v[im]) 
					id = im - 1;
				else 
					iz = im + 1;
			}
		} while ((iz <= id) && !encontrado);
		
		if (encontrado) System.out.println("# Número '" + busqueda + "' encontrado :)");
			else System.out.println("# Número '" + busqueda + "' NO encontrado :(");
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int opcion = 0;
		int v[] = new int[10];
		boolean ordenado = false;
		
		do {
			mostrarMenu();
			opcion = leerOpcion(s);
			switch (opcion) {
				case 1:
					v = llenarVector(s, v);
					break;
				case 2:
					visualizarVector(v);
					break;
				case 3:
					v = ordenarVector(v);
					ordenado = true;
					break;
				case 4:
					if (ordenado) busquedaDicotomica(s, v);
						else System.out.println("\nError: El vector no está ordenado.");
					break;
			}
		} while (opcion != 5);
		
		s.close();
	}

}
