package ordenarnombres;

import java.util.Scanner;

public class OrdenarNombres {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String a[] = new String[7];
		String aux = "";
		int i = 0, e = 0;
		
		for (i = 0; i < a.length; i++) {
			System.out.print("Nombre " + (i + 1) + ": ");
			a[i] = entrada.nextLine();
		}
		entrada.close();
		
		System.out.print("a = [");
		for (i = 0; i < (a.length - 1); i++) System.out.print(a[i] + ", ");
		System.out.println(a[(a.length - 1)] + "]");
		
		for (i = 0; i < (a.length - 1); i++) 
			for (e = (i + 1); e < a.length; e++) {
				if (a[i].compareToIgnoreCase(a[e]) > 0) { // (> menor a mayor), (< mayor a menor)
					aux = a[i];
					a[i] = a[e];
				    a[e] = aux;
				}}
		
		System.out.print("a = [");
		for (i = 0; i < (a.length - 1); i++) System.out.print(a[i] + ", ");
		System.out.println(a[(a.length - 1)] + "]");
	}

}
