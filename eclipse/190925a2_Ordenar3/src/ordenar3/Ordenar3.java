package ordenar3;

import java.util.Scanner;

public class Ordenar3 {

	public static void main(String[] args) {
		int a = 0, b = 0, c = 0, aux = 0;
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.print("Número 1: ");
		a = entrada.nextInt();
		System.out.print("Número 2: ");
		b = entrada.nextInt();
		System.out.print("Número 3: ");
		c = entrada.nextInt();
		
		entrada.close();
		
		if (a > b) {
			// a <-> b
			aux = a;
			a = b;
			b = aux;
		}
		
		if (a > c) {
			// a <-> c
			aux = a;
			a = c;
			c = aux;
		}
		
		if (b > c) {
			// b <-> c
			aux = b;
			b = c;
			c = aux;
		}
		
		System.out.print(a + ", " + b + ", " + c);
		
	}

}
