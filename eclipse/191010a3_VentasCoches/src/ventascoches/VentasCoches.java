package ventascoches;

public class VentasCoches {

	public static void main(String[] args) {
		int im = 0, ic = 0;
		int s0 = 0, s1 = 0, s2 = 0;
		
		int mvc[][] = {
				{  8,   9,   6},
				{ 12,   8,  96},
				{ 25,  36,  41},
				{ 15,  17,  52},
				{ 12,  23,   5},
				{  3,   2,   5},
				{ 10,   9,  21},
				{ 41,  32,  14},
				{ 15,   6,  24},
				{  7,   5,   3},
				{ 47,  56,  12},
				{ 36,  47,  95}
		};
		
		
		// visualizamos la matriz
		for (im = 0; im < 12; im++) {
			for (ic = 0; ic < 3; ic++) {
				System.out.print(mvc[im][ic] + "\t");
			}
			System.out.print("\n");
		}
		
		//calculamos la suma
		for (im = 0; im < 12; im++) {
			s0 += mvc[im][0];
			s1 += mvc[im][1];
			s2 += mvc[im][2];
		}
		
		System.out.print("\nTotales:\n");
		System.out.print(s0 + "\t" + s1 + "\t" + s2 + "\n");
		
		// visulaizamos la suma
		System.out.print("\nLa marca (columna) con mayores ventas es ");
		
		if (s0 > s1 && s0 > s2) 
			System.out.println("la 1 (" + s0 + ")");
		else 
			if (s1 > s0 && s1 > s2)
				System.out.println("la 2 (" + s1 + ")");
			else 
				System.out.println("la 3 (" + s2 + ")");
	}

}
