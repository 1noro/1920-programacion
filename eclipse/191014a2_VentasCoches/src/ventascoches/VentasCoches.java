// visualizar 

package ventascoches;

public class VentasCoches {

	public static void main(String[] args) {
		int im = 0, ic = 0;
		
		String mes[] = {
				"enero     ",
				"febrero   ",
				"marzo     ",
				"abril     ",
				"mayo      ",
				"junio     ",
				"julio     ",
				"agosto    ",
				"septiembre",
				"octubre   ",
				"noviembre ",
				"diciembre "
		};
		
		String marca[] = {
				"BMW ", 
				"Ford", 
				"Seat"
		};
		
		int mvc[][] = {
				{  8,   9,   6},
				{ 12,   8,  96},
				{ 25,  36,  41},
				{ 15,  17,  52},
				{ 12,  23,   5},
				{  3,   2,   5},
				{ 10,   9,  21},
				{ 41,  32,  14},
				{ 15,   6,  24},
				{  7,   5,   3},
				{ 47,  56,  12},
				{ 36,  47,  95}
		};
		
		int vmes[] = new int[12];
		
		
		// visualizamos la matriz
		System.out.print("          \t");
		for (ic = 0; ic < 3; ic++)
			System.out.print(marca[ic].toUpperCase() + "\t");
		System.out.print("\n");
		System.out.println("-------------------------------------");
			
		for (im = 0; im < 12; im++) {
			System.out.print(mes[im].toUpperCase() + "\t");
			for (ic = 0; ic < 3; ic++)
				System.out.print(mvc[im][ic] + "\t");
			System.out.print("\n");
		}
		System.out.println("-------------------------------------");
		
	}

}
