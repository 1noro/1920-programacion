package paisGuerra;

public class Instancia {
    
    protected int id;
    protected String nombre;
    
    Instancia () {
        this.id = 0;
        this.nombre = "";
    }
    
    Instancia (int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String toString() {
        return this.id + "\t" + this.nombre;
    }
    
    public String toStringMultiline() {
        return this.id + "\n" + this.nombre + "\n";
    }

}
