package calcularDNI;

import java.util.Scanner;

public class CalcularDNI {
	
	public static char letraDNI(int n) {
		String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
		return letras.charAt(n % 23);
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int n = 0;
		
		do {
			System.out.print("Introduce tu número del DNI sin la letra: ");
			n = s.nextInt();
		} while (n > 99999999);
		
		System.out.println("Resultado: " + n + letraDNI(n));
		
		s.close();
	}

}
