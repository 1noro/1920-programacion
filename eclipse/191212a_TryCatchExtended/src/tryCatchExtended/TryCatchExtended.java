package tryCatchExtended;
import java.io.IOException;
import java.util.Scanner;
public class TryCatchExtended {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int n1 = 0, n2 = 0, d = 0;
		char otro = ' ';
		
		do {
			System.out.print("n1? ");
			n1 = s.nextInt();
			System.out.print("n2? ");
			n2 = s.nextInt();
			
			try {
				d = n1/n2;
			} catch (ArithmeticException ae) { //hay excepciones que no se importan
				d = 0;
			}
			
			System.out.println("Division: " + d);

			do {
				try {
					System.out.println("Otra vez? (s/n)");
					otro = Character.toLowerCase((char) System.in.read());
					while(System.in.read()!='\n');
				} catch (IOException ioe) {}
			} while (otro != 's' && otro != 'n');
		} while (otro == 's');
		s.close();
	}

}
