package adivinoCutre;

import java.util.Scanner;

public class AdivinoCutre {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int i = 0, e = 0;
		int tamPant = 200;
		
		int jugadores = 0;
		String palabraOri = "";
		int adivino = 0;
		
		String palabras[];
		boolean jugando[];
		int votos[];

		System.out.print("Nº de jugadores?: ");
		jugadores = s.nextInt();
		
		palabras = new String[jugadores];
		jugando = new boolean[jugadores];
		votos = new int[jugadores];
		
		System.out.print("Introduce la palabra original: ");
		palabraOri = new Scanner(System.in).nextLine();
		
		System.out.print("Selecciona el adivino entre 1 y "+jugadores+": ");
		adivino = s.nextInt();
		adivino--;
		
		for (i = 0; i < jugadores; i++) {
			for (e = 0; e < tamPant; e++) System.out.println();
			System.out.println("Turno del jugador " + (i + 1) + " pulse INTRO");
			new Scanner(System.in).nextLine();
			for (e = 0; e < tamPant; e++) System.out.println();
			if (i != adivino) {
				System.out.println("Turno del jugador " + (i + 1) + "\nLa palabra es: " + palabraOri + "\npulse INTRO");
				new Scanner(System.in).nextLine();
			} else {
				System.out.println("Turno del jugador " + (i + 1) + "\nEres el adivino.");
				new Scanner(System.in).nextLine();
			}
		}

		s.close();
	}

}
