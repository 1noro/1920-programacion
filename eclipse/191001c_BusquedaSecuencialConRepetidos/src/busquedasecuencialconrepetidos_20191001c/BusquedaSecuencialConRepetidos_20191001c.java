package busquedasecuencialconrepetidos_20191001c;

import java.util.Scanner;

public class BusquedaSecuencialConRepetidos_20191001c {

	public static void main(String[] args) {
		Scanner e = new Scanner(System.in);
		int i = 0, tam = 12, busqueda = 0, encontrados = 0;
		
		int v[] = {2, 2, 3, 4, 5, 6, 7, 8, 7, 10, 11, 7};
//		int v[] = new int[tam];
//		
//		for (i = 0; i < tam; i++) {
//			System.out.print("Teclea el valor " + (i + 1) + ": ");
//			v[i] = e.nextInt();
//		}
		
		System.out.print("Número a buscar: ");
		busqueda = e.nextInt();

		for (i = 0; i < tam; i++) {
			if (busqueda == v[i]) 
				encontrados ++;
		}
		
		if (encontrados > 0) 
			System.out.println("Existen: " + encontrados);
		else
			System.out.println("No existe :(");
		
		e.close();
	}

}
