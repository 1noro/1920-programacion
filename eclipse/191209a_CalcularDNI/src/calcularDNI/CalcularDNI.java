package calcularDNI;

import java.util.Scanner;

public class CalcularDNI {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int n = 0;
		char letras[] = {'T', 'R', 'W', 'A', 'G', 'M', 
						 'Y', 'F', 'P', 'D', 'X', 'B', 
						 'N', 'J', 'Z', 'S', 'Q', 'V', 
						 'H', 'L', 'C', 'K', 'E'};
		
		do {
			System.out.print("Introduce tu número del DNI sin la letra: ");
			n = s.nextInt();
		} while (n > 99999999);
		
		System.out.println("Resultado: " + n + letras[n % 23]);
		
		s.close();
	}

}
