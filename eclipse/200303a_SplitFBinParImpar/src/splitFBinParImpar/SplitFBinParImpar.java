package splitFBinParImpar;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class SplitFBinParImpar {
	
	public static final String dir = "/opt/archivosJava/";
	public static final String juntofilename = "numbinJunto.dat";
	public static final String parfilename = "numbinPar.dat";
	public static final String imparfilename = "numbinImpar.dat";
	public static final int fin = 0;
	
	public static int readNum(Scanner s) {
		int out = 0;
		do {
			System.out.print("Número en [1, 100] (0 acaba): ");
			out = s.nextInt();
		} while ((out < 1 || out > 100) && out != 0);
		return out;
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int in = 0, num = 0;

		try {
			ObjectOutputStream oos = 
					new ObjectOutputStream(new FileOutputStream(dir + juntofilename));
			do {
				in = readNum(s);
				if (in != 0) {
					oos.writeInt(in);
					System.out.println("> guardando: " + in);
				}
			} while (in != 0);
			oos.close();
		} catch (IOException ioe) {}
		
		System.out.println();
		
		try {
			ObjectInputStream ois = 
					new ObjectInputStream(new FileInputStream(dir + juntofilename));
			ObjectOutputStream oosp = 
					new ObjectOutputStream(new FileOutputStream(dir + parfilename));
			ObjectOutputStream oosi = 
					new ObjectOutputStream(new FileOutputStream(dir + imparfilename));
			while (true) {
				try {
					num = ois.readInt();
					if (num % 2 == 0) {
						oosp.writeInt(num);
						System.out.println("[par] " + num);
					} else {
						oosi.writeInt(num);
						System.out.println("[imp] " + num);
					}
			    } catch (EOFException eof) {break;}
			}
			ois.close();
			oosp.close();
			oosi.close();
		} catch (IOException ioe) {};
		
		System.out.println("\n# PARES #");
		try {
			ObjectInputStream ois = 
					new ObjectInputStream(new FileInputStream(dir + parfilename));
			while (true) {
				try {
					num = ois.readInt();
					System.out.println(num);
			    } catch (EOFException eof) {break;}
			}
			ois.close();
		} catch (IOException ioe) {};
		
		System.out.println("\n# IMPARES #");
		try {
			ObjectInputStream ois = 
					new ObjectInputStream(new FileInputStream(dir + imparfilename));
			while (true) {
				try {
					num = ois.readInt();
					System.out.println(num);
			    } catch (EOFException eof) {break;}
			}
			ois.close();
		} catch (IOException ioe) {};
		
	}

}
