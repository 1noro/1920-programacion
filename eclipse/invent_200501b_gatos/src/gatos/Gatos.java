package gatos;

import java.util.Scanner;
import java.lang.NumberFormatException;
import java.lang.IndexOutOfBoundsException;

/*
 * clase Gato
 * nombre
 * raza
 * color
 * din_amo
 * 
 * menu
 * agregar gatos
 * ver gatos
 * ver gatos por amo
 * 
 * array de 20
 * 
 * */

public class Gatos {
    
    public static final int nMaxOpt = 5;
    public static final String finAltas = "fin";
    
    // --- UTILIDADES ----------------------------------------------------------

    public static int getLastI(Gato[] cats) {
        int i = 0;
        for (i = 0; i < cats.length; i++) {
            if (cats[i] == null) {
                // System.out.println("# Lleno hasta: " + i);
                break;
            }
        }
        return i;
    }
    
    public static Gato[] agregarStack(Gato[] cats) {
        int i = getLastI(cats);
        try {cats[i + 0] = new Gato("Narciso",   "Ruso Azul",    "Gris    ", 1, "44438497J");} catch (IndexOutOfBoundsException ioobe) {};
        try {cats[i + 1] = new Gato("Monique",   "Comun Eur",    "Blanco  ", 2, "33322497X");} catch (IndexOutOfBoundsException ioobe) {};
        try {cats[i + 2] = new Gato("Patri  ",   "Egipcio  ",    "Amarillo", 4, "77722497L");} catch (IndexOutOfBoundsException ioobe) {};
        try {cats[i + 3] = new Gato("Tricia",    "Naranjo  ",    "Naranja ", 3, "11122497Z");} catch (IndexOutOfBoundsException ioobe) {};
        try {cats[i + 4] = new Gato("Rufino",    "Común Eur",    "Marron  ", 5, "22222497G");} catch (IndexOutOfBoundsException ioobe) {};
        try {cats[i + 5] = new Gato("Susi  ",    "Siamés   ",    "Blanco  ", 1, "33322497X");} catch (IndexOutOfBoundsException ioobe) {};
        System.out.println("# Gatos por defecto agregados");
        return cats;
    }
    
    // --- INTERFAZ DE USUARIO -------------------------------------------------
    public static void printMenu() {
        System.out.println();
        System.out.println("--- MENU ---");
        System.out.println(" 1 - Agreagar gato");
        System.out.println(" 2 - Agregar gatos por defecto");
        System.out.println(" 3 - Ver gatos");
        System.out.println(" 4 - Ver gatos por amo");
        System.out.println(" 5 - Ver gatos con nulos");
        System.out.println();
        System.out.println(" 0 - Salir");
        System.out.println();
    }
    
    public static int readOpt(Scanner s, int max) {
        int out = 0;
        do {
            System.out.print("Escribe una opción: ");
            out = s.nextInt();
            s.nextLine();
        } while (out < 0 || out > max);
        return out;
    }
    
    // --- ALTAS ---------------------------------------------------------------
    public static String readNombre(Scanner s) {
        String in = "";
        do {
            System.out.print("Nombre (15 char max); '" + finAltas + "' para acabar: ");
            in = s.nextLine();
        } while (!in.equals(finAltas) && in.length() > 15);
        return in;
    }
    
    public static String readRaza(Scanner s) {
        String in = "";
        do {
            System.out.print("Raza (15 char max): ");
            in = s.nextLine();
        } while (in.length() > 15);
        return in;
    }
    
    public static String readColor(Scanner s) {
        String in = "";
        do {
            System.out.print("Color (15 char max): ");
            in = s.nextLine();
        } while (in.length() > 15);
        return in;
    }
    
    public static int readEdad(Scanner s) {
        String in_str = "";
        int in = 0;
        boolean isInt = false;
        do {
            System.out.print("Edad (15 char max): ");
            in_str = s.nextLine();
            try {
                in = Integer.parseInt(in_str);
                isInt = true;
            } catch (NumberFormatException nfe) {
                System.out.println("# ERROR: No es un número.");
            }
        } while (!isInt);
        return in;
    }
    
    public static String readDniAmo(Scanner s) {
        String in = "";
        do {
            System.out.print("DNI Amo (9 char max): ");
            in = s.nextLine();
        } while (in.length() > 9);
        return in;
    }

    public static Gato[] altas(Scanner s, Gato[] cats) {
        int i = getLastI(cats);
        String nombre = "";
        String raza = "";
        String color = "";
        int edad = 0;
        String dni_amo = "";
        while (!nombre.equals(finAltas) && i < cats.length) {
            System.out.println("\n>> Nueva alta:");
            nombre = readNombre(s);
            if (!nombre.equals(finAltas)) {
                raza = readRaza(s);
                color = readColor(s);
                edad = readEdad(s);
                dni_amo = readDniAmo(s);
                cats[i] = new Gato(nombre, raza, color, edad, dni_amo);
                i++;
            }
        }
        return cats;
    }
    
    // --- LISTADOS ------------------------------------------------------------
    public static void listar(Gato[] cats) {
        int i = 0;
        System.out.println("# --- Listado de gatos --- ");
        //while (i < cats.length) {
        while (i < cats.length && cats[i] != null) {
//            if (cats[i] != null) System.out.println(cats[i].getNombre());
//                else System.out.println("null");
            System.out.println(cats[i].toString());
            i++;
        }
    }
    
    public static void listarNull(Gato[] cats) {
        int i = 0;
        System.out.println("# --- Listado de gatos --- ");
        while (i < cats.length) {
            if (cats[i] != null) System.out.println(cats[i].getNombre());
                else System.out.println("null");
            i++;
        }
    }
    
    public static void listarPorAmo(Gato[] cats) {
        System.out.println("# --- Listado de gatos por nombre --- ");
        int i = 0, e = 0;
        int tope = getLastI(cats);
        Gato aux = null;
        Gato catsAmo[] = cats.clone();
        // catsAmo que sea del tamaño de hasta getLastI(cats)
        for (i = 0; i < (tope - 1); i++) {
            for (e = i; e < tope; e++) {
                if (catsAmo[i].getDniAmo().compareTo(catsAmo[e].getDniAmo()) > 0) {
                    aux = catsAmo[i];
                    catsAmo[i] = catsAmo[e];
                    catsAmo[e] = aux;
                }
            }
        }
        i = 0;
        while (i < catsAmo.length && catsAmo[i] != null) {
            System.out.println(catsAmo[i].toStringAmo());
            i++;
        }
    }

    // --- MAIN ----------------------------------------------------------------
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int opt = 0;
        Gato cats[] = new Gato[20];
        
        do {
            printMenu();
            opt = readOpt(s, nMaxOpt);
            if (opt != 0) {
                switch (opt) {
                    case 1:
                        cats = altas(s, cats);
                        break;
                    case 2:
                        cats = agregarStack(cats);
                        break;
                    case 3:
                        listar(cats);
                        break;
                    case 4:
                        listarPorAmo(cats);
                        break;
                    case 5:
                        listarNull(cats);
                        break;
                }
            }
        } while (opt != 0);
        
        System.out.println("Bye (;︵;)");
        s.close();
    }

}
