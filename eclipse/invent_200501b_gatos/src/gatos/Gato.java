package gatos;

/*
 * clase Gato
 * nombre
 * raza
 * color
 * edad
 * din_amo
 * */

public class Gato {
    
    private String nombre;
    private String raza;
    private String color;
    private int edad;
    private String dniAmo;
    
    Gato(String nombre, String raza, String color, int edad, String dni_amo) {
        this.nombre = nombre;
        this.raza = raza;
        this.color = color;
        this.edad = edad;
        this.dniAmo = dni_amo;
    }
    
    Gato() {
        this.nombre = "";
        this.raza = "";
        this.color = "";
        this.edad = 0;
        this.dniAmo = "";
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getRaza() {
        return raza;
    }
    
    public void setRaza(String raza) {
        this.raza = raza;
    }
    
    public String getColor() {
        return color;
    }
    
    public void setColor(String color) {
        this.color = color;
    }
    
    public int getEdad() {
        return edad;
    }
    
    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    public String getDniAmo() {
        return dniAmo;
    }
    
    public void setDniAmo(String dni_amo) {
        this.dniAmo = dni_amo;
    }
    
    public String toString() {
        return this.nombre + "\t" + this.raza + "\t" + this.color + "\t" + Integer.toString(this.edad) + "\t" + this.dniAmo;
    }
    
    public String toStringAmo() {
        return this.dniAmo + "\t" + this.nombre + "\t" + this.raza + "\t" + this.color + "\t" + Integer.toString(this.edad);
    }
    
}
