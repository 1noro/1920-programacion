package ventascoches;

import java.util.Scanner;

public class VentasCoches {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int im = 0, ic = 0;
		int mvc[][] = new int[12][3];
		
		/*int mvc[][] = {
				{  1,   2,   3},
				{  1,   2,   3},
				{  1,   2,   3},
				{  1,   2,   3},
				{  1,   2,   3},
				{  1,   2,   3},
				{  1,   2,   3},
				{  1,   2,   3},
				{  1,   2,   3},
				{  1,   2,   3},
				{  1,   2,   3},
				{  1,   2,   3}
		};*/
		
		// llenamos la matriz
		for (im = 0; im < 12; im++) {
			for (ic = 0; ic < 3; ic++) {
				System.out.print("Ventas de la marca " + ic + 1 + " en el mes " + (im + 1) + " : ");
				mvc[im][ic] = scan.nextInt();
			}
		}
		scan.close();
		System.out.print("\n\n");
		
		// visualizamos el resultado
		for (im = 0; im < 12; im++) {
			for (ic = 0; ic < 3; ic++) {
				System.out.print(mvc[im][ic] + "\t");
			}
			System.out.print("\n");
		}

	}

}
