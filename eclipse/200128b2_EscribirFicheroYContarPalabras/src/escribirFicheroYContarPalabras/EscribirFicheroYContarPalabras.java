package escribirFicheroYContarPalabras;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class EscribirFicheroYContarPalabras {
	
	public static void askAndWriteF(String fullDir) {
		int c = ' ';
		try {
			//TRUE para que no sobreescriba
			FileWriter fw = new FileWriter(fullDir, false);
			System.out.println("#Escribe texto: ");
			c = (char) System.in.read();
			while (c != '*') {
				fw.write(c); 
				c = (char) System.in.read();
			}
			fw.close();
		} catch(IOException ioe) {}
	}
	
	public static String readF(String fullDir) {
		String out = "";
		int c = ' ';
		try {
			FileReader fr = new FileReader(fullDir);
			c = fr.read();
			while (c != -1) { //-1 es el fin de fichero
				out += (char) c;
				c = fr.read();
			}
			fr.close();
		} catch(IOException ioe) {}
		return out;
	}

	public static void main(String[] args) {
		String dir = "/opt/archivosJava/";
		String filename = "palabras00.txt";
		int c = ' ';
		String texto = "";
		int palabras = 0, i = 0;
		
		askAndWriteF(dir + filename);
		System.out.println();
		texto = readF(dir + filename);
		System.out.println("#Texto leido:");
		System.out.println("-------------------------------------------------");
		System.out.println(texto);
		System.out.println("-------------------------------------------------");
		
		// NO FURRULA (REVISAR EN EL FUTURO)
		i = 0;
		//texto += ' ';
		while (i < texto.length()) {
			c = texto.charAt(i);
			System.out.println((char) c);
			if (c == ' ' || c == '\n' && i != 0) {
				palabras++;
				if (i < texto.length() - 1) {
					i++;
					c = texto.charAt(i);
					while (c == ' ' || c == '\n' && i < texto.length()) {
						c = texto.charAt(i);
						i++;
					}
				}
			} else i++;
		}
		
		System.out.println();
		System.out.println("#Total palabras: " + palabras);
		
	}

}
