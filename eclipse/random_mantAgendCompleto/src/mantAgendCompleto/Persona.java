package mantAgendCompleto;
import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile; 
public class Persona {
	private int codigo;
	private String nombre;
	private int edad;
	final int LONGNOMBRE = 25;
	Persona(int codigo, String nombre, int edad){
		this.codigo = codigo;
		this.nombre = nombre;
		this.edad = edad;
	}
	
	int tamano(){
		return (4+2+25+4);
	}
	
	String blancos(int numblancos){
		char[] blancos = new char[numblancos];
		for(int i = 0 ;i<numblancos; i++)
			blancos[i] = ' ';
		String sblancos = new String(blancos);
		return sblancos;
	}
	
	String construirNombre(){
		String aux;
		int longrelleno;
		nombre.trim();
		longrelleno = LONGNOMBRE - nombre.length();
		aux = nombre + blancos(longrelleno);
		return aux;
	}
	
	void grabarEnArchivo(RandomAccessFile f){
		String aux;
		try{
			f.writeInt(codigo);
			aux = construirNombre();
			f.writeUTF(aux);
			f.writeInt(edad);
		}catch(IOException ioe){
			System.out.println(ioe.getMessage());
		}
	}
	
	boolean leerDeArchivo(RandomAccessFile f){
		boolean finArchivo = false;
		try{
			codigo = f.readInt();
			nombre = f.readUTF();
			edad = f.readInt();
		}catch(EOFException eofe){
			finArchivo=true;
		}catch(IOException ioe){
			System.out.println("Error: "+ioe.getMessage());
		}
		return (finArchivo);
	}
	
	void mostrarDatos(int t){
		switch(t){
		case 0:
			System.out.println("Nombre.....................: "+nombre);
			System.out.println("Edad.......................: "+edad);
			break;
		case 1:
			System.out.println("1.- Nombre.................: "+nombre);
			System.out.println("2.- Edad...................: "+edad);
			break;
		case 2:
			System.out.println(codigo+"\t"+nombre+"\t"+edad);
			break;
		}
	}

	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
}
