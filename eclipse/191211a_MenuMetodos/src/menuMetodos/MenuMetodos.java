package menuMetodos;
import java.util.Scanner;
public class MenuMetodos {
	
	static void mostrarMenu() {
		System.out.println("\n MENU");
		System.out.println("=====================");
		System.out.println("1 - Teclear frase");
		System.out.println("2 - Codificar frase");
		System.out.println("3 - Decodificar frase");
		System.out.println("4 - Fin");
	}
	
	static int leerOpcion(Scanner s) {
		int opcion = 0; 
		do {
			System.out.print("Teclea una opción: ");
			opcion = s.nextInt();
		} while (opcion < 1 || opcion > 4);
		return opcion;
	}
	
	static String leerFrase(Scanner s) {
		String frase = ""; 
		System.out.print("Teclea una frase: ");
		s.nextLine();
		frase = s.nextLine();
		return frase;
	}
	
	static String codificar(String frase) {
		String fraseOut = "";
		int i = 0;
		for (i = 0; i < frase.length(); i++) {
			if (frase.charAt(i) == ' ') fraseOut += '*';
				else if (frase.charAt(i) == 'Z') fraseOut += 'A';
				else if (frase.charAt(i) == 'z') fraseOut += 'a';
				else fraseOut += (char) (frase.charAt(i) + 1);
		}
		return "\nFrase codificada: '" + fraseOut + "'";
	}
	
	static String decodificar(String frase) {
		return "\nFrase decodificada: '" + frase + "'";
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String fraseIn = "";
		int opcion = 0;
		
		do {
			mostrarMenu();
			opcion = leerOpcion(s);
			switch (opcion) {
				case 1:
					fraseIn = leerFrase(s);
					break;
				case 2:
					System.out.println(codificar(fraseIn));
					break;
				case 3:
					System.out.println(decodificar(fraseIn));
					break;
			}
		} while (opcion != 4);

		s.close();
	}

}
