package examen11ex3;

import java.util.Scanner;

public class Examen11Ex3 {

	public static void main(String[] args) {
		// qué delegación vendió mas? (NO FUNCIONA CORRECTAMENTE)
		
		Scanner s = new Scanner(System.in);
//		int meses = 12, deleg = 4, prod = 6;
		int meses = 2, deleg = 2, prod = 3;
		int im = 0, id = 0, ip = 0;
		
		int v[] = new int[deleg];
		int ide = 0; //indice para v[]
		int vM = 0; //valor maximo
		int vD = 0; //venta delegacion (acumulador)
		
		// matriz predefinifda
		int m[][][] = {
				{{23, 234, 45}, {123, 324, 46}},
				{{76, 24, 453}, {2133, 34, 91}}
		};
		
		// visualizar matriz
		for (im = 0; im < meses; im++) {
			System.out.print("\n>mes: " + im + "\n");
			for (id = 0; id < deleg; id++) {
				System.out.print("\n\t#deleg: " + id + "\n\t  ");
				for (ip = 0; ip < (prod - 1); ip++) {
					System.out.print(m[im][id][ip] + " (" + ip + "), ");
				}
				System.out.print(m[im][id][prod - 1] + " (" + ip + ")");
			}
		}
		
		for (id = 0; id < deleg; id++) {
			vD = 0;
			//calculamos las ventas de la delegación vD
			for (im = 0; im < meses; im++) {
				for (ip = 0; ip < prod; ip ++) {
					vD += m[im][id][ip];
				}
			}
			
			if (vD >= vM) {
				if (vD > vM) {
					vM = vD;
					ide = 0;
				}
				
				//guardamos el indice de la delegacion en el vector, porque es 
				// el máximo de momento
				//v[ide++] = id; //OPCIÓN ALTERNATIVA ONELINE
				v[ide] = id; 
			    ide++;
			}		
		}
		
		System.out.println();
		
		System.out.println("Mayor: " + vM);
		System.out.println("Delegaciones con valor máximo: ");
		for (id = 0; id < (ide - 1); id++)
			System.out.print(v[id] + ", ");
		System.out.print(v[ide - 1]);
		
		s.close();

	}

}
