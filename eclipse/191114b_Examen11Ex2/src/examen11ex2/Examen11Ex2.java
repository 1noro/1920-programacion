package examen11ex2;

import java.util.Scanner;

public class Examen11Ex2 {
	public static void main(String[] args) {
		Scanner e = new Scanner(System.in);
		int tam = 4;
		int v[] = new int[tam];
		int i = 0, j = 0;
	    int b = 0;
		boolean encontrado = false;
		
		i = 0;
		while (i < tam) {
			System.out.print("Teclea " + i + ": ");
			v[i] = e.nextInt();
			
			j = 0;
			encontrado = false;
			while (j < i && !encontrado) {
				if (v[i] == v[j]) encontrado = true;
				j++;
			}
			
			if (!encontrado) i++; 
		}
		
		System.out.print("v = [");
		for (i = 0; i < (tam - 1); i++) System.out.print(v[i] + ", ");
		System.out.println(v[tam - 1] + "]");
		
		System.out.print("Numero a buscar: ");
		b = e.nextInt();
		
		i = 0;
		encontrado = false;
		while (i < tam && !encontrado) {
			if (b == v[i]) encontrado = true;
			i++;
		}
		
		if (encontrado) System.out.println(":)");
			else System.out.println(":(");
		
		e.close();
	}
}
