package leerLineaALinea;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.util.Scanner;
public class LeerLineaALinea {

	public static void main(String[] args) {
		String dir = "/opt/archivosJava/";
		String filename = "lineas00.txt";
		Scanner s = new Scanner(System.in);
		String linea = "", texto = "";
		
		// ESCRIBIR ARCHIVO 
		try {
			//TRUE para que no sobreescriba
			FileWriter fw = new FileWriter(dir + filename, false);
			System.out.println("#Escribe texto: ");
			linea = s.nextLine();
			while (!linea.equals("*")) {
				linea += '\n';
				fw.write(linea); 
				linea = s.nextLine();
			}
			fw.close();
		} catch(IOException ioe) {}
		s.close();
		
		// LEER ARCHIVO
		try {
			FileReader fr = new FileReader(dir + filename);
			BufferedReader br = new BufferedReader(fr);
			linea = br.readLine();
			while (linea != null) {
				texto += linea + '\n';
				linea = br.readLine();
			}
			br.close();
			fr.close();
		} catch(IOException ioe) {}
		
		System.out.println();
		System.out.println("#Texto leido:");
		System.out.println(texto);
		
	}

}
