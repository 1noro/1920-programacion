package busquedadicotomica_20191007a;

import java.util.Scanner;

public class BusquedaDicotomica_20191007a {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int tam = 12, i = 0, e = 0, aux = 0;
		int busqueda = 0, iz = 0, im = 0, id = tam - 1;
		boolean encontrado = false;
		
		int v[] = { 3,  4, 66,  1,  2,  9, 88, 9, 36, 10,  1, 57};
		
		// Visualizamos el array original
		System.out.print("v[" + tam + "] = [");
		for (i = 0; i < (tam - 1); i++)
			System.out.print(v[i] + ", ");
		System.out.print(v[i] + "] (original)\n");
		
		// Ordenamos el array con el metodo de la Burbuja
		for (i = 0; i < (tam - 1); i++) 
			for (e = (i + 1); e < tam; e++)  {
				if (v[i] > v[e]) { // (< mayor a menor), (> menor a mayor)
					aux = v[i];
					v[i] = v[e];
				    v[e] = aux;
				}
			}
		
		// Visualizamos el array ordenado
		System.out.print("v[" + tam + "] = [");
		for (i = 0; i < (tam - 1); i++)
			System.out.print(v[i] + ", ");
		System.out.print(v[i] + "] (ordenado)\n");
		
		// Preguntamos por el número a buscar
		System.out.print("Número a buscar: ");
		busqueda = scan.nextInt();
		scan.close();
		
		// Realizamos la búsqueda dicotómica
		do {
			im = (int) (iz + id)/2;
			if (busqueda == v[im]) 
				encontrado = true;
			else {
				if (busqueda < v[im]) 
					id = im - 1;
				else 
					iz = im + 1;
			}
		} while ((iz <= id) && !encontrado);
		
		// Mostramos los resultados
		if (!encontrado) 
			System.out.print("No he encontrado '" + busqueda + "' :(\n");
		else 
			System.out.print("He encontrado '" + busqueda + "' en la posición '" + im + "' :D\n");
	}
}
