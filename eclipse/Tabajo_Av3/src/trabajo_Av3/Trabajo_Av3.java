package trabajo_Av3;

import java.io.IOException;
import java.io.RandomAccessFile;






public class Trabajo_Av3 {
	/*
	 *
Mant_Libros

Clase Libro que tenga numero int (clave), isbn string(13),  titulo string (25), autor string (25), edicion int, precio float

Altas (confirmar el alta, y si quieres otra), bajas, modificaciones, consultas, listado 
	 *
	 */
    // static final String ruta = "/Users/pinon/eclipse-workspace/";
    static final String ruta = "/opt/jarchivos/";
	
	public static void main(String[] args)throws IOException{
		Trabajo_Av3 a = new Trabajo_Av3();
		char opcion = 0 ;
		// boolean fin=false;
		RandomAccessFile fs = new RandomAccessFile (ruta+"biblioteca.dat","rw");
		fs.close();
		while (opcion != 'F'){
			
				Teclado t = new Teclado();
				
				System.out.println("\n\tMENU\n\t====\n");
				System.out.println("A.- Altas.");
				System.out.println("B.- Bajas.");
				System.out.println("C.- Consultas. ");
				System.out.println("M.- Modificaciones.");		
				System.out.println("L.- Listados.");
				System.out.println("F.- Fin.");
				System.out.print("\n\n\tTeclee opción? ");
				opcion = t.leerChar();
				opcion= Character.toUpperCase(opcion);
			
				switch(opcion) {
				case 'A':
					a.altas();
					break;
				case 'B':
					a.bajas();
					break;
				case 'C':
					a.consultas();
					break;
				case 'M':
					a.modificaciones();
					break;
				case 'L': 
					a.listados();
					break;
				case 'F':
					a.fin();
					break;	
				}
			}			
		}
	
	void altas()throws IOException{
		Libro pv = new Libro(0,"1234567890123","titulo","autor",0,0);
		int numero, edicion;
		String isbn,titulo,autor;
		float precio;
		Teclado t = new Teclado();
		
		char confirmar = ' ';
		RandomAccessFile fich = new RandomAccessFile(ruta+"biblioteca.dat","rw");
		System.out.println("\n\tALTAS\n\t=====\n");
		do{
			System.out.print("Teclear numero (0 para fin)...: ");
			numero = t.leerInt();
		}while(numero == Integer.MIN_VALUE);
		while(numero!=0){
			fich.seek(numero * pv.tamano());
			pv.leerDeArchivo(fich);
			if((pv.getNumero()!= 0 && pv.getIsbn() != "1234567890123" )&& numero*pv.tamano()<fich.length())
				System.out.println("\n\tLa persona ya está dada de alta.....\n");
			else{
				do {
					System.out.print("Teclear isbn............: ");
					isbn = t.leerString();
				}while(isbn.length()>13 || isbn.length()<13 );
				do {
				System.out.print("Teclear titulo..................: ");
				titulo =t.leerString();
				}while(titulo.length()>25);
				do {
					System.out.print("Teclear autor..................: ");
					autor =t.leerString();
				}while(autor.length()>25);
				do{
					System.out.print("Teclear edicion.................: ");
					edicion = t.leerInt();
				}while(edicion == Integer.MIN_VALUE);
				do{
					System.out.print("Teclear precio..................: ");
					precio = t.leerFloat();
				}while(precio == Integer.MIN_VALUE);
				
				do{
					System.out.print("\n\tConfirmar el alta (s/n)? ");
					confirmar = Character.toLowerCase(t.leerChar());
				}while (confirmar != 's' && confirmar != 'n');
				if (confirmar=='s'){
				Libro p = new Libro(numero, isbn, titulo, autor, edicion,precio);
				if (numero * p.tamano()>fich.length())
					fich.seek(fich.length());
				while(numero*p.tamano()>fich.length())
					(new Libro(0,"1234567890123","titulo","autor",0,0)).grabarEnArchivo(fich);
				fich.seek(numero * p.tamano());
				p.grabarEnArchivo(fich);
			  }
			}
			do{
				System.out.print("Teclear numero (0 para fin)...: ");
				numero = t.leerInt();
			}while(numero == Integer.MIN_VALUE);
		}
		fich.close();
	}
	
	void bajas()throws IOException{
		Libro p = new Libro(0,"1234567890123","titulo","autor",0,0);
		Teclado t = new Teclado();
		int numBus=0;
		char confirmar;
		System.out.println("\n\tBAJAS\n\t=====\n");
		RandomAccessFile fich = new RandomAccessFile(ruta+"biblioteca.dat","rw");
		do{
			System.out.print("\nTeclee el numero del libro ");
			numBus = t.leerInt();
		}while(numBus == Integer.MIN_VALUE);
		fich.seek(numBus*p.tamano());
		p.leerDeArchivo(fich);
		if(p.getNumero() == 0 || p.getIsbn() == "1234567890123"){
			System.out.println("\n\tEl alumno con el código: "+numBus+" no está registrado.\n");
		}
		else{
			p.mostrarDatos(0);
			do{
				System.out.print("\n\tDesea borrar el registro (s/n)? ");
				confirmar = t.leerChar();
			}while(Character.toLowerCase(confirmar)!='s' && Character.toLowerCase(confirmar)!='n');
			if (Character.toLowerCase(confirmar)=='s'){
				p = new Libro(0,"1234567890123","titulo","autor",0,0);
				fich.seek(numBus * p.tamano());
				p.grabarEnArchivo(fich);
				System.out.println("\n\tRegistro borrado correctamente\n");
			}
		}
		fich.close();
	}
	
	void modificaciones() throws IOException{
		Libro p = new Libro(0,"1234567890123","titulo","autor",0,0);
		Teclado t = new Teclado();
		char otro;
		int numBus = 0,cm=0,edicionN=0;
		String isbnN= null,tituloN= null, autorN = null;
		float precioN=0;
		do{
			RandomAccessFile fich = new RandomAccessFile(ruta+"biblioteca.dat","rw");
			System.out.println("\n\tMODIFICACIONES\n\t==============\n");
			do{
				System.out.print("Teclee numero del libro.....: ");
				numBus=t.leerInt();
			}while(numBus == Integer.MIN_VALUE);
			fich.seek(numBus * p.tamano());
			p.leerDeArchivo(fich);
			if (p.getNumero() == 0 || p.getIsbn() == "1234567890123")
				System.out.println("\n\tEl alumno con el número: "+numBus+" no está registrado.\n");
			else{
				isbnN = p.getIsbn();
				tituloN= p.getTitulo();
				autorN = p.getAutor();
				p.mostrarDatos(1);
				do{
					do{
						System.out.print("\n\tTeclee campo a modificar (1-5)?");
						cm=t.leerInt();
					}while(cm==Integer.MIN_VALUE || cm<1 || cm>5);
					switch(cm){
					case 1:
						do {
							System.out.print("Teclear isbn............: ");
							isbnN = t.leerString();
						}while(isbnN.length()>13 || isbnN.length()<13 );
						break;
					case 2:
						
						do {
						System.out.print("Teclear titulo..................: ");
						tituloN =t.leerString();
						}while(tituloN.length()>25);
						break;
					case 3:
						
						do {
							System.out.print("Teclear autor..................: ");
							autorN =t.leerString();
						}while(autorN.length()>25);
						break;
					case 4:
					
						do{
							System.out.print("Teclear edicion.................: ");
							edicionN = t.leerInt();
						}while(edicionN == Integer.MIN_VALUE);
						break;
					case 5:
						
						do{
							System.out.print("Teclear precio..................: ");
							precioN = t.leerFloat();
						}while(precioN == Integer.MIN_VALUE);
						break;
						
					}
					do{
						System.out.print("\n\tOtro campo a modificar (s/n)? ");
						otro = t.leerChar();
					}while (Character.toLowerCase(otro) != 's' && Character.toLowerCase(otro) != 'n');
				}while (Character.toLowerCase(otro)=='s');
				do{
					System.out.print("\n\tConfirmar las modificaciones (s/n)? ");
					otro = t.leerChar();
				}while (Character.toLowerCase(otro) != 's' && Character.toLowerCase(otro) != 'n');
				if (Character.toLowerCase(otro)=='s'){
					p = new Libro(numBus,isbnN,tituloN,autorN,edicionN,precioN);
					fich.seek(numBus*p.tamano());
					p.grabarEnArchivo(fich);
				}
				fich.close();
				}
			do{
				System.out.print("\n\tModificar otro libro (s/n).....: ");
				otro = t.leerChar();
			}while(Character.toLowerCase(otro)!='s' && Character.toLowerCase(otro)!='n');
		}while(Character.toLowerCase(otro)=='s');
	}
	
	void consultas()throws IOException {
		Libro p = new Libro(0,"1234567890123","titulo","autor",0,0);
		Teclado t = new Teclado();
		int codBus;
		RandomAccessFile fich = new RandomAccessFile(ruta+"biblioteca.dat","r");
		do{
			System.out.print("\nTeclee el código a buscar? ");
			codBus = t.leerInt();
		}while(codBus == Integer.MIN_VALUE);
		fich.seek(codBus * p.tamano());
		p.leerDeArchivo(fich);
		if(p.getNumero() != 0 )
			p.mostrarDatos(0);
		else
			System.out.println("\nNo existe ninguna persona con el número: "+codBus+" en el fichero.\n");
		fich.close();
	}
	
	
	

    void listados()throws IOException{
    	boolean fin = false;
    	Libro p = new Libro(0,"1234567890123","titulo","autor",0,0);
    	System.out.println("\n\tLISTADO\n\t=======\n");
    	RandomAccessFile fich = new RandomAccessFile (ruta+"biblioteca.dat","rw");
    	
    	while(!fin) {
    		if(p.getNumero()!=0 && p.getIsbn() != "1234567890123")
    			p.mostrarDatos(2);
    		// System.out.println("a");
    		fin= p.leerDeArchivo(fich);
    	}
    	fich.close();
    }
    
    void fin(){
    	System.out.println("\n\n\n\tFINAL DEL PROGRAMA\n\t==================\n");
    }
}
