package trabajo_Av3;

import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile; 

public class Libro {
	/*
	 * número(key) int, isbn Srting(13), titulo autor String (25), edición int, Precio float 
	 */
	private int numero;
	private String isbn;
	private String titulo;
	private String autor;
	private int edicion;
	private float precio;
	final int LONGISBN = 13;
	final int LONGTIT = 25;
	
	Libro(int numero, String isbn,String titulo,String autor, int edicion, float precio){
		this.numero = numero;
		this.isbn = isbn;
		this.titulo = titulo;
		this.autor = autor;
		this.edicion=edicion;
		this.precio = precio;
	}

	int tamano(){
		return (4+2+13+2+25+2+25+4+4);
	}
	
	String blancos(int numblancos){
		char[] blancos = new char[numblancos];
		for(int i = 0 ;i<numblancos; i++)
			blancos[i] = ' ';
		String sblancos = new String(blancos);
		return sblancos;
	}
	
	String construirtitulo(){
		String aux;
		int longrelleno;
		titulo.trim();
		longrelleno = LONGTIT - titulo.length();
		aux = titulo + blancos(longrelleno);
		return aux;
	}
	String construirautor(){
		String aux;
		int longrelleno;
		autor.trim();
		longrelleno = LONGTIT - autor.length();
		aux = autor + blancos(longrelleno);
		return aux;
	}
	void grabarEnArchivo(RandomAccessFile f){
		String aux;
		try{
			f.writeInt(numero);
			f.writeUTF(isbn);
			aux = construirtitulo();
			f.writeUTF(aux);
			aux = construirautor();
			f.writeUTF(aux);
			f.writeInt(edicion);
			f.writeFloat(precio);
		}catch(IOException ioe){
			System.out.println(ioe.getMessage());
		}
	}
	
	boolean leerDeArchivo(RandomAccessFile f){
		boolean finArchivo = false;
		try{
			numero = f.readInt();
			isbn = f.readUTF();
			titulo = f.readUTF();
			autor = f.readUTF();
			edicion = f.readInt();
			precio = f.readFloat();
		}catch(EOFException eofe){
			finArchivo=true;
		}catch(IOException ioe){
			System.out.println("Error: "+ioe.getMessage());
		}
		return (finArchivo);
	}
	
	void mostrarDatos(int t){
		switch(t){
		case 0:
			System.out.println("Numero.....................: "+numero);
			System.out.println("Isbn.......................: "+isbn);
			System.out.println("Titulo.....................: "+titulo);
			System.out.println("Autor......................: "+autor);
			System.out.println("Edicion....................: "+edicion);
			System.out.println("Precio.....................: "+precio);
			break;
		case 1:
			System.out.println("Numero.........................: "+numero);
			System.out.println("1.- Isbn.......................: "+isbn);
			System.out.println("2.- Titulo.....................: "+titulo);
			System.out.println("3.- Autor......................: "+autor);
			System.out.println("4.- Edicion....................: "+edicion);
			System.out.println("5.- Precio.....................: "+precio);
			break;
		case 2:
			System.out.println(numero+"\t"+isbn+"\t"+titulo+"\t"+autor+"\t"+edicion+"\t"+precio);
			break;
		}
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getEdicion() {
		return edicion;
	}

	public void setEdicion(int edicion) {
		this.edicion = edicion;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}
}
