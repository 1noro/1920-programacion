package arrayProfesores;

import java.util.Scanner;

public class arrayProfesores {
    
    public static final int nMaxOpt = 5;
    public static final int nMaxOptOrdenar = 5;
    
    // --- UTILIDADES ----------------------------------------------------------
    public static String tabular(String in, int longitud) {
        String out = "";
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in.length()) out += in.charAt(i);
                else out += ' ';
        }
        return out;
    }
    
    public static String tabular(int in, int longitud) {
        String out = "";
        String in_str = Integer.toString(in);
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in_str.length()) out += in_str.charAt(i);
            else out += ' ';
        }
        return out;
    }
    
    public static String tabular(double in, int longitud) {
        String out = "";
        String in_str = Double.toString(in);
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in_str.length()) out += in_str.charAt(i);
            else out += ' ';
        }
        return out;
    }
    
    public static String tabular(float in, int longitud) {
        String out = "";
        String in_str = Float.toString(in);
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in_str.length()) out += in_str.charAt(i);
                else out += ' ';
        }
        return out;
    }
    
    // --- INTERFAZ DE USUARIO -------------------------------------------------
    public static void printMenu(int max) {
        System.out.println("");
        System.out.println("--- MENU ---");
        System.out.println("");
        System.out.println(" 1 - Altas (auto)");
        System.out.println(" 2 - Listar");
        System.out.println(" 3 - Ordenar");
        System.out.println(" 4 - Matar por nombre");
        System.out.println("");
        System.out.println(" " + max + " - Fin");
        System.out.println("");
    }
    
    public static void printMenuOrd(int max) {
        System.out.println("");
        System.out.println("--- ORDENAR POR ---");
        System.out.println("");
        System.out.println(" 1 - Nombre");
        System.out.println(" 2 - Edad");
        System.out.println(" 3 - Inutilidad");
        System.out.println(" 4 - Vivo");
        System.out.println("");
        System.out.println(" " + max + " - Volver al menú");
        System.out.println("");
    }

    public static int readOpt(Scanner s, int max) {
        int out = 0;
        do {
            System.out.print("Teclee opción (1-" + max + "): ");
            out = s.nextInt();
            s.nextLine();
        } while (out < 1 || out > max);
        return out;
    }
    
    // --- ALTAS ---------------------------------------------------------------
    public static Profesor[] altasAuto(Profesor[] profesores) {
        System.out.println("\n--- ALTAS ---");
        profesores[0] = new Profesor("Emilio",  65,  8.5, 'V');
        profesores[1] = new Profesor("MJ",      59, 10.0, 'V');
        profesores[2] = new Profesor("Patxi",   54,  7.5, 'V');
        profesores[3] = new Profesor("Alfonso", 32,  1.7, 'V');
        System.out.println("\n# Altas generadas automáticamente.");
        return profesores;
    }
    
    // --- LISTADO -------------------------------------------------------------
    public static void listar(Profesor[] profesores) {
        System.out.println("\n--- LISTADO ---");
        int i = 0;
        for (i = 0; i < profesores.length; i++) {
            System.out.print(tabular(profesores[i].getNombre(), 10) + " ");
            System.out.print(tabular(profesores[i].getEdad(), 4) + " ");
            System.out.print(tabular(profesores[i].getInutilidad(), 7) + " ");
            System.out.print(profesores[i].getVivo() + "\n");
        }
    }
    
    // --- ORDENAR -------------------------------------------------------------
    public static Profesor[] ordenarPor(Profesor profesores[], int opt) {
        Profesor aux = null;
        int i = 0, e = 0;
        for (i = 0; i < profesores.length - 1; i++) {
            for (e = i + 1; e < profesores.length; e++) {
                if (
                    (profesores[i].getNombre().compareTo(profesores[e].getNombre()) > 0 && opt == 1) ||
                    (profesores[i].getEdad() > profesores[e].getEdad() && opt == 2) ||
                    (profesores[i].getInutilidad() > profesores[e].getInutilidad() && opt == 3) ||
                    (profesores[i].getVivo() > profesores[e].getVivo() && opt == 4)
                ) {
                    aux = profesores[i];
                    profesores[i] = profesores[e];
                    profesores[e] = aux;
                }
            }
        }
        return profesores;
    }
    
    public static Profesor[] ordenar(Scanner s, Profesor profesores[]) {
        int opt = 0;
        do {
            printMenuOrd(nMaxOptOrdenar);
            opt = readOpt(s, nMaxOptOrdenar);
            if (opt != nMaxOptOrdenar) profesores = ordenarPor(profesores, opt);
            System.out.println("\n# Array ordenado por la opción " + opt);
        } while(opt != nMaxOptOrdenar);
        return profesores;
    }
    
    // --- MATAR ---------------------------------------------------------------
    public static Profesor[] matar(Scanner s, Profesor profesores[]) {
        boolean muerte = false;
        String nombre = "";
        int i = 0;
        System.out.print("Escribe el nombre del profesor que quieres matar: ");
        nombre = s.nextLine();
        for (i = 0; i < profesores.length; i++) {
            if (profesores[i].getNombre().equalsIgnoreCase(nombre)) {
                if (profesores[i].getVivo() == 'V') {
                    profesores[i].setVivo('M');
                    System.out.println("\n# Has matado a " + profesores[i].getNombre() + ".");
                    muerte = true;
                } else {
                    System.out.println("\n# Has rematado a " + profesores[i].getNombre() + ".");
                    muerte = true;
                }
            }
        }
        if (!muerte) System.out.println("\n# No has matado a nadie pringao.");
        return profesores;
    }
    
    // --- MAIN ----------------------------------------------------------------
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int opt = 0;
        Profesor profesores[] = new Profesor[4];
        
        do {
            printMenu(nMaxOpt);
            opt = readOpt(s, nMaxOpt);
            if (opt != nMaxOpt) {
                switch (opt) {
                    case 1:
                        profesores = altasAuto(profesores);
                        break;
                    case 2:
                        listar(profesores);
                        break;
                    case 3:
                        profesores = ordenar(s, profesores);
                        break;
                    case 4:
                        profesores = matar(s, profesores);
                        break;
                }
            }
        } while (opt != nMaxOpt);
        
        System.out.println("FINAL DEL PROGRAMA");
        s.close();
    }

}
