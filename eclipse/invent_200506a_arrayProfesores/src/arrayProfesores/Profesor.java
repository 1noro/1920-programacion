package arrayProfesores;

public class Profesor {
    
    private String nombre;
    private int edad;
    private double inutilidad;
    private char vivo;
    
    Profesor() {
        nombre = "";
        edad = 0;
        inutilidad = 0.0;
        vivo = 'V';
    }
    
    Profesor(String nombre, int edad, double inutilidad, char vivo) {
        this.nombre = nombre;
        this.edad = edad;
        this.inutilidad = inutilidad;
        this.vivo = vivo;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public int getEdad() {
        return edad;
    }
    
    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    public double getInutilidad() {
        return inutilidad;
    }
    
    public void setInutilidad(double inutilidad) {
        this.inutilidad = inutilidad;
    }
    
    public char getVivo() {
        return vivo;
    }
    
    public void setVivo(char vivo) {
        this.vivo = vivo;
    }

}
