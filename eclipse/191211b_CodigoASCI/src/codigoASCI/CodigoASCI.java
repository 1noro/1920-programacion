package codigoASCI;
import java.util.Scanner;
public class CodigoASCI {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		char caracter = ' ';
		int num = 0;

		do {
			do {
				System.out.print("Escribe un número entre 0 y 255 (256 para salir): ");
				num = s.nextInt();
			} while (num < 0 || num > 256);
			if (num != 256) {
				caracter=(char) num;  
				System.out.println(num + " - '" + caracter + "'");
			}
		} while (num != 256);
		
		s.close();
	}

}
