package archivoBinario;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ArchivoBinario {
	
	public static final String dir = "/opt/archivosJava/";
	public static final String filename = "bin01.dat";
	//public static final boolean overwriteFile = true;

	public static void main(String[] args) throws IOException {
		int 
			t1[] = new int[10], 
			t2[] = new int[10],
			i = 0;
		
		// llenamos el vector con los multiplos de 5
		System.out.println("> Llenamos t1");
		for (i = 0; i < 10; i++) t1[i] = i * 5;
		
		// Escribimos
		System.out.println("> Escribimos de t1 al archivo");
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(dir + filename));
		for (i = 0; i < 10; i++) oos.writeInt(t1[i]);
		oos.close();
		
		// Leemos
		System.out.println("> Leemos del archivo a t2");
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(dir + filename));
		for (i = 0; i < 10; i++) t2[i] = ois.readInt();
		ois.close();
		
		// Visualizamos
		System.out.println("> Visualizamos t2");
		for (i = 0; i < 10; i++) System.out.println(t2[i]);
		
	}

}
