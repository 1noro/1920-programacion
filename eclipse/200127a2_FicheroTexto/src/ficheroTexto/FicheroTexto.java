package ficheroTexto;
import java.io.IOException;
import java.io.FileWriter;
public class FicheroTexto {

	public static void main(String[] args) {
		String dir = "/opt/archivosJava/";
		String filename = "a00.txt";
		char car = ' ';
		try {
			//TRUE para que no sobreescriba
			FileWriter fw = new FileWriter(dir + filename, true);
			System.out.println("Escribe texto: ");
			car = (char) System.in.read();
			while (car != '*') {
				fw.write(car); 
				car = (char) System.in.read();
			}
			fw.close();
		} catch(IOException ioe) {}
		System.out.println("Bye (;︵;)");
	}

}
