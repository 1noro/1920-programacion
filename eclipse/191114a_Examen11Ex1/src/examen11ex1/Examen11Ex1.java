package examen11ex1;

import java.util.Scanner;

public class Examen11Ex1 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int i = 0, n1 = 0, n2 = 0, razon = 0;
		
		do {
		
			do {
				System.out.print("Primer número: ");
				n1 = s.nextInt();
				System.out.print("Segundo número: ");
				n2 = s.nextInt();
			} while (n1 > n2);
			
			System.out.print("Razón: ");
			razon = s.nextInt();
			
			i = n1;
			while (i <= n2) {
				System.out.println(i);
				i = i * razon;
			}
			
			do {
				System.out.print("Repetir? (1/0): ");
				razon = s.nextInt();
			} while (razon != 0 && razon != 1);
			
		} while (razon == 1);
		
		s.close();

	}

}
