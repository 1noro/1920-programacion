package inputstring;

import java.util.Scanner;

public class InputString {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String cadena = null;
		cadena = "Esto es una cadena.";
		System.out.println("Cadena: " + cadena);
		
		// ---------------------------------------------------------------------
		String cadena2 = new String("Esto es una cadena.");
		System.out.println("Cadena: " + cadena2);
		
		System.out.println("Longitud: " + cadena.length());
		
		// ---------------------------------------------------------------------
		String cadena3 = null;
		System.out.print("Cadena3: ");

//		cadena3 = entrada.next(); // solo lee la primera palabra de la frase
		cadena3 = entrada.nextLine();
		entrada.close();
		
		System.out.println("Cadena3: " + cadena3);
		System.out.println("Longitud cadena3: " + cadena3.length());
		System.out.println("3ª posicion de caden3: " + cadena3.charAt(2));

	}

}
