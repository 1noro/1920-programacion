package busquedasecuencial_20191001b;

import java.util.Scanner;

public class BusquedaSecuencial_20191001b {

	public static void main(String[] args) {
		Scanner e = new Scanner(System.in);
		int i = 0, tam = 12, busqueda = 0;
		boolean existe = false;
		
//		int v[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
		int v[] = new int[tam];
		
		for (i = 0; i < tam; i++) {
			System.out.print("Teclea el valor " + (i + 1) + ": ");
			v[i] = e.nextInt();
		}
		
		System.out.print("Número a buscar: ");
		busqueda = e.nextInt();
		
		i = 0;
		do {
			if (busqueda == v[i]) 
				existe = true;
			i++;
		} while (!existe && i < tam);

		if (existe) 
			System.out.println("Existe :D");
		else
			System.out.println("No existe :(");
		
		e.close();
	}

}
