package cajero_20190926a;

import java.util.Scanner;

public class Cajero_20190926a {

	public static void main(String[] args) {
		int in = 0, tot = 0;
		Scanner e = new Scanner(System.in);
		
		do {
			System.out.print("Precio producto: ");
			in = e.nextInt();
			tot += in;
		} while (in != 0);
		
		System.out.print("Total a pagar: " + tot + "€\n");
		
		do {
			System.out.print("Precio producto: ");
			in = e.nextInt();
			tot -= in;
			if (tot > 0) {
				System.out.print("Falta por pagar: " + tot + "€\n");
			}
		} while (tot > 0);

		System.out.print("Cambio: " + (tot * (-1)) + "€\n");
		
		e.close();
	}

}
