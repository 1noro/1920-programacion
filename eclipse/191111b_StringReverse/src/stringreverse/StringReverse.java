package stringreverse;

import java.util.Scanner;

public class StringReverse {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String a = "", b = "";
		int i = 0;
		System.out.print("Frase: ");

		a = entrada.nextLine();
		entrada.close();
		
		// múltiples formas de hacerlo, a cada cual mas simple
		System.out.print("Frase rev: ");
		for (i = a.length() - 1; i >= 0; i--) System.out.print(a.charAt(i));
		System.out.println();
		
		b = "";
		for (i = a.length() - 1; i >= 0; i--) b = b.concat(Character.toString(a.charAt(i)));
		System.out.println("Frase rev: " + b);
		
		b = "";
		for (i = a.length() - 1; i >= 0; i--) b += Character.toString(a.charAt(i));
		System.out.println("Frase rev: " + b);
		
		b = "";
		for (i = a.length() - 1; i >= 0; i--) b += a.charAt(i);
		System.out.println("Frase rev: " + b);

	}

}
