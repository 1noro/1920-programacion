package descripcionarray_20191028c;

import java.util.Scanner;

public class DescripcionArray_20191028c {

	public static void main(String[] args) {
		Scanner e = new Scanner(System.in);
		int i = 0;
		
		//int v1[]; // definimos el array
		//v1 = new int[8]; // instanciamos el array
		
		// la definicion e instanciacion se pueden resumir en 1 linea
		int v2[] = new int[8];
		
		
		for (i = 0; i < 8; i++) {
			System.out.print("Teclea el valor " + i + ": ");
			v2[i] = e.nextInt();
		}
		
		System.out.print("v2 = [");
		for (i = 0; i < 8; i++) {
			if (i != 7)
				System.out.print(v2[i] + ", ");
			else
				System.out.print(v2[i]);
		}
		System.out.print("]\n");
		e.close();
	}

}
