package ahorcado;

import java.io.IOException;
import java.util.Scanner;

public class Ahorcado {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String palabra = "";
		int i = 0, e = 0, contF = 0;
		char letra = ' ', salida = ' ';
		boolean encontrado = false, todoF = false, todoA = false;
		int tam = 0;
		char aciertos[] = new char[tam];
		char fallos[] = new char[tam];
		char vpalabra[] = palabra.toCharArray();
		
		do {
			// Leemos la plabra
			System.out.print("Palabra: ");
			palabra = entrada.nextLine();
			palabra = palabra.toLowerCase();
			
			// Obtenemos el tamaño de la palabra
			tam = palabra.length();
			
			// Creamos los vectores de acirtos y fallos
			aciertos = new char[tam];
			fallos = new char[tam];
			
			// Creamos un vector con todos los caracteres de la palabra
			vpalabra = palabra.toCharArray();
			
			// Formateamos los vectores de aciertos y fallos
			aciertos[0] = vpalabra[0];
			fallos[0] = vpalabra[0];
			for (i = 1; i < tam - 1; i++) {
				aciertos[i] = '_';
				fallos[i] = '_';
			}
			aciertos[tam-1] = vpalabra[tam-1];
			fallos[tam-1] = vpalabra[tam-1];
	
			// BUCLE DONDE COMPROBAMOS LAS LETRAS
			contF = 1;
			while (!todoF && !todoA) {
				encontrado = false;
				
				// imprimimos los aciertos
				System.out.print("Aciertos: ");
				for (i = 0; i < tam; i++)
					System.out.print(aciertos[i] + " ");
				System.out.println();
				
				// imprimimos los fallos
				System.out.print("Fallos:   ");
				for (i = 0; i < tam; i++)
					System.out.print(fallos[i] + " ");
				System.out.println();
				
				// leemos la letra en minusculas
				System.out.print("Letra: ");
				try {
					letra = (char) System.in.read();
					while(System.in.read()!='\n'); //leer hasta pulsar enter
				} catch (IOException ioe) {}
				letra = Character.toLowerCase(letra);
				
				// comprobamos si lo acertamos
				// si lo encontramos lo metemos en aciertos
				for (e = 0; e < tam; e++) {
					if (letra == vpalabra[e]) {
						aciertos[e] = letra;
						encontrado = true;
					}
				}
				
				// si lo fallamos lo metemos en fallos
				if (!encontrado) {
					fallos[contF] = letra;
					contF++;
				}
				
				// comprobamos si todo el array de fallos está completo
				todoF = true;
				for (e = 0; e < tam; e++)
					if (fallos[e] == '_')
						todoF = false;
				
				// comprobamos si todo el array de aciertos está completo
				todoA = true;
				for (e = 0; e < tam; e++)
					if (aciertos[e] == '_')
						todoA = false;		
				
			}
			
			// comprobamos si ganaste
			if (todoA) System.out.println("Ganaste :)");
				else System.out.println("Perdiste :(");
			System.out.println();
			
			// preguntamos si quiere salir o repetir
			do {
				System.out.print("Salir? (S/N): ");
				try {
					salida = (char) System.in.read();
					while(System.in.read()!='\n'); //leer hasta pulsar enter
				} catch (IOException ioe) {}
				salida = Character.toLowerCase(salida);
				System.out.println();
			} while (salida != 's' && salida != 'n');
		
		} while (salida != 's');
		
		entrada.close();

	}

}
