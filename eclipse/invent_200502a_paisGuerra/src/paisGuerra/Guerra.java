package paisGuerra;

public class Guerra {
    
    private int id;
    private String nombre;
    private int idPais1;
    private int idPais2;
    
    Guerra () {
        this.id = 0;
        this.nombre = "";
        this.idPais1 = 0;
        this.idPais2 = 0;
    }
    
    Guerra (int id, String nombre, int idPais1, int idPais2) {
        this.id = id;
        this.nombre = nombre;
        this.idPais1 = idPais1;
        this.idPais2 = idPais2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdPais1() {
        return idPais1;
    }

    public void setIdPais1(int idPais1) {
        this.idPais1 = idPais1;
    }

    public int getIdPais2() {
        return idPais2;
    }

    public void setIdPais2(int idPais2) {
        this.idPais2 = idPais2;
    }
    
    public String toString() {
        return this.id + "\t" + this.nombre + "\t" + this.idPais1 + "\t" + this.idPais2;
    }
}
