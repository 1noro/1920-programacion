package paisGuerra;

import java.util.Scanner;

/*
 * TODO:
 * Euniciado básico - OK
 * Comprobar que no se repita el ide de pais y el de guerra - 
 * Comprobar que los ides de los contrincantes estén dados de alta - 
 * Hacer el tabulado en los listados - 
 * Guardar los arrays en un archivo - 
 * Llenar los arrays a partir de los archivos - 
 * */

public class paisGuerra {
    
    public static final int nMaxOpt = 5;
    public static final String finAltas = "fin";
    
    // --- UTILIDADES ----------------------------------------------------------

    public static int getLastI(Object[] objArr) {
        int i = 0;
        while (i < objArr.length && objArr[i] != null) i++;
        return i;
    }

    // --- INTERFAZ DE USUARIO -------------------------------------------------
    public static void printMenu() {
        System.out.println();
        System.out.println("--- MENU ---");
        System.out.println(" 1 - Alta paises");
        System.out.println(" 2 - Alta guerras");
        System.out.println(" 3 - Listar paises");
        System.out.println(" 4 - Listar guerras");
        System.out.println(" 5 - Buscar paises por guerra");
        System.out.println();
        System.out.println(" 0 - Salir");
        System.out.println();
    }
    
    public static int readOpt(Scanner s, int max) {
        int out = 0;
        do {
            System.out.print("Escribe una opción: ");
            out = s.nextInt();
            s.nextLine();
        } while (out < 0 || out > max);
        return out;
    }
    
    // --- ALTAS ---------------------------------------------------------------

    public static String readNombre(Scanner s, int limite) {
        String in = "";
        do {
            System.out.print("Nombre (" + limite + " char max); '" + finAltas + "' para acabar: ");
            in = s.nextLine();
        } while (!in.equals(finAltas) && in.length() > limite);
        return in;
    }
    
    public static int readId(Scanner s, String motivo) {
        String in_str = "";
        int in = 0;
        boolean isInt = false;
        do {
            System.out.print("Id " + motivo + " (numérico): ");
            in_str = s.nextLine();
            try {
                in = Integer.parseInt(in_str);
                isInt = true;
            } catch (NumberFormatException nfe) {
                System.out.println("# ERROR: No es un número.");
            }
        } while (!isInt);
        return in;
    }

    public static Pais[] altasPaises(Scanner s, Pais[] paises) {
        int i = getLastI(paises);
        String nombre = "";
        int id = 0;
        while (!nombre.equals(finAltas) && i < paises.length) {
            System.out.println("\n>> Nueva alta:");
            nombre = readNombre(s, 10);
            if (!nombre.equals(finAltas)) {
                id = readId(s, "de país");
                paises[i] = new Pais(id, nombre);
                i++;
            }
        }
        return paises;
    }
    
    public static Guerra[] altasGuerras(Scanner s, Guerra[] guerras) {
        int i = getLastI(guerras);
        String nombre = "";
        int id = 0;
        int idPais1 = 0;
        int idPais2 = 0;
        while (!nombre.equals(finAltas) && i < guerras.length) {
            System.out.println("\n>> Nueva alta:");
            nombre = readNombre(s, 20);
            if (!nombre.equals(finAltas)) {
                id = readId(s, "de guerra");
                idPais1 = readId(s, "del primer contrincante");
                idPais2 = readId(s, "del segundo contrincante");
                guerras[i] = new Guerra(id, nombre, idPais1, idPais2);
                i++;
            }
        }
        return guerras;
    }
    
    // --- LISTADOS ------------------------------------------------------------
    public static void listar(Object[] arr) {
        int i = 0;
        System.out.println("\n# --- Listado --- ");
        while (i < arr.length && arr[i] != null) {
            System.out.println(arr[i].toString());
            i++;
        }
    }
    
    public static void listarNull(Object[] arr) {
        int i = 0;
        System.out.println("\n# --- Listado --- ");
        while (i < arr.length) {
            if (arr[i] != null) System.out.println(arr[i].toString());
                else System.out.println("null");
            i++;
        }
    }
    
    // --- CONSULTAS -----------------------------------------------------------
    public static int[] getIdContrincantes(int idGuerra, Guerra[] guerras) {
        int[] out = {0, 0};
        int i = 0;
        while (i < guerras.length) {
            if (guerras[i].getId() == idGuerra) {
                out[0] = guerras[i].getIdPais1();
                out[1] = guerras[i].getIdPais2();
                break;
            }
            i++;
        }
        return out;
    }
    
    public static Pais getPaisFromId(int idPais, Pais[] paises) {
        Pais out = null;
        int i = 0;
        for (i = 0; i < paises.length; i++) {
            if (paises[i].getId() == idPais) {
                out = paises[i];
                break;
            }
        }
        return out;
    }
    
    public static void listarPaisesPorGuerra(Scanner s, Pais[] paises, Guerra[] guerras) {
        int idGuerra = readId(s, "de la guerra a seleccionar");
        int[] idContrincantes = getIdContrincantes(idGuerra, guerras);
        int idPais1 = idContrincantes[0];
        int idPais2 = idContrincantes[1];
        System.out.println("\n--- PAISES EN LA GUERRA " + idGuerra + " ---");
        System.out.println(getPaisFromId(idPais1, paises).toString());
        System.out.println(getPaisFromId(idPais2, paises).toString());
    }

    // --- MAIN ----------------------------------------------------------------
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int opt = 0;
        Pais paises[] = new Pais[4];
        Guerra guerras[] = new Guerra[2];
        
        do {
            printMenu();
            opt = readOpt(s, nMaxOpt);
            if (opt != 0) {
                switch (opt) {
                    case 1:
                        paises = altasPaises(s, paises);
                        break;
                    case 2:
                        guerras = altasGuerras(s, guerras);
                        break;
                    case 3:
                        listarNull(paises);
                        break;
                    case 4:
                        listarNull(guerras);
                        break;
                    case 5:
                        listarPaisesPorGuerra(s, paises, guerras);
                        break;
                }
            }
        } while (opt != 0);
        
        System.out.println("Bye (;︵;)");
        s.close();
    }

}
