package grabarLineaALinea;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
public class GrabarLineaALinea {

	public static void main(String[] args) {
		String dir = "/opt/archivosJava/";
		String filename = "lineas00.txt";
		Scanner s = new Scanner(System.in);
		String linea = "";
		try {
			//TRUE para que no sobreescriba
			FileWriter fw = new FileWriter(dir + filename, false);
			System.out.println("#Escribe texto: ");
			linea = s.nextLine();
			while (!linea.equals("*")) {
				linea += '\n';
				fw.write(linea); 
				linea = s.nextLine();
			}
			fw.close();
		} catch(IOException ioe) {}
		s.close();
	}

}
