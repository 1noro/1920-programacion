package rodriguezgarciaalfredo1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class RodriguezGarciaAlfredo1 {
    
    public static final int nMaxOpt = 4;
    public static final String finAltas = "xxx";
    public static final String dir = "/opt/jarchivos/";
    // public static final String dir = "C:\\Datos\\"; // para Emilio
    public static final String filenameP = "personas.txt";
    public static final String filenameV = "hombres.txt";
    public static final String filenameM = "mujeres.txt";
    
    // --- UTILIDADES ----------------------------------------------------------
    public static String tabular(String in, int longitud) {
        String out = "";
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in.length()) out += in.charAt(i);
                else out += ' ';
        }
        return out;
    }
    
    public static String tabular(int in, int longitud) {
        String out = "";
        String in_str = Integer.toString(in);
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in_str.length()) out += in_str.charAt(i);
            else out += ' ';
        }
        return out;
    }
    
    // --- INTERFAZ DE USUARIO -------------------------------------------------
    public static void printMenu() {
        System.out.println();
        System.out.println("--- MENU ---");
        System.out.println(" 1 - Altas");
        System.out.println(" 2 - Visualizar Fichero");
        System.out.println(" 3 - Crear ficheros por sexos");
        System.out.println();
        System.out.println(" " + nMaxOpt + " - Fin");
        System.out.println();
    }
    
    public static int readOpt(Scanner s, int max) {
        int out = 0;
        do {
            System.out.print("Teclee opción (1-" + max + "): ");
            out = s.nextInt();
            s.nextLine();
        } while (out < 1 || out > max);
        return out;
    }
    
    // --- ALTAS ---------------------------------------------------------------
    public static String readNombre(Scanner s, int limite) {
        String in = "";
        System.out.print("Nombre; '" + finAltas + "' para acabar: ");
        in = s.nextLine();
        return in;
    }
    
    public static int readEdad(Scanner s) {
        String in_str = "";
        int in = 0;
        boolean isInt = false;
        do {
            System.out.print("Edad (numérico): ");
            in_str = s.nextLine();
            try {
                in = Integer.parseInt(in_str);
                isInt = true;
            } catch (NumberFormatException nfe) {
                System.out.println("# ERROR: No es un número.");
            }
        } while (!isInt);
        return in;
    }
    
    public static char readSexo(Scanner s) {
        char in = ' ';
        do {
            System.out.print("Sexo (V/M): ");
            in = s.nextLine().toUpperCase().charAt(0);
        } while (in != 'V' && in != 'M');
        return in;
    }
    
    public static void altas(Scanner s, String fullDir) {
        String nombre = "";
        int edad = 0;
        char sexo = 0;
        System.out.println("\n--- ALTAS ---");
        try {
            //TRUE para que no sobreescriba
            BufferedWriter bw = new BufferedWriter(new FileWriter(fullDir, true));
            while (!nombre.equalsIgnoreCase(finAltas)) {
                System.out.println("\n>> Nueva alta:");
                nombre = readNombre(s, 10);
                if (!nombre.equalsIgnoreCase(finAltas)) {
                    edad = readEdad(s);
                    sexo = readSexo(s);
                    bw.write(nombre);
                    bw.newLine();
                    bw.write(Integer.toString(edad));
                    bw.newLine();
                    bw.write(sexo);
                    bw.newLine();
                }
            }
            bw.close();
        } catch(IOException ioe) {
            System.out.println("# ERROR: Guardado fallido");
        }
        System.out.println("\n# ARCHIVO GUARDADO");
    }
    
    // --- LISTADO -------------------------------------------------------------
    public static void listar(String fullDir) {
        String nombre = "";
        int edad = 0;
        char sexo = ' ';
        System.out.println("\n--- LISTADO ---");
        try {
            BufferedReader br = new BufferedReader(new FileReader(fullDir));
            nombre = br.readLine();
            while (nombre != null) {
                edad = Integer.parseInt(br.readLine());
                sexo = (char) br.readLine().charAt(0);
                System.out.print(tabular(nombre, 10) + " ");
                System.out.print(tabular(edad,    4) + " ");
                System.out.print(sexo + "\n");
                nombre = br.readLine();
            }
            br.close();
        } catch(IOException ioe) {
            System.out.println("# ERROR: Lectura fallida");
        }
    }
    
    // --- SEPARAR -------------------------------------------------------------
    public static void separar(String fullDirP, String fullDirV, String fullDirM) {
        String nombre = "";
        int edad = 0;
        char sexo = ' ';
        System.out.println("\n# Separando hombres y mujeres");
        try {
            BufferedReader br = new BufferedReader(new FileReader(fullDirP));
            BufferedWriter bwV = new BufferedWriter(new FileWriter(fullDirV, false));
            BufferedWriter bwM = new BufferedWriter(new FileWriter(fullDirM, false));
            nombre = br.readLine();
            while (nombre != null) {
                edad = Integer.parseInt(br.readLine());
                sexo = (char) br.readLine().charAt(0);
                if (sexo == 'V') {
                    bwV.write(nombre);
                    bwV.newLine();
                    bwV.write(Integer.toString(edad));
                    bwV.newLine();
                    bwV.write(sexo);
                    bwV.newLine();
                } else {
                    bwM.write(nombre);
                    bwM.newLine();
                    bwM.write(Integer.toString(edad));
                    bwM.newLine();
                    bwM.write(sexo);
                    bwM.newLine();
                }
                nombre = br.readLine();
            }
            bwM.close();
            bwV.close();
            br.close();
        } catch(IOException ioe) {
            System.out.println("# ERROR: Lectura/Escritura fallida");
        }
    }

    // --- MAIN ----------------------------------------------------------------
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int opt = 0;
        
        do {
            printMenu();
            opt = readOpt(s, nMaxOpt);
            if (opt != nMaxOpt) {
                switch (opt) {
                    case 1:
                        altas(s, dir + filenameP);
                        break;
                    case 2:
                        listar(dir + filenameP);
                        break;
                    case 3:
                        separar(dir + filenameP, dir + filenameV, dir + filenameM);
                        break;
                }
            }
        } while (opt != nMaxOpt);
        
        System.out.println("FINAL DEL PROGRAMA");
        s.close();
    }

}
