package separarVocales;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class SepararVocales {
    
    public static boolean esVocal(char c) {
        boolean out = false;
        c = Character.toLowerCase(c);
        if (c == 'a' ||
                c == 'e' ||
                c == 'i' ||
                c == 'o' ||
                c == 'u' ||
                c == 'á' ||
                c == 'é' ||
                c == 'í' ||
                c == 'ó' ||
                c == 'ú' ||
                c == 'ü' 
        )
        out = true;
        return out;
    }

    public static void guardarEnArchivo(String dir, String filename, String linea) {
        // ESCRIBIR ARCHIVO 
        try {
            //TRUE para que no sobreescriba
            FileWriter fw = new FileWriter(dir + filename, true);
            fw.write(linea + "\n");
            fw.close();
        } catch(IOException ioe) {}
    }
    
    public static void main(String[] args) {
        
        String dir = "/opt/jarchivos/";
        String filenameIn = "entrada.txt";
        String filenameOutV = "salidaV.txt";
        String filenameOutC = "salidaC.txt";
        
        String linea = "";
        
        // LEER ARCHIVO
        try {
            FileReader fr = new FileReader(dir + filenameIn);
            BufferedReader br = new BufferedReader(fr);
            linea = br.readLine();
            while (linea != null) {
                if (esVocal(linea.charAt(0))) 
                    guardarEnArchivo(dir, filenameOutV, linea);
                else
                    guardarEnArchivo(dir, filenameOutC, linea);
                linea = br.readLine();
            }
            br.close();
            fr.close();
        } catch(IOException ioe) {}
        
    }

}
