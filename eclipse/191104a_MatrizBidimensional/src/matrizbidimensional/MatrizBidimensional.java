package matrizbidimensional;

//import java.util.Scanner;

public class MatrizBidimensional {

	public static void main(String[] args) {
//		Scanner scan = new Scanner(System.in);
//		int m[][], ifi = 0, ico = 0;
//		m = new int[6][4];
		
//		int m[][] = new int[6][4], ifi = 0, ico = 0;

		int ifi = 0, ico = 0;
		int m[][] = {
				{23, 40, 80,  3},
				{98, 12, 32,  2},
				{99, 20, 10, 31},
				{87,  1, 32,  0},
				{67, 70, 80, 20},
				{34, 12, 99, 44}
		};
		
		// llenamos la matriz
		/*for (ifi = 0; ifi < 6; ifi++) {
			for (ico = 0; ico < 4; ico++) {
				System.out.print("m[" + ifi + "][" + ico + "] = ");
				m[ifi][ico] = scan.nextInt();
			}
		}*/
		
//		scan.close();
//		System.out.print("\n\n");
		
		// mostramos la matriz
		for (ifi = 0; ifi < 6; ifi++) {
			for (ico = 0; ico < 4; ico++) {
				System.out.print(m[ifi][ico] + "\t");
			}
			System.out.print("\n");
		}
	}

}
