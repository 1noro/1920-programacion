package mantLibros;

import java.io.BufferedWriter;
import java.io.IOException;

public class Libro {
    
    private static final int PREF = 2; // PREFIJO_STRING (UTF)
    private static final int LONG_INT = 4;
    private static final int LONG_FLOAT = 4;
    
    public static final int LONGITUD = LONG_INT + PREF + 13 + PREF + 25 + PREF + 25 + LONG_INT + LONG_FLOAT;
    
    private int numero;
    private String isbn; // 13
    private String titulo; // 25
    private String autor; // 25
    private int edicion;
    private float precio;
    
    Libro() {
        this.numero = 0;
        this.isbn = "ISBN VACIO".trim();
        this.titulo = "TITULO VACIO".trim();
        this.autor = "AUTOR VACIO".trim();
        this.edicion = 0;
        this.precio = 0;
    }
    
    Libro(int numero, String isbn, String titulo, String autor, int edicion, float precio) {
        this.numero = numero;
        this.isbn = isbn.trim();
        this.titulo = titulo.trim();
        this.autor = autor.trim();
        this.edicion = edicion;
        this.precio = precio;
    }
    
    // constructor de strings, por si hace falta
    Libro(String numero, String isbn, String titulo, String autor, String edicion, String precio) {
        this.numero = Integer.parseInt(numero);
        this.isbn = isbn.trim();
        this.titulo = titulo.trim();
        this.autor = autor.trim();
        this.edicion = Integer.parseInt(edicion);
        this.precio = Float.parseFloat(precio);
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn.trim();
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo.trim();
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor.trim();
    }

    public int getEdicion() {
        return edicion;
    }

    public void setEdicion(int edicion) {
        this.edicion = edicion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    public void guardarEnArchivo(BufferedWriter bw) {
        try {
            bw.write(Integer.toString(this.numero) + "\n");
            bw.write(this.isbn + "\n");
            bw.write(this.titulo + "\n");
            bw.write(this.autor + "\n");
            bw.write(Integer.toString(this.edicion) + "\n");
            bw.write(Float.toString(this.precio) + "\n");
        } catch(IOException ioe) {
            // ioe.printStackTrace();
            System.out.println("# ERROR: Fallo al guardar en el archivo.");
        }
    }

}
