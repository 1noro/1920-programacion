package ficheroTexArticulos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FicheroTexArticulos {

    public static final int nMaxOpt = 4;
    public static final String finAltas = "fin";
    public static final String dir = "/opt/jarchivos/";
    public static final String filename = "articulos.txt";
    
    // --- UTILIDADES ----------------------------------------------------------
    public static String tabular(String in, int longitud) {
        String out = "";
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in.length()) out += in.charAt(i);
                else out += ' ';
        }
        return out;
    }
    
    public static String tabular(int in, int longitud) {
        String out = "";
        String in_str = Integer.toString(in);
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in_str.length()) out += in_str.charAt(i);
            else out += ' ';
        }
        return out;
    }
    
    public static String tabular(double in, int longitud) {
        String out = "";
        String in_str = Double.toString(in);
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in_str.length()) out += in_str.charAt(i);
            else out += ' ';
        }
        return out;
    }
    
    public static String tabular(float in, int longitud) {
        String out = "";
        String in_str = Float.toString(in);
        int i = 0;
        for (i = 0; i < longitud; i++) {
            if (i < in_str.length()) out += in_str.charAt(i);
                else out += ' ';
        }
        return out;
    }
    
    // --- INTERFAZ DE USUARIO -------------------------------------------------
    public static void printMenu() {
        System.out.println();
        System.out.println("--- MENU ---");
        System.out.println(" 1 - Altas");
        System.out.println(" 2 - Listado");
        System.out.println(" 3 - Listado pedidos");
        System.out.println();
        System.out.println(" " + nMaxOpt + " - Salir");
        System.out.println();
    }
    
    public static int readOpt(Scanner s, int max) {
        int out = 0;
        do {
            System.out.print("Teclee opción (1-" + max + "): ");
            out = s.nextInt();
            s.nextLine();
        } while (out < 0 || out > max);
        return out;
    }

    // --- ALTAS ---------------------------------------------------------------
    public static String readDenom(Scanner s, int limite) {
        String in = "";
        do {
            System.out.print("Denominación (" + limite + " char max); '" + finAltas + "' para acabar: ");
            in = s.nextLine();
        } while (!in.equals(finAltas) && in.length() > limite);
        return in;
    }
    
    public static int readInt(Scanner s, String motivo) {
        String in_str = "";
        int in = 0;
        boolean isInt = false;
        do {
            System.out.print(motivo + " (numérico): ");
            in_str = s.nextLine();
            try {
                in = Integer.parseInt(in_str);
                isInt = true;
            } catch (NumberFormatException nfe) {
                System.out.println("# ERROR: No es un Integer.");
            }
        } while (!isInt);
        return in;
    }
    
    public static double readDouble(Scanner s, String motivo) {
        String in_str = "";
        double in = 0;
        boolean isDouble = false;
        do {
            System.out.print(motivo + " (numérico): ");
            in_str = s.nextLine();
            try {
                in = Double.parseDouble(in_str);
                isDouble = true;
            } catch (NumberFormatException nfe) {
                System.out.println("# ERROR: No es un Double.");
            }
        } while (!isDouble);
        return in;
    }
    
    public static void altas(Scanner s, String fullDir) {
        String denom = "";
        int sMin = 0;
        int sMax = 0;
        int sAct = 0;
        double precio = 0;
        System.out.println("\n--- ALTAS ---");
        try {
            //TRUE para que no sobreescriba
            BufferedWriter bw = new BufferedWriter(new FileWriter(fullDir, true));
            while (!denom.equalsIgnoreCase(finAltas)) {
                System.out.println("\n>> Nueva alta:");
                denom = readDenom(s, 15);
                if (!denom.equalsIgnoreCase(finAltas)) {
                    do {
                        sMin = readInt(s, "Stock mínimo");
                        sMax = readInt(s, "Stock máximo");
                    } while (sMin > sMax);
                    sAct = readInt(s, "Stock actual");
                    precio = readDouble(s, "Precio unitario");
                    bw.write(denom);
                    bw.newLine();
                    bw.write(Integer.toString(sMin));
                    bw.newLine();
                    bw.write(Integer.toString(sMax));
                    bw.newLine();
                    bw.write(Integer.toString(sAct));
                    bw.newLine();
                    bw.write(Double.toString(precio));
                    bw.newLine();
                }
            }
            bw.close();
        } catch(IOException ioe) {
            System.out.println("# ERROR: Guardado fallido");
        }
        System.out.println("\n# ARCHIVO GUARDADO");
    }
    
    // --- LISTADO -------------------------------------------------------------
    public static void listar(String fullDir) {
        int cont = 1;
        String denom = "";
        int sMin = 0;
        int sMax = 0;
        int sAct = 0;
        double precio = 0;
        double precioTotalArt = 0;
        double precioTotal = 0;
        System.out.println("\n--- LISTADO ---");
        System.out.print(tabular("N",      2) + " ");
        System.out.print(tabular("DENOM", 15) + " ");
        System.out.print(tabular("S_MIN",  5) + " ");
        System.out.print(tabular("S_MAX",  5) + " ");
        System.out.print(tabular("S_ACT",  5) + " ");
        System.out.print(tabular("PRECIO", 7) + " ");
        System.out.print(tabular("TOTAL",  7) + "\n");
        try {
            BufferedReader br = new BufferedReader(new FileReader(fullDir));
            denom = br.readLine();
            while (denom != null) {
                sMin = Integer.parseInt(br.readLine());
                sMax = Integer.parseInt(br.readLine());
                sAct = Integer.parseInt(br.readLine());
                precio = Double.parseDouble(br.readLine());
                precioTotalArt = (double) sAct * precio;
                System.out.print(tabular(cont,   2) + " ");
                System.out.print(tabular(denom, 15) + " ");
                System.out.print(tabular(sMin,   5) + " ");
                System.out.print(tabular(sMax,   5) + " ");
                System.out.print(tabular(sAct,   5) + " ");
                System.out.print(tabular(precio, 7) + " ");
                System.out.print(tabular(precioTotalArt, 7) + "\n");
                precioTotal += precioTotalArt;
                cont++;
                denom = br.readLine();
            }
            br.close();
        } catch(IOException ioe) {
            System.out.println("# ERROR: Lectura fallida");
        }
        System.out.println("\n# Precio Total Final: " + precioTotal);
        System.out.println("\n# ARCHIVO LEIDO");
    }
    
    // --- LISTADO PEDIDOS -----------------------------------------------------
    public static void listarPedidos(String fullDir) {
        int cont = 1;
        String denom = "";
        int sMin = 0;
        int sMax = 0;
        int sAct = 0;
        double precio = 0;
        int uPed = 0;
        double precioTotalPed = 0;
        double precioTotal = 0;
        System.out.println("\n--- LISTADO ---");
        System.out.print(tabular("N",      2) + " ");
        System.out.print(tabular("DENOM", 15) + " ");
        System.out.print(tabular("S_ACT",  5) + " ");
        System.out.print(tabular("U_PED",  5) + " ");
        System.out.print(tabular("TOTAL",  7) + "\n");
        try {
            BufferedReader br = new BufferedReader(new FileReader(fullDir));
            denom = br.readLine();
            while (denom != null) {
                sMin = Integer.parseInt(br.readLine());
                sMax = Integer.parseInt(br.readLine());
                sAct = Integer.parseInt(br.readLine());
                precio = Double.parseDouble(br.readLine());
                if (sAct < sMin) {
                    uPed = sMax - sAct;
                    precioTotalPed = (double) uPed * precio;
                    System.out.print(tabular(cont,   2) + " ");
                    System.out.print(tabular(denom, 15) + " ");
                    System.out.print(tabular(sAct,   5) + " ");
                    System.out.print(tabular(uPed,   5) + " ");
                    System.out.print(tabular(precioTotalPed, 7) + "\n");
                    precioTotal += precioTotalPed;
                    cont++;
                }
                denom = br.readLine();
            }
            br.close();
        } catch(IOException ioe) {
            System.out.println("# ERROR: Lectura fallida");
        }
        System.out.println("\n# Precio Total Final del Pedido: " + precioTotal);
        System.out.println("\n# ARCHIVO LEIDO");
    }
    
    // --- MAIN ----------------------------------------------------------------
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int opt = 0;
        
        do {
            printMenu();
            opt = readOpt(s, nMaxOpt);
            if (opt != nMaxOpt) {
                switch (opt) {
                    case 1:
                        altas(s, dir + filename);
                        break;
                    case 2:
                        listar(dir + filename);
                        break;
                    case 3:
                        listarPedidos(dir + filename);
                        break;
                }
            }
        } while (opt != nMaxOpt);
        
        System.out.println("FINAL DEL PROGRAMA");
        s.close();
    }

}
