package ventascoches;

public class VentasCoches {

	public static void main(String[] args) {
		int im = 0, ic = 0;
		int suma = 0;
		
		int mvc[][] = {
				{  8,   9,   6},
				{ 12,   8,  96},
				{ 25,  36,  41},
				{ 15,  17,  52},
				{ 12,  23,   5},
				{  3,   2,   5},
				{ 10,   9,  21},
				{ 41,  32,  14},
				{ 15,   6,  24},
				{  7,   5,   3},
				{ 47,  56,  12},
				{ 36,  47,  95}
		};
		
		
		// visualizamos la matriz
		for (im = 0; im < 12; im++) {
			for (ic = 0; ic < 3; ic++) {
				System.out.print(mvc[im][ic] + "\t");
			}
			System.out.print("\n");
		}
		
		//calculamos la suma
		for (im = 0; im < 12; im++) {
			for (ic = 0; ic < 3; ic++) {
				suma += mvc[im][ic];
			}
		}
		
		// visulaizamos la suma
		System.out.print("\nLa suma total es: " + suma + "\n");

	}

}
