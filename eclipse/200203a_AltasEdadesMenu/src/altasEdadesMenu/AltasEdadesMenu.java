package altasEdadesMenu;

import java.util.Scanner;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.NumberFormatException;

public class AltasEdadesMenu {
	
	public static final int nMaxOpt = 4;
	public static final String finAltas = "fin";
	public static final String dir = "/opt/archivosJava/";
	public static final String filename = "altasEdad01.dat";
	public static final boolean overwriteFile = true;

	// --- INTERFAZ DE USUARIO -------------------------------------------------

	public static void printMenu() {
		System.out.println();
		System.out.println("--- MENU ---");
		System.out.println(" 1 - Altas");
		System.out.println(" 2 - Listados");
		System.out.println(" 3 - Media de edad");
		System.out.println(" 4 - Nombre del mayor y menor (no rep.)");
		System.out.println();
		System.out.println(" 0 - Salir");
		System.out.println();
	}
	
	public static int readOpt(Scanner s, int max) {
		int out = 0;
		do {
			System.out.print("Escribe una opción: ");
			out = s.nextInt();
			s.nextLine();
		} while (out < 0 || out > max);
		return out;
	}
	
	// --- LECTURAS Y VALIDACIONES DE FORMATO ----------------------------------
	public static String readNombre(Scanner s) {
		String nombre = "";
		
		do {
			System.out.print(" Nombre (15 char max); '" + finAltas + "' para acabar: ");
			nombre = s.nextLine();
		} while(!nombre.equals(finAltas) && nombre.length() > 15);
		
		return nombre;
	}
	
	public static int readEdad(Scanner s) {
		String edad_str = "";
		int edad = 0;
		boolean continuar = true;
		
		do {
			System.out.print(" Edad: ");
			edad_str = s.nextLine();
			try {
				edad = Integer.parseInt(edad_str);
				continuar = false;
			} catch (NumberFormatException nfe) {}
		} while(continuar);
		
		return edad;
	}
	
	// --- FUNCIONES GENÉRICAS -------------------------------------------------
	public static void saveAlta(String linea, String fullDir) {
		System.out.println(" ### GUARDANDO: " + linea);
		try {
			//TRUE para que no sobreescriba
			FileWriter fw = new FileWriter(fullDir, overwriteFile);
			linea += '\n';
			fw.write(linea); 
			fw.close();
		} catch(IOException ioe) {}
	}
	
	public static void altas(Scanner s, String fullDir) {
		Alta myAlta;
		String nombre = "";
		int edad = 0;
		
		do {
			System.out.println("\n>> Nueva alta:");
			nombre = readNombre(s);
			
			if (!nombre.equals(finAltas)) {
				edad = readEdad(s);
				
				myAlta = new Alta(nombre, edad);
				saveAlta(myAlta.toStringForFile(), fullDir);
			}
			
		} while(!nombre.equals(finAltas));
	}
	
	public static void printListados(String fullDir) {
		Alta myAlta;
		String linea = "";
		System.out.println("\n--- LISTADOS ---");
		try {
			FileReader fr = new FileReader(fullDir);
			BufferedReader br = new BufferedReader(fr);
			linea = br.readLine();
			while (linea != null) {
				myAlta = new Alta(linea);
				System.out.println("" + myAlta.toString());
				linea = br.readLine();
			}
			br.close();
			fr.close();
		} catch(IOException ioe) {}
	}
	
	public static void printMediaEdad(String fullDir) {
		Alta myAlta;
		String linea = "";
		int cont = 0, suma = 0;
		
		try {
			FileReader fr = new FileReader(fullDir);
			BufferedReader br = new BufferedReader(fr);
			linea = br.readLine();
			while (linea != null) {
				myAlta = new Alta(linea);
				suma += myAlta.getEdad();
				cont++;
				
				linea = br.readLine();
			}
			br.close();
			fr.close();
		} catch(IOException ioe) {}
		
		System.out.println("\n--- MEDIA ---");
		System.out.println(" Medaia de edad: " + suma + " / " + cont + " = " + ((float) suma / (float) cont));
	}
	
	public static void printNombreMayorMenor(String fullDir) {
		Alta myAlta;
		String linea = "";
		String n = "", nMax = "", nMin = "123456789012345";
		
		try {
			FileReader fr = new FileReader(fullDir);
			BufferedReader br = new BufferedReader(fr);
			linea = br.readLine();
			while (linea != null) {
				myAlta = new Alta(linea);
				n = myAlta.getNombre();
				
				if (n.length() > nMax.length()) nMax = n;
				if (n.length() < nMin.length()) nMin = n;
				
				linea = br.readLine();
			}
			br.close();
			fr.close();
		} catch(IOException ioe) {}
		
		System.out.println("\n--- NOMBRES MAX Y MIN ---");
		System.out.println(" Nombre mayor: " + nMax);
		System.out.println(" Nombre menor: " + nMin);
	}

	// --- MAIN ----------------------------------------------------------------
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int opt = 0;

		do {
			printMenu();
			opt = readOpt(s, nMaxOpt);
			if (opt != 0) {
				switch (opt) {
					case 1:
						altas(s, dir + filename);
						break;
					case 2:
						printListados(dir + filename);
						break;
					case 3:
						printMediaEdad(dir + filename);
						break;
					case 4:
						printNombreMayorMenor(dir + filename);
						break;
				}
			}
		} while (opt != 0);
		
		System.out.println("Bye (;︵;)");
		
		s.close();
	}

}
