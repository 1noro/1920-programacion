package altasEdadesMenu;

public class Alta {
	
	private String nombre;
	private int edad;
	
	Alta (String _nombre, int _edad) {
		this.nombre = _nombre;
		this.edad = _edad;
	}
	
	Alta (String _linea_from_file) {
		String part[] = _linea_from_file.split("::");
		this.nombre = part[0];
		this.edad = Integer.parseInt(part[1]);
	}
	
	Alta () {
		this.nombre = "a";
		this.edad = 0;
	}
	
	public void setNombre(String _nombre) {this.nombre = _nombre;}
	public void setEdad(int _edad) {this.edad = _edad;}
	
	public String getNombre() {return this.nombre;}
	public int getEdad() {return this.edad;}
	
	// --- OTROS METODOS -------------------------------------------------------
	
	public String toString() {
		String tab = "\t";
		if (this.nombre.length() <= 6) tab = "\t\t";
		return "" + 
				this.nombre + tab +
				this.edad + "";
	}
	
	public String toStringForFile() {
		return "" + 
				this.nombre + "::" +
				this.edad + "";
	}
	
}
