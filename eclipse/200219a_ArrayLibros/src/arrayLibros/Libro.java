package arrayLibros;

import java.util.Scanner;

public class Libro {
	private String isbn;
	private int isbnLen = 13;
	private String titulo;
	private int tituloLen = 25;
	private String autor;
	private int autorLen = 25;
	private String genero;
	private int generoLen = 15;
	private float precio;
	
	Libro () {
		this.isbn = "";
		this.titulo = "";
		this.autor = "";
		this.genero = "";
		this.precio = 0;
	}
	
	Libro (String _isbn, String _tit, String _aut, String _gen, float _prec) {
		this.isbn = _isbn;
		this.titulo = _tit;
		this.autor = _aut;
		this.genero = _gen;
		this.precio = _prec;
	}
	
	public String getIsbn() {return this.isbn;}
	public String getTitulo() {return this.titulo;}
	public String getAutor() {return this.autor;}
	public String getGenero() {return this.genero;}
	public float getPrecio() {return this.precio;}
	
	public void setIsbn(String _isbn) {this.isbn = _isbn;}
	public void setTitulo(String _tit) {this.titulo = _tit;}
	public void setAutor(String _aut) {this.autor = _aut;}
	public void setGenero(String _gen) {this.genero = _gen;}
	public void setPrecio(float _prec) {this.precio = _prec;}
	
	public void readIsbn(Scanner s) {
		String in = "";
		do {
			System.out.print(" ISBN: ");
			in = s.nextLine();
		} while (in.length() > this.isbnLen);
		this.setIsbn(in);
	}
	
	public void readTitulo(Scanner s) {
		String in = "";
		do {
			System.out.print(" Titulo: ");
			in = s.nextLine();
		} while (in.length() > this.tituloLen);
		this.setTitulo(in);
	}
	
	public void readAutor(Scanner s) {
		String in = "";
		do {
			System.out.print(" Autor: ");
			in = s.nextLine();
		} while (in.length() > this.autorLen);
		this.setAutor(in);
	}
	
	public void readGenero(Scanner s) {
		String in = "";
		do {
			System.out.print(" Genero: ");
			in = s.nextLine();
		} while (in.length() > this.generoLen);
		this.setGenero(in);
	}
	
	public void readPrecio(Scanner s) {
		String in_str = "";
		float in = 0;
		boolean continuar = true;
		
		do {
			System.out.print(" Precio: ");
			in_str = s.nextLine();
			try {
				in = Float.parseFloat(in_str);
				continuar = false;
			} catch (NumberFormatException nfe) {}
		} while(continuar);
		
		this.setPrecio(in);
	}
	
	public String toNsp(String str, int n) {
		while (str.length() <= n) str += " ";
		return str;
	}
	
	public String toString() {
		return 	" " + 
				this.isbn + " " + 
				toNsp(this.titulo, 50) + " " + 
				toNsp(this.autor, 25) + " " +
				toNsp(this.genero, 12) + " " +
				this.precio;
	}
	
}
