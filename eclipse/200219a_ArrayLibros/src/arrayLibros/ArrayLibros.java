package arrayLibros;

import java.util.Scanner;

public class ArrayLibros {

	// --- CLI -----------------------------------------------------------------
	public static void printMenu() {
		System.out.println("");
		System.out.println("--- MENU ---");
		System.out.println("");
		System.out.println("1 - Altas auto");
		System.out.println("2 - Listar");
		System.out.println("3 - Ordenar");
		System.out.println("");
		System.out.println("0 - Fin");
		System.out.println("");
	}
	
	public static void printMenuOrd() {
		System.out.println("");
		System.out.println("--- ORDENAR POR ---");
		System.out.println("");
		System.out.println("1 - Titulo");
		System.out.println("2 - Autor");
		System.out.println("3 - Precio");
		System.out.println("");
		System.out.println("0 - Nada");
		System.out.println("");
	}

	public static int readOpt(Scanner s, int max) {
		int out = 0;
		do {
			System.out.print("> ");
			out = s.nextInt();
			s.nextLine();
		} while (out > max || out < 0);
		return out;
	}

	// --- UTILS ---------------------------------------------------------------
	public static Libro[] altasAuto() {
		Libro arrL[] = new Libro[20];
		arrL[0] = new Libro("i0", "t0", "a0", "g0", (float) 8.0);
		arrL[1] = new Libro("i1", "t1", "a1", "g1", (float) 8.0);
		arrL[2] = new Libro("i2", "t2", "a2", "g2", (float) 8.0);
		arrL[3] = new Libro("i3", "t3", "a3", "g3", (float) 8.0);
		arrL[4] = new Libro("i4", "t4", "a4", "g4", (float) 8.0);
		arrL[5] = new Libro("i5", "t5", "a5", "g5", (float) 8.0);
		arrL[6] = new Libro("i6", "t6", "a6", "g6", (float) 8.0);
		arrL[7] = new Libro("i7", "t7", "a7", "g7", (float) 8.0);
		arrL[8] = new Libro("i8", "t8", "a8", "g8", (float) 8.0);
		arrL[9] = new Libro("i9", "t9", "a9", "g9", (float) 8.0);
		arrL[10] = new Libro("i10", "t10", "a10", "g10", (float) 8.0);
		arrL[11] = new Libro("i11", "t11", "a11", "g11", (float) 8.0);
		arrL[12] = new Libro("i12", "t12", "a12", "g12", (float) 8.0);
		arrL[13] = new Libro("i13", "t13", "a13", "g13", (float) 8.0);
		arrL[14] = new Libro("i14", "t14", "a14", "g14", (float) 8.0);
		arrL[15] = new Libro("i15", "t15", "a15", "g15", (float) 8.0);
		arrL[16] = new Libro("i16", "t16", "a16", "g16", (float) 8.0);
		arrL[17] = new Libro("i17", "t17", "a17", "g17", (float) 8.0);
		arrL[18] = new Libro("i18", "t18", "a18", "g18", (float) 8.0);
		arrL[19] = new Libro("i19", "t19", "a19", "g19", (float) 8.0);
		return arrL;
	}

	public static Libro[] altasAuto2() {
		Libro arrL[] = new Libro[20];
		arrL[0] = new Libro("0-261-10320-2","Sin titulo","John Ronald Reuel Tolkien","Fantástica",(float)21.99);
		arrL[1] = new Libro("84-0133-720-8","El nombre del viento","Patrick Rothfuss","Novela",(float)22.9);
		arrL[2] = new Libro("84-01-42282-5","Fahrenheit 451","Ray Bradbury","Novela",(float)6);
		arrL[3] = new Libro("84-01-49136-3","Fundación","Isaac Asimov","Novela",(float)9.95);
		arrL[4] = new Libro("84-01-49654-3","Los límites de la Fundación","Isaac Asimov","Novela",(float)11.95);
		arrL[5] = new Libro("84-08-08925-4","El Símbolo Perdido","Dan Brown","Novela",(float)21.9);
		arrL[6] = new Libro("08-1137-542-X","Análisis estructurado moderno","Edward Fraguez","Ensayo",(float)28);
		arrL[7] = new Libro("84-16288-95-3","El honor de Dios","Lidia Falcó O'Neil","Narrativa",(float)26);
		arrL[8] = new Libro("84-204-0494-3","Cuando ya no importe","Juan Carlos Onetti","Novela",(float)16);
		arrL[9] = new Libro("84-206-3311-9","El Aleph","Jorge Luís Borges","Relato Corto",(float)9);
		arrL[10] = new Libro("84-207-2634-6","El sí de las niñas","Leando Fernández Mortatín","Teatro",(float)6);
		arrL[11] = new Libro("84-226-6412-7","Negreros","Alberto Vázquez Figueroa","Novela",(float)9.75);
		arrL[12] = new Libro("84-233-1157-0","El tiempo","Ana María Matute","Novela",(float)6);
		arrL[13] = new Libro("84-2334-161-0","La reina en el palacio de la corrientes de aire","Stieg Larsson","Novela",(float)22.5);
		arrL[14] = new Libro("84-239-9486-4","Ficciones","Jorge Luís Borges","Relato Corto",(float)6.5);
		arrL[15] = new Libro("84-246-8013-5","Bollos preñados","Karlos Arguiñano","Ensayo",(float)9.95);
		arrL[16] = new Libro("84-310-3074-0","Tokio Blues","Haruki Murakami","Novela",(float)21.5);
		arrL[17] = new Libro("84-339-2158-3","El antropólogo inocente","Nigel Barley","Ensayo",(float)18);
		arrL[18] = new Libro("84-350-3467-4","La muerte en Venecia","Thomas Mann","Novela",(float)12);
		arrL[19] = new Libro("84-376-0092-8","La vida es sueño","Pedro Calderón de la Barca","Teatro",(float)6.9);

		return arrL;
	}

	public static void listar(Libro arrL[]) {
		for (int i = 0; i < arrL.length; i++) System.out.println(arrL[i]);
	}
	
	public static Libro[] ordenarPor(Libro arrL[], int opt) {
		Libro aux = null;
		int i = 0, e = 0;
		for (i = 0; i < arrL.length - 1; i++) {
			for (e = i + 1; e < arrL.length; e++) {
				if (
					(arrL[i].getTitulo().compareTo(arrL[e].getTitulo()) > 0 && opt == 1) ||
					(arrL[i].getAutor().compareTo(arrL[e].getAutor()) > 0 && opt == 2) ||
					(arrL[i].getPrecio() > arrL[e].getPrecio() && opt == 3)
				) {
					aux = arrL[i];
					arrL[i] = arrL[e];
					arrL[e] = aux;
				}
			}
		}
		return arrL;
	}
	
	public static Libro[] ordenar(Scanner s, Libro arrL[]) {
		int opt = 0;
		printMenuOrd();
		opt = readOpt(s, 3);
		if (opt != 0) arrL = ordenarPor(arrL, opt);
		return arrL;
	}

	// --- MAIN ----------------------------------------------------------------
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int opt = 0;

		Libro arrL[] = new Libro[20];

		do {
			printMenu();
			opt = readOpt(s, 3);
			if (opt != 0) {
				switch (opt) {
					case 1:
						arrL = altasAuto2();
						break;
					case 2:
						listar(arrL);
						break;
					case 3:
						arrL = ordenar(s, arrL);
						break;
				}
			}
		} while(opt != 0);
	}

}
