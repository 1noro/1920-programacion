package ficheroBinarioPersonas;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.io.IOException;
import java.io.EOFException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
public class FicheroBinarioPersonas {
	static final String ruta = "E:\\JavaDAM_1\\FicherosDatos\\";
	static int menu(Scanner e) {
		int op = 0;
		System.out.println("\n\tMENU\n\t====\n");
		System.out.println("1.- Altas.");
		System.out.println("2.- Consultas.");
		System.out.println("3.- Listados.");
		System.out.println("4.- Fin.\n");
		do {
			try {
				System.out.print("\nTeclee opción (1-4)? ");
				op = e.nextInt();
			}catch(InputMismatchException ime) {
				op = Integer.MAX_VALUE;
				e.nextLine();}
		}while(op<1 || op>4 || op == Integer.MAX_VALUE);
		e.nextLine();
		return op;
	}
	static char  menuListados(Scanner e) {
		char op = 0;
		System.out.println("\n\tMENU LISTADOS\n\t==============\n");
		System.out.println("G.- General.");
		System.out.println("V.- Hombres.");
		System.out.println("M.- Mujeres.");
		System.out.println("E.- Ente edades.");
		System.out.println("F.- Voler a menu.\n");
		do {
			try {
				System.out.print("\tTeclee opción (G/V/M/E/F)? ");
				op = Character.toLowerCase((char) System.in.read());
				while(System.in.read() != '\n');
			}catch(IOException ioe) {	}
		}while ("gvmef".indexOf(op)==-1);
//		}while(op != 'g' && op != 'v' && op != 'm' && op != 'e' && op != 'f');
		
		
		return op;
	}
	static String convertirLetraAMayuscula(String nif) {
		String n = nif.substring(0,8);
		n += Character.toUpperCase(nif.charAt(8));
		return n;
	}
	static boolean comprobarNif(String nif) {
		String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
		boolean correcto = false;
		if(!nif.equalsIgnoreCase("999") && Character.toUpperCase(nif.charAt(8)) == letras.charAt((Integer.parseInt(nif.substring(0,8))%23)))
			correcto = true;
		return correcto;
	}
	static void altas(Scanner e) {
		String nif=null, nombre=null;
		char sexo = ' ';
		int edad=0;
		System.out.println("\n\tALTAS\n\t=====\n");
		try {
			ObjectOutputStream ficheroSal = new ObjectOutputStream( new FileOutputStream(ruta+"persona.dat"));
			do {
				System.out.print("Nif (999 para finalizar)? ");
				nif = e.nextLine();
			}while(!comprobarNif(nif) && !nif.equalsIgnoreCase( "999"));
			while(!nif.equalsIgnoreCase("999")) {
				do {
					System.out.print("Nombre (máximo 20 caracteres)? ");
					nombre = e.nextLine();
				}while(nombre.length()>20);
				System.out.print("Edad? ");
				edad = e.nextInt();
				e.nextLine();
				do {
					System.out.print("Sexo (V = Varon/M = mujer)? ");
					sexo = Character.toUpperCase((char) System.in.read());
					while(System.in.read() != '\n');
				}while(sexo != 'V' && sexo != 'M');
				nif = convertirLetraAMayuscula(nif);
				ficheroSal.writeUTF(nif);
				ficheroSal.writeUTF(nombre);
				ficheroSal.writeInt(edad);
				ficheroSal.writeChar(sexo);
				do {
					System.out.print("Nif (999 para finalizar)? ");
					nif = e.nextLine();
				}while(!comprobarNif(nif) && !nif.equalsIgnoreCase( "999"));
			}
			ficheroSal.close();
		}catch(IOException ioe) {}
		
	}
	static void consultas(Scanner e) {
		String nifBus = null;
		String nif = null, nombre = null;
		int edad = 0;
		char sexo = ' ';
		char otro = ' ';
		boolean fin = false,encontrado = false;
		System.out.println("\n\tCONSULTAS\n\t=========\n");
		do {
			do {
				System.out.print("Teclea nif a buscar? ");
				nifBus = e.nextLine();
			}while(!comprobarNif(nifBus));
			try {
				ObjectInputStream ficheroEnt = new ObjectInputStream (new FileInputStream(ruta +"persona.dat"));
				while(!fin){
					try {
						nif = ficheroEnt.readUTF();
						nombre = ficheroEnt.readUTF();
						edad = ficheroEnt.readInt();
						sexo = ficheroEnt.readChar();
						if (nifBus.equalsIgnoreCase(nif) ) { 
							encontrado = true;
							ficheroEnt.close();
						}
					}catch(EOFException eofe) {
						ficheroEnt.close();
						fin = true;
					}
				}
			}catch(IOException ioe) {}
			if(encontrado) {
				System.out.println("Nombre => "+nombre);
				System.out.println("Edad ===> "+edad);
				System.out.println("Sexo ====> "+sexo+"\n");
			}
			else
				System.out.println("\nNo existe nadie con el dni: "+nifBus+" en el fichero.\n");
			try {
				do {
					System.out.print("\tRealizar otra búsqueda (s/n)? ");
					otro = (char) System.in.read();
					while(System.in.read() != '\n');
				}while(otro != 's' && otro != 'n');
			}catch(IOException ioe) {}
			encontrado = false;
			fin = false;
		}while(otro=='s');
	}
	static String tabular(String cad) {
		String tab = "\t";
		int longcad = cad.length();
		if (longcad<8)
			tab = "\t\t\t";
		else
			if (longcad<16)
				tab ="\t\t";	
		return tab;
	}
	static void listadoGeneral() {
		boolean  fin = false;
		String nif = null, nombre = null;
		int edad = 0,contV = 0,contM=0,totEdM=0,totEdV=0;
		float mediaEdadV = 0,mediaEdadM = 0,mediaEdadG = 0;
		char sexo = ' ';
		System.out.println("\n\t\tLISTADO GENERAL\n\t\t================\n");
		try {
			ObjectInputStream ficheroEnt = new ObjectInputStream (new FileInputStream(ruta +"persona.dat"));
			System.out.println("Nif\tNombre\t\t\t\t\t\tEdad\tSexo\n-------------------------------------------------------------------------");
			while(!fin){
				try {
					nif = ficheroEnt.readUTF();
					nombre = ficheroEnt.readUTF();
					edad = ficheroEnt.readInt();
					sexo = ficheroEnt.readChar();
					System.out.println(nif+"\t"+nombre+tabular(nombre)+"\t"+edad+"\t\t"+sexo);
					if(sexo == 'V') {
						contV++;
						totEdV += edad;
					}
					else {
						contM++;
						totEdM += edad;
					}
				}catch(EOFException eof) {
					ficheroEnt.close();
					fin = true;
				}
			}
		}catch(IOException ioe) {}
		try {
			mediaEdadV = totEdV/contV;
		}catch(ArithmeticException ae) {
			mediaEdadV = 0;
		}
		try {
			mediaEdadM = totEdM/contM;
		}catch(ArithmeticException ae) {
			mediaEdadM = 0;
		}
		try {
			mediaEdadG = totEdM/contM;
		}catch(ArithmeticException ae) {
			mediaEdadG = 0;
		}
		System.out.println("\nTotal hombres listados ==> "+contV);
		System.out.println("Total mujeres listados ==> "+contM);
		System.out.println("Media de edad  hombres listados ==> "+mediaEdadV);
		System.out.println("Media de edad  mujeres listadas ==> "+mediaEdadV);
		System.out.println("\nMedia de edad  general ==> "+mediaEdadG);
	}
	static void listadoVaronesMujeres(char tl) {
		boolean  fin = false;
		String literal = " HOMBRES";
		if(tl == 'm') literal = "  MUJERES";
		String nif = null, nombre = null;
		int edad = 0,sumEd = 0,contPer = 0,edadMayor = 0, edadMenor=Integer.MAX_VALUE;
		float mediaEdad = 0;
		char sexo = ' ';
		System.out.println("\n\t\tLISTADO"+literal+"\n\t\t=================\n");
		try {
			ObjectInputStream ficheroEnt = new ObjectInputStream (new FileInputStream(ruta +"persona.dat"));
			System.out.println("Nif\tNombre\t\t\t\t\t\tEdad\tSexo\n-------------------------------------------------------------------------");
			while(!fin){
				try {
					nif = ficheroEnt.readUTF();
					nombre = ficheroEnt.readUTF();
					edad = ficheroEnt.readInt();
					sexo = ficheroEnt.readChar();
					if(tl == 'v' && sexo == 'V' || tl == 'm' && sexo == 'M')
						System.out.println(nif+"\t"+nombre+tabular(nombre)+"\t"+edad+"\t\t"+sexo);
					contPer++;
					sumEd += edad;
					if(edad>edadMayor)
						edadMayor = edad;
					if(edad<edadMenor)
						edadMenor = edad;
				}catch(EOFException eof) {
					ficheroEnt.close();
					fin = true;
				}
			}
		}catch(IOException ioe) {}
		try {
			mediaEdad = sumEd/contPer;
		}catch(ArithmeticException ae) {
			mediaEdad = 0;
		}
		if(edadMenor == Integer.MAX_VALUE) edadMenor = 0;
		System.out.println("\nTotal"+literal+" listados ==> "+contPer);
		System.out.println("Media de edad ==> "+mediaEdad);
		System.out.println("Edad más alta ==> "+edadMayor);
		System.out.println("Edad más baja ==> "+edadMenor);
	}
	static void listadoEntreEdades(Scanner e) {
		boolean fin = false;
		String nif = null, nombre = null;
		int edad = 0;
		char sexo = ' ';
		int edadMenor = 0,edadMayor = 0;
		System.out.println("\n\t\tLISTADO ENTRE EDADES\n\t\t====================\n");
		do {
			System.out.print("Edad menor.........? ");
			edadMenor = e.nextInt();
			System.out.print("Edad mayor..........? ");
			edadMayor = e.nextInt();
		}while(edadMenor > edadMayor);
		try {
			ObjectInputStream ficheroEnt = new ObjectInputStream (new FileInputStream(ruta +"persona.dat"));
			System.out.println("Nif\tNombre\t\t\t\t\t\tEdad\tSexo\n-------------------------------------------------------------------------");
			while(!fin){
				try {
					nif = ficheroEnt.readUTF();
					nombre = ficheroEnt.readUTF();
					edad = ficheroEnt.readInt();
					sexo = ficheroEnt.readChar();
					if(edad >= edadMenor  && edad <= edadMayor)
						System.out.println(nif+"\t"+nombre+tabular(nombre)+"\t"+edad+"\t\t"+sexo);
				}catch(EOFException eof) {
					ficheroEnt.close();
					fin = true;
				}
			}
		}catch(IOException ioe) {}
	}
	static void fin() {
		System.out.println("\n\n\n\n\tFINAL DEL PROGRAMA\n\t===================\n");
	}
	public static void main(String [] args) throws IOException {
		Scanner entrada = new Scanner(System.in);
		int opcion = 0;
		do {
			opcion = menu(entrada);
			switch(opcion) {
			case 1:
				altas(entrada);
				break;
			case 2:
				consultas(entrada);
				break;
			case 3:
				char o = ' ';
				do {
					o = menuListados(entrada);
					switch(o) {
					case 'g':
						listadoGeneral();
						break;
					case 'v':
						listadoVaronesMujeres(o);
						break;
					case 'm':
						listadoVaronesMujeres(o);
						break;
					case 'e':
						listadoEntreEdades(entrada);
					}
				}while(o != 'f');
				break;
				default:
					fin();
			}
		}while(opcion!=4);
		entrada.close();
	}
}
