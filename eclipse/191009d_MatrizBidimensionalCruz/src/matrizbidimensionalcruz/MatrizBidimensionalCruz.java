package matrizbidimensionalcruz;

public class MatrizBidimensionalCruz {

	public static void main(String[] args) {
		int tam = 8, ifi = 0, ico = 0;
		int m[][] = new int[tam][tam];
		
		// llenamos la matriz a 9
		for (ifi = 0; ifi < tam; ifi++) 
			for (ico = 0; ico < tam; ico++) 
				m[ifi][ico] = 9;
		
		// dibujamos el tablero
		for (ifi = 0; ifi < tam; ifi++) 
			for (ico = 0; ico < tam; ico++) 
				if (ifi == ico || (tam - 1) == (ifi + ico)) m[ifi][ico] = 1;
					else m[ifi][ico] = 0;
		
		// mostramos la matriz
		for (ifi = 0; ifi < tam; ifi++) {
			for (ico = 0; ico < tam; ico++) 
				System.out.print(m[ifi][ico] + " ");
			System.out.print("\n");
		}

	}

}
