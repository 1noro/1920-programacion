package mantAlum_00;
import java.io.IOException;
import java.io.RandomAccessFile;
public class MantAlum_00 {
	static final String ruta = "C:\\Users\\Marta\\Desktop\\Ficheros\\";
	String menu() throws IOException{
		Teclado t = new Teclado();
		System.out.println("\n\tMENU\n\t====\n");
		System.out.println("A.- Altas.");
		System.out.println("B.- Bajas.");
		System.out.println("M.- Modificaciones.");
		System.out.println("C.- Consultas. ");
		System.out.println("L.- Listados.");
		System.out.println("F.- Fin.");
		System.out.print("\n\n\tTeclee opcion? ");
		String op = t.leerString();
		return op;
	}
	void altas()throws IOException{
		Alumno av = new Alumno(0," ",0,0,' ');
		Teclado t = new Teclado();
		int numero;
		String nombre;
		float nota1, nota2;
		RandomAccessFile fich = new RandomAccessFile(ruta+"alumnos.dat","rw");
		System.out.println("\n\tALTAS\n\t=====\n");
		do{
			System.out.print("Número............: ");
			numero = t.leerInt();
		}while(numero == Integer.MIN_VALUE);
		while(numero!=0){
			fich.seek(numero * av.tamano());
			av.leerDeArchivo(fich);
			if(av.getNumero()  !=  0)
				System.out.println("\n\tEl registro ya existe.....\n");
			else{
				System.out.print("Nombre............: ");
				nombre = t.leerString();
				do{
					System.out.print("Nota1.............: ");
					nota1 = t.leerFloat();
				}while(nota1 == Float.MIN_VALUE || nota1 < 1 || nota1 > 10);
				do{
					System.out.print("Nota2.............: ");
					nota2 = t.leerFloat();
				}while(nota2 == Float.MIN_VALUE || nota2 < 1 || nota2 > 10);
	
				Alumno a = new Alumno(numero, nombre, nota1, nota2, ' ');
				if (numero * a.tamano()>fich.length())
					fich.seek(fich.length());
				while(numero*a.tamano()>fich.length())
					av.grabarEnArchivo(fich);
				fich.seek(numero * a.tamano());
				a.grabarEnArchivo(fich);
			}
			do{
				System.out.print("\n\nNúmero..........: ");
				numero = t.leerInt();
			}while(numero == Integer.MIN_VALUE);
		}
		fich.close();
	}
	
	void fin(){
		System.out.println("\n\n\n\tFINAL DEL PROGRAMA\n\t==================\n");
		}
		
	void modificaciones() throws IOException{
		Alumno a = new Alumno(0," ",0,0,' ');
		Teclado t = new Teclado();
		char otro;
		int numBus = 0,cm=0;
		String nombreN;
		float nota1N, nota2N;
		do{
			RandomAccessFile fich = new RandomAccessFile(ruta+"alumnos.dat","rw");
			System.out.println("\n\tMODIFICACIONES\n\t==============\n");
			do{
				System.out.print("Teclee número del alumno.....: ");
				numBus=t.leerInt();
			}while(numBus == Integer.MIN_VALUE);
			fich.seek(numBus * a.tamano());
			a.leerDeArchivo(fich);
			if (a.getNumero() == 0)
				System.out.println("\n\tEl alumno con el número: "+numBus+" no está registrado.\n");
			else{
				nombreN = a.getNombre();
				nota1N = a.getNota1();
				nota2N = a.getNota2();
				a.mostrarDatos(1);
				do{
					do{
						System.out.print("\n\tTeclee campo a modificar (1-3)?");
						cm=t.leerInt();
					}while(cm==Integer.MIN_VALUE || cm<1 || cm>3);
					switch(cm){
					case 1:
						System.out.print("Teclee nuevo nombre........: ");
						nombreN = t.leerString();
						break;
					case 2:
						do{
							System.out.print("Teclee primera nota.....: ");
							nota1N = t.leerFloat();
						}while (nota1N == Integer.MIN_VALUE || nota1N<1 || nota1N>10);
						break;
					default:
						do{
							System.out.print("Teclee segunda nota.....: ");
							nota2N = t.leerFloat();
						}while (nota2N == Integer.MIN_VALUE || nota2N<1 || nota2N>10);
						break;
						}
					do{
						System.out.print("\n\tOtro campo a modificar (s/n)? ");
						otro = t.leerChar();
					}while (Character.toLowerCase(otro) != 's' && Character.toLowerCase(otro) != 'n');
				}while (Character.toLowerCase(otro)=='s');
				do{
					System.out.print("\n\tConfirmar las modificaciones (s/n)? ");
					otro = t.leerChar();
				}while (Character.toLowerCase(otro) != 's' && Character.toLowerCase(otro) != 'n');
				if (Character.toLowerCase(otro)=='s'){
					a = new Alumno(numBus, nombreN, nota1N, nota2N, ' ');
					fich.seek(numBus*a.tamano());
					a.grabarEnArchivo(fich);
				}
				fich.close();
				}
			do{
				System.out.print("\n\tModificar otro alumno (s/n).....: ");
				otro = t.leerChar();
			}while(Character.toLowerCase(otro)!='s' && Character.toLowerCase(otro)!='n');
		}while(Character.toLowerCase(otro)=='s');
	}

	void bajas()throws IOException{
		Alumno a = new Alumno(0," ",0,0,' ');
		Teclado t = new Teclado();
		int numBus=0;
		char confirmar;
		System.out.println("\n\tBAJAS\n\t=====\n");
		RandomAccessFile fich = new RandomAccessFile(ruta+"alumnos.dat","rw");
		do{
			System.out.print("\nTeclee el número del alumno? ");
			numBus = t.leerInt();
		}while(numBus == Integer.MIN_VALUE);
		fich.seek(numBus*a.tamano());
		a.leerDeArchivo(fich);
		if(a.getNumero() == 0){
			System.out.println("\n\tEl alumno con el número: "+numBus+" no está registrado.\n");
		}
		else{
			a.mostrarDatos(2);
			do{
				System.out.print("\n\tDesea borrar el registro (s/n)? ");
				confirmar = t.leerChar();
			}while(Character.toLowerCase(confirmar)!='s' && Character.toLowerCase(confirmar)!='n');
			if (Character.toLowerCase(confirmar)=='s'){
				a=new Alumno(0," ",0,0,' ');
				fich.seek(numBus * a.tamano());
				a.grabarEnArchivo(fich);
				System.out.println("\n\tRegistro borrado correctamente\n");
			}
		}
		fich.close();
	}
//************************ Listado con medias ******************************
	void listados()throws IOException{
		Alumno a = new Alumno(0," ",0,0,' ');
		Teclado t = new Teclado();
		boolean fin = false;
		final int LINEAS = 5;
		int cp=0,cl=LINEAS+1,ca=0;
		RandomAccessFile fich = new RandomAccessFile(ruta+"alumnos.dat","r");
		do{
			while(!fin && cl < LINEAS){
				if(a.getNumero() != 0){
					if(a.getApto() == 'S'){
						a.mostrarDatos(3);
						ca++;
					}
					else
						a.mostrarDatos(0);
					cl++;
				}
				fin = a.leerDeArchivo(fich);
			}
			if (cl==LINEAS){
				System.out.println("\n\tPulse <Intro> para continuar....");
				t.leerSalto();
				}
			if(!fin){
				System.out.println("\t\t\t\tLISTADO\t\t\t\tPag.: "+ ++cp+"\n\t\t\t\t=======\n");
				System.out.println("Número\tNombre\t\t\t\tNota 1\t\tNota 2\tApto\tMedia");
				System.out.println("-----------------------------------------------------------------------------");
				cl=0;
			}
		}while(!fin);
			fich.close();
			System.out.println("\nTotal de aprobados listados............: "+ca);
			System.out.println("\n\n\t\tFIN DEL LISTADO \n");
			System.out.println("\n\tPulse <Intro> para continuar....");
			t.leerSalto();
	}	

	void consultas() throws IOException{
		Alumno a = new Alumno(0," ",0,0,' ');
		Teclado t = new Teclado();
		int numBus;
		System.out.println("\n\tCONSULTAS\n\t=========\n");
		RandomAccessFile fich = new RandomAccessFile(ruta+"alumnos.dat","r");
		do{
			System.out.print("\nTeclee el número del alumno? ");
			numBus = t.leerInt();
		}while(numBus == Integer.MIN_VALUE);
		fich.seek(numBus * a.tamano());
		a.leerDeArchivo(fich);
		if(a.getNumero() != 0)
			a.mostrarDatos(2);
		else
			System.out.println("\nNo existe ningún alumno con el número: "+numBus+" en el fichero.\n");
		fich.close();
		System.out.println("\n\tPulse <Intro> para continuar....");
		System.in.read();
		t.leerSalto();
	}
	public static void main(String[] args)throws IOException{
		MantAlum_00 ma = new MantAlum_00();
		String opcion;
		boolean fin=false;
		RandomAccessFile fs = new RandomAccessFile (ruta+"alumnos.dat","rw");
		fs.close();
		while (!fin){
			opcion = ma.menu();
			if(opcion.equalsIgnoreCase("A")) ma.altas();
			if(opcion.equalsIgnoreCase("B")) ma.bajas();
			if(opcion.equalsIgnoreCase("M")) ma.modificaciones();
			if(opcion.equalsIgnoreCase("C")) ma.consultas();
			if(opcion.equalsIgnoreCase("L")) ma.listados();
			if(opcion.equalsIgnoreCase("F")){
				ma.fin();
				fin=true;
			}
		}
	}
}
