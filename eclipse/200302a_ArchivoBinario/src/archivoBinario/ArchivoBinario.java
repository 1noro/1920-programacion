package archivoBinario;

//import java.io.*;
import java.io.IOException;
//import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ArchivoBinario {
	
	public static final String dir = "/opt/archivosJava/";
	public static final String filename = "bin01.dat";
	//public static final boolean overwriteFile = true;

	public static void main(String[] args) {
		int 
			t1[] = new int[10], 
			t2[] = new int[10],
			i = 0;
		
		// llenamos el vector con los multiplos de 5
		System.out.println("> Llenamos t1");
		for (i = 0; i < 10; i++) t1[i] = i * 5;
		
		// Escribimos
		System.out.println("> Escribimos de t1 al archivo");
		try {
			// FileOutputStream fos = new FileOutputStream(dir + filename);
			// ObjectOutputStream oos = new ObjectOutputStream(fos);
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(dir + filename));
			for (i = 0; i < 10; i++) oos.writeInt(t1[i]);
			oos.close();
		} catch (IOException ioe) {
			System.out.println("[FAIL] Error en grabación.");
		}
		
		// Leemos
		System.out.println("> Leemos del archivo a t2");
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(dir + filename));
			for (i = 0; i < 10; i++) {
				t2[i] = ois.readInt();
				/*try {
					t2[i] = ois.readInt();
			    } catch (EOFException e) {
			    	break;
			    }*/
			}
			ois.close();
				
		} catch (IOException ioe) {
			System.out.println("[FAIL] Error en lectura.");
		}
		
		// Visualizamos
		System.out.println("> Visualizamos t2");
		for (i = 0; i < 10; i++) System.out.println(t2[i]);
		
	}

}
