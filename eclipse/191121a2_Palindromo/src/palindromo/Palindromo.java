package palindromo;

import java.util.Scanner;

public class Palindromo {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String fraseIn = "";
		int i = 0, e = 0;
		boolean esPalindromo = true;
		char c1 = ' ', c2 = ' ';
		
		System.out.print("Frase: ");
		fraseIn = entrada.nextLine();
		entrada.close();
		fraseIn = fraseIn.toLowerCase();
		
		i = 0;
		e = ((fraseIn.length() - 1) - i);
		while (e >= i) {
			c1 = fraseIn.charAt(i);
			c2 = fraseIn.charAt(e);
			while (c1 == ' ') c1 = fraseIn.charAt(++i);
			while (c2 == ' ') c2 = fraseIn.charAt(--e);
			if (c1 != c2) esPalindromo = false;
			i++;
			e--; 
		}
		
		if (esPalindromo) System.out.println("Es palindromo.");
			else System.out.println("NO es palindromo.");
		
		// perfecto, da las vueltas justas

	}

}
