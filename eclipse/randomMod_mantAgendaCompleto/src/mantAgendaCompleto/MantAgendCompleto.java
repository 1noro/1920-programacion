package mantAgendaCompleto;

import java.io.IOException;
import java.io.RandomAccessFile;

public class MantAgendCompleto {
    
    static final String ruta = "/opt/jarchivos/";
    
    String menu() throws IOException {
        Teclado t = new Teclado();
        String op = null;
        System.out.println("\n\tMENU\n\t====\n");
        System.out.println("A.- Altas.");
        System.out.println("B.- Bajas.");
        System.out.println("C.- Consultas. ");
        System.out.println("M.- Modificaciones.");
        System.out.println("L.- Listados.");
        System.out.println("F.- Fin.");
        System.out.print("\n\n\tTeclee opción? ");
        op = t.leerString();
        return op;
    }
    
    void altas() throws IOException {
        
        Persona pv = new Persona(0, "nombre", 0);
        Teclado t = new Teclado();
        int codigo, edad;
        String nombre;
        char confirmar = ' ';
        RandomAccessFile fich = new RandomAccessFile(ruta+"agenda.dat","rw");
        
        System.out.println("\n\tALTAS\n\t=====\n");
        
        do {
            System.out.print("Teclear código (0 para fin)...: ");
            codigo = t.leerInt();
        } while (codigo == Integer.MIN_VALUE);
        
        while (codigo != 0) {
            fich.seek(codigo * pv.tamano());
            pv.leerDeArchivo(fich);
            
            if(pv.getCodigo() != 0 && (codigo * pv.tamano()) < fich.length()) {
                System.out.println("\n\tLa persona ya está dada de alta.....\n");
            } else {
                
                do {
                    System.out.print("Teclear nombre............: ");
                    nombre = t.leerString();
                } while (nombre.length() > 20);
                
                System.out.print("Teclear edad..................: ");
                edad = t.leerInt();
                
                do {
                    System.out.print("\n\tConfirmar el alta (s/n)? ");
                    confirmar = Character.toLowerCase(t.leerChar());
                } while (confirmar != 's' && confirmar != 'n');
                
                if (confirmar == 's'){
                    Persona p = new Persona(codigo, nombre, edad);
                    
                    if ((codigo * p.tamano()) > fich.length()) fich.seek(fich.length());
                    while(codigo*p.tamano()>fich.length()) pv.grabarEnArchivo(fich);
                    fich.seek(codigo * p.tamano());
                    p.grabarEnArchivo(fich);
                }
            }
            
            do{
                System.out.print("Teclear código (0 para fin)...: ");
                codigo = t.leerInt();
            } while (codigo == Integer.MIN_VALUE);
        }
        
        fich.close();
    }
    
    void bajas() throws IOException {
        
        Persona p = new Persona(0, " ", 0);
        Teclado t = new Teclado();
        int codBus=0;
        char confirmar;
        
        System.out.println("\n\tBAJAS\n\t=====\n");
        RandomAccessFile fich = new RandomAccessFile(ruta + "agenda.dat", "rw");
        
        do {
            System.out.print("\nTeclee el código de la persona? ");
            codBus = t.leerInt();
        } while (codBus == Integer.MIN_VALUE);
        
        fich.seek(codBus * p.tamano());
        p.leerDeArchivo(fich);
        
        if(p.getCodigo() == 0){
            System.out.println("\n\tEl alumno con el código: "+codBus+" no est� registrado.\n");
        }
        
        else {
            p.mostrarDatos(0);
            do{
                System.out.print("\n\tDesea borrar el registro (s/n)? ");
                confirmar = t.leerChar();
            } while(Character.toLowerCase(confirmar) != 's' && Character.toLowerCase(confirmar) != 'n');
            if (Character.toLowerCase(confirmar) == 's') {
                p = new Persona(0, " ", 0);
                fich.seek(codBus * p.tamano());
                p.grabarEnArchivo(fich);
                System.out.println("\n\tRegistro borrado correctamente\n");
            }
        }
        fich.close();
    }
    
    void modificaciones() throws IOException {
        
        Persona p = new Persona(0, " ", 0);
        Teclado t = new Teclado();
        char otro;
        int codBus = 0,cm=0, edadN;
        String nombreN;
        
        do {
            RandomAccessFile fich = new RandomAccessFile(ruta + "agenda.dat", "rw");
            System.out.println("\n\tMODIFICACIONES\n\t==============\n");
            
            do{
                System.out.print("Teclee código de la persona.....: ");
                codBus=t.leerInt();
            } while (codBus == Integer.MIN_VALUE);
            
            fich.seek(codBus * p.tamano());
            p.leerDeArchivo(fich);
            
            if (p.getCodigo() == 0) {
                System.out.println("\n\tEl alumno con el número: " + codBus + " no está registrado.\n");
            } else {
                nombreN = p.getNombre();
                edadN = p.getEdad();
                p.mostrarDatos(1);
                
                do {
                    do {
                        System.out.print("\n\tTeclee campo a modificar (1-3)?");
                        cm = t.leerInt();
                    } while(cm == Integer.MIN_VALUE || cm < 1 || cm > 3);
                    
                    switch (cm) {
                        case 1:
                            System.out.print("Teclee nuevo nombre........: ");
                            nombreN = t.leerString();
                            break;
                        case 2:
                            do{
                                System.out.print("Teclee nueva edad.....: ");
                                edadN = t.leerInt();
                            } while (edadN == Integer.MIN_VALUE);
                            break;
                    }
                    
                    do {
                        System.out.print("\n\tOtro campo a modificar (s/n)? ");
                        otro = t.leerChar();
                    } while (Character.toLowerCase(otro) != 's' && Character.toLowerCase(otro) != 'n');
                    
                } while (Character.toLowerCase(otro) == 's');
                
                do {
                    System.out.print("\n\tConfirmar las modificaciones (s/n)? ");
                    otro = t.leerChar();
                } while (Character.toLowerCase(otro) != 's' && Character.toLowerCase(otro) != 'n');
                
                if (Character.toLowerCase(otro) == 's') {
                    p = new Persona(codBus, nombreN, edadN);
                    fich.seek(codBus * p.tamano());
                    p.grabarEnArchivo(fich);
                }
                
                fich.close();
            }
            
            do {
                System.out.print("\n\tModificar otro alumno (s/n).....: ");
                otro = t.leerChar();
            } while (Character.toLowerCase(otro) != 's' && Character.toLowerCase(otro) != 'n');
        } while (Character.toLowerCase(otro) == 's');
    }
    
//***********************Consultas***********************************************
    
    void consultas() throws IOException {
        Teclado t = new Teclado();
        String op;
        System.out.println("\n\tCONSULTAS\n\t=========\n");
        
        do{
            System.out.print("\nPor código (C) o por nombre(N)? ");
            op = t.leerString();
        } while (!op.equalsIgnoreCase("C") && !op.equalsIgnoreCase("N"));
        
        if(op.equalsIgnoreCase("C")) consultasCodigo();
        else consultasNombre();
    }
    
    void consultasCodigo() throws IOException {
        
        Persona p = new Persona(0, "", 0);
        Teclado t = new Teclado();
        int codBus;
        RandomAccessFile fich = new RandomAccessFile(ruta + "agenda.dat", "r");
        
        do {
            System.out.print("\nTeclee el código a buscar? ");
            codBus = t.leerInt();
        } while (codBus == Integer.MIN_VALUE);
        
        fich.seek(codBus * p.tamano());
        p.leerDeArchivo(fich);
        
        if(p.getCodigo() != 0) p.mostrarDatos(0);
        else System.out.println("\nNo existe ninguna persona con el número: " + codBus + " en el fichero.\n");
        fich.close();
    }
    
    void consultasNombre() throws IOException {
        Persona p = new Persona(0, "", 0);
        Teclado t = new Teclado();
        RandomAccessFile fich = new RandomAccessFile(ruta + "agenda.dat", "r");
        String nombre;
        long pos;
        
        System.out.println("Teclee el nombre a buscar: ");
        
        nombre = t.leerString();
        pos = localizarNombre(nombre, fich);
        
        if (pos != Long.MAX_VALUE){
            fich.seek(pos);
            p.leerDeArchivo(fich);
            if(p.getCodigo() != 0) p.mostrarDatos(0);
        } else System.out.println("Ese nombre no está registrado. ");
        fich.close();
    }
    
    long localizarNombre(String nombre, RandomAccessFile fich) throws IOException {
        Persona p = new Persona(0, nombre, 0);
        boolean fin = false;
        long pos;
        String nombreAux;
        nombreAux = p.construirNombre();
        pos = fich.getFilePointer();
        fin = p.leerDeArchivo(fich);
        
        while(!p.getNombre().equalsIgnoreCase(nombreAux) && !fin) {
            pos = fich.getFilePointer();
            fin = p.leerDeArchivo(fich);
        }
        
        if(fin) pos = Long.MAX_VALUE;
        return pos;
    }
    
//**********************************Listado*****************************************************
    
    void listados() throws IOException {
        boolean fin = false;
        Persona p = new Persona(0, " ", 0);
        System.out.println("\n\tLISTADO\n\t=======\n");
        RandomAccessFile fich = new RandomAccessFile (ruta + "agenda.dat", "rw");
        
        while(!fin) {
            if(p.getCodigo() != 0) p.mostrarDatos(2);
            fin = p.leerDeArchivo(fich);
        }
        fich.close();
    }
    
    void fin(){
        System.out.println("\n\n\n\tFINAL DEL PROGRAMA\n\t==================\n");
    }
    
    public static void main(String[] args) throws IOException {
        MantAgendCompleto a = new MantAgendCompleto();
        String opcion;
        boolean fin=false;
        RandomAccessFile fs = new RandomAccessFile (ruta+"agenda.dat","rw");
        fs.close();
        
        while (!fin){
            opcion = a.menu();
            
            if(opcion.equalsIgnoreCase("A")) a.altas();
            if(opcion.equalsIgnoreCase("B")) a.bajas();
            if(opcion.equalsIgnoreCase("C")) a.consultas();
            if(opcion.equalsIgnoreCase("M")) a.modificaciones();
            if(opcion.equalsIgnoreCase("L")) a.listados();
            if(opcion.equalsIgnoreCase("F")){
                a.fin();
                fin=true;
            }
            
        }
    }

}
