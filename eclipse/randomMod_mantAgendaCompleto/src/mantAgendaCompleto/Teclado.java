package mantAgendaCompleto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Teclado {
    
    String leerString() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        return (s);
    }
    
    int leerInt() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int i;
        String s=br.readLine();
        try {
            i = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            i = Integer.MIN_VALUE;
        }
        return i;
    }
    
    char leerChar() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        char c;
        String s = br.readLine();
        c = s.charAt(0);
        return c;
    }
    
    void leerSalto() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        br.readLine();
    }
    
}
