package readDataFromFile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ReadDataFromFile {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String dir = "/opt/archivosJava/";
		String filename = "personas.dat";
		String linea = "";
		String nombre = "", edad = "";
		int contP = 0, sumE = 0;
		
		// ESCRIBIR ARCHIVO 
		try {
			//TRUE para que no sobreescriba
			FileWriter fw = new FileWriter(dir + filename, true);
			BufferedWriter bw = new BufferedWriter(fw);
			System.out.print("> Nombre ('*' para fin): ");
			nombre = s.nextLine();
			while (!nombre.equals("*")) {
				System.out.print("> Edad: ");
				edad = s.nextLine();
				bw.write(nombre + ' ' + edad);
				bw.newLine();
				System.out.print("> Nombre ('*' -> fin): ");
				nombre = s.nextLine();
			}
			bw.close();
			fw.close();
		} catch(IOException ioe) {}
		s.close();
		
		// LEER ARCHIVO
		System.out.println();
		try {
			FileReader fr = new FileReader(dir + filename);
			BufferedReader br = new BufferedReader(fr);
			linea = br.readLine();
			while (linea != null) {
				String [] strPart = linea.split(" ");
				if (strPart[0].length() >= 8)
					System.out.println("" + strPart[0] + "\t" + strPart[1]);
				else
					System.out.println("" + strPart[0] + "\t\t" + strPart[1]);
				sumE += Integer.parseInt(strPart[1]);
				contP++;
				linea = br.readLine();
			}
			br.close();
			fr.close();
		} catch(IOException ioe) {}
		
		System.out.println();
		System.out.println("#N. personas: " + contP);
		System.out.println("#AVG edad personas: " + (sumE/contP));
		
	}

}
