package mantArtCompleto_02B;
import java.io.IOException;
import java.io.RandomAccessFile;
public class MantArtCompleto_02B {
	static final String ruta = "C:\\Users\\Marta\\Desktop\\Ficheros\\";	//manera de Emilio
	String menu(int opcion) throws IOException{
		Teclado t = new Teclado();
		String op = "";
		switch(opcion) {
		case 0:
			System.out.println("\n\tMENU\n\t====\n");
			System.out.println("A.- Altas.");
			System.out.println("B.- Bajas.");
			System.out.println("M.- Modificaciones.");
			System.out.println("C.- Consultas. ");
			System.out.println("L.- Listados.");
			System.out.println("F.- Fin.");
			System.out.print("\n\n\tTeclee opci�n? ");
			op = t.leerString();
			break;
		case 1:
			int opl = 0;
			do {
				System.out.println("\n\tMENU LISTADOS\n\t=============\n");
				System.out.println("1.- General.");
				System.out.println("2.- Entre L�mites.");
				System.out.println("3.- Pedidos.");
				System.out.println("4.- Volver al menu.");
				do {
					System.out.println("\n\n\tTeclee opci�n? ");
					opl = t.leerInt();
				}while(opl == Integer.MIN_VALUE || opl < 1 || opl > 4);
				switch(opl) {
				case 1:
					listadoGeneral();
					break;
				case 2:
					listadoEntreLimites();
					break;
				case 3:
					listadoPedidos();
					break;
				}
			}while(opl != 4);			
		}		
		return op;
	}
	void altas()throws IOException{
		Articulo av = new Articulo(0," ",0,0,0,0,' ');
		Teclado t = new Teclado();
		int codigo;
		String denominacion;
		double stockAct, stockMin, stockMax;
		float precio;
		char confirmar;
		RandomAccessFile fich = new RandomAccessFile(ruta+"articulosM2.dat","rw");
		System.out.println("\n\tALTAS\n\t=====\n");
		do{
			System.out.print("C�digo............: ");
			codigo = t.leerInt();
		}while(codigo == Integer.MIN_VALUE);
		while(codigo!=0){
			fich.seek(codigo * av.tamano());
			av.leerDeArchivo(fich);
			if(av.getCodigo()  !=  0)
				System.out.println("\n\tEl registro ya existe.....\n");
			else{
				do {
					System.out.print("Denominaci�n............: ");
					denominacion = t.leerString();
				}while(denominacion.length()>20);
				do{
					System.out.print("Stock Actual.............: ");
					stockAct = t.leerDouble();
				}while(stockAct == Double.MIN_VALUE);
				do{
					do{
						System.out.print("Stock M�nimo.............: ");
						stockMin = t.leerDouble();
					}while(stockMin == Double.MIN_VALUE);
					do{
						System.out.print("Stock M�ximo.............: ");
						stockMax = t.leerDouble();
					}while(stockMax == Double.MIN_VALUE);
				}while(stockMax<stockMin);
				do{
					System.out.print("Precio.............: ");
					precio = t.leerFloat();
				}while(precio == Float.MIN_VALUE);
				do{
					System.out.print("\n\tConfirmar el alta (s/n)? ");
					confirmar = Character.toLowerCase(t.leerChar());
				}while (confirmar != 's' && confirmar != 'n');
				if (confirmar=='s'){
				Articulo a = new Articulo(codigo, denominacion, stockAct, stockMin, stockMax, precio, ' ');
				if (codigo * a.tamano()>fich.length())
					fich.seek(fich.length());
				while(codigo*a.tamano()>fich.length())
					av.grabarEnArchivo(fich);
				fich.seek(codigo * a.tamano());
				a.grabarEnArchivo(fich);
			  }
			}
			do{
				System.out.print("\n\nC�digo..........: ");
				codigo = t.leerInt();
			}while(codigo == Integer.MIN_VALUE);
		}
		fich.close();
	}
	
	void fin(){
		System.out.println("\n\n\n\tFINAL DEL PROGRAMA\n\t==================\n");
	}
		
	void modificaciones() throws IOException{
		Articulo a = new Articulo(0," ",0,0,0,0,' ');
		Teclado t = new Teclado();
		char otro;
		int codBus = 0,cm=0;
		String denominacionN;
		double stockActN, stockMinN, stockMaxN;
		float precioN;
		do{
			RandomAccessFile fich = new RandomAccessFile(ruta+"articulosM2.dat","rw");
			System.out.println("\n\tMODIFICACIONES\n\t==============\n");
			do{
				System.out.print("Teclee c�digo del alumno.....: ");
				codBus=t.leerInt();
			}while(codBus == Integer.MIN_VALUE);
			fich.seek(codBus * a.tamano());
			a.leerDeArchivo(fich);
			if (a.getCodigo() == 0)
				System.out.println("\n\tEl alumno con el c�digo: "+codBus+" no est� registrado.\n");
			else{
				denominacionN = a.getDenominacion();
				stockActN = a.getStockAct();
				stockMinN = a.getStockMin();
				stockMaxN = a.getStockMax();
				precioN = a.getPrecio();
				a.mostrarDatos(1);
				do{
					do{
						System.out.print("\n\tTeclee campo a modificar (1-5)?");
						cm=t.leerInt();
					}while(cm==Integer.MIN_VALUE || cm<1 || cm>5);
					switch(cm){
					case 1:
						do {
							System.out.print("Teclee nueva denominaci�n........: ");
							denominacionN = t.leerString();
						}while(denominacionN.length()>20);					
						break;
					case 2:
						do{
							System.out.print("Teclee stock actual.....: ");
							stockActN = t.leerDouble();
						}while (stockActN == Double.MIN_VALUE);
						break;
					case 3:
						do{
							System.out.print("Teclee stock m�nimo.....: ");
							stockMinN = t.leerDouble();
						}while (stockMinN == Double.MIN_VALUE || stockMaxN<stockMinN);
						break;
					case 4:
						do{
							System.out.print("Teclee stock m�ximo.....: ");
							stockMaxN = t.leerDouble();
						}while (stockMaxN == Double.MIN_VALUE || stockMaxN<stockMinN);
						break;
					default:
						do{
							System.out.print("Teclee precio.....: ");
							precioN = t.leerFloat();
						}while (precioN == Float.MIN_VALUE);
						break;
						}
					do{
						System.out.print("\n\tOtro campo a modificar (s/n)? ");
						otro = t.leerChar();
					}while (Character.toLowerCase(otro) != 's' && Character.toLowerCase(otro) != 'n');
				}while (Character.toLowerCase(otro)=='s');
				do{
					System.out.print("\n\tConfirmar las modificaciones (s/n)? ");
					otro = t.leerChar();
				}while (Character.toLowerCase(otro) != 's' && Character.toLowerCase(otro) != 'n');
				if (Character.toLowerCase(otro)=='s'){
					a = new Articulo(codBus, denominacionN, stockActN, stockMinN, stockMaxN, precioN, ' ');
					fich.seek(codBus*a.tamano());
					a.grabarEnArchivo(fich);
				}
				fich.close();
				}
			do{
				System.out.print("\n\tModificar otro alumno (s/n).....: ");
				otro = t.leerChar();
			}while(Character.toLowerCase(otro)!='s' && Character.toLowerCase(otro)!='n');
		}while(Character.toLowerCase(otro)=='s');
	}

	void bajas()throws IOException{
		Articulo a = new Articulo(0," ",0,0,0,0,' ');
		Teclado t = new Teclado();
		int codBus=0;
		char confirmar;
		System.out.println("\n\tBAJAS\n\t=====\n");
		RandomAccessFile fich = new RandomAccessFile(ruta+"articulosM2.dat","rw");
		do{
			System.out.print("\nTeclee el n�mero del alumno? ");
			codBus = t.leerInt();
		}while(codBus == Integer.MIN_VALUE);
		fich.seek(codBus*a.tamano());
		a.leerDeArchivo(fich);
		if(a.getCodigo() == 0){
			System.out.println("\n\tEl alumno con el n�mero: "+codBus+" no est� registrado.\n");
		}
		else{
			a.mostrarDatos(2);
			do{
				System.out.print("\n\tDesea borrar el registro (s/n)? ");
				confirmar = t.leerChar();
			}while(Character.toLowerCase(confirmar)!='s' && Character.toLowerCase(confirmar)!='n');
			if (Character.toLowerCase(confirmar)=='s'){
				a=new Articulo(0," ",0,0,0,0,' ');
				fich.seek(codBus * a.tamano());
				a.grabarEnArchivo(fich);
				System.out.println("\n\tRegistro borrado correctamente\n");
			}
		}
		fich.close();
	}
	
	void consultas() throws IOException{
		Articulo a = new Articulo(0," ",0,0,0,0,' ');
		Teclado t = new Teclado();
		int codBus;
		System.out.println("\n\tCONSULTAS\n\t=========\n");
		RandomAccessFile fich = new RandomAccessFile(ruta+"articulosM2.dat","r");
		do{
			System.out.print("\nTeclee el c�digo del alumno? ");
			codBus = t.leerInt();
		}while(codBus == Integer.MIN_VALUE);
		fich.seek(codBus * a.tamano());
		a.leerDeArchivo(fich);
		if(a.getCodigo() != 0)
			a.mostrarDatos(2);
		else
			System.out.println("\nNo existe ning�n alumno con el n�mero: "+codBus+" en el fichero.\n");
		fich.close();
		System.out.println("\n\tPulse <Intro> para continuar....");
		System.in.read();
		t.leerSalto();
	}
	
//************************ Listados ******************************

	void listadoGeneral()throws IOException{
		Articulo a = new Articulo(0," ",0,0,0,0,' ');
		Teclado t = new Teclado();
		boolean fin = false;
		final int LINEAS = 4;
		int cp=0,cl=LINEAS+1;
		RandomAccessFile fich = new RandomAccessFile(ruta+"articulosM2.dat","r");
		do{
			while(!fin && cl < LINEAS){
				if(a.getCodigo() != 0){	
//					System.out.println("Posici�n de puntero.........: "+fich.getFilePointer());
					a.mostrarDatos(0);
					cl++;
				}
				fin = a.leerDeArchivo(fich);
			}
			if (cl==LINEAS){
				System.out.println("\n\tPulse <Intro> para continuar....");
				t.leerSalto();
			}
			if(!fin){
				System.out.println("\t\t\t\tLISTADO\t\t\t\tPag.: "+ ++cp+"\n\t\t\t\t=======\n");
				System.out.println("C�digo\tDenominaci�n\t\tStock actual\tStock m�nimo\tStock m�ximo\tPrecio\tAviso");
				System.out.println("------------------------------------------------------------------------------------------------");
				cl=0;
			}
		}while(!fin);
			fich.close();
			System.out.println("\n\n\t\tFIN DEL LISTADO \n");
			System.out.println("\n\tPulse <Intro> para continuar....");
			t.leerSalto();
	}	

	void listadoEntreLimites() throws IOException{
		Articulo a = new Articulo(0," ",0,0,0,0,' ');
		Teclado t = new Teclado();
		boolean fin = false;
		final int LINEAS = 4;
		int cp=0,cl=LINEAS+1;
		int codMax = 0, codMin = 0;
		System.out.println("\n\tLISTADO ENTRE L�MITES\n\t=====================\n");
		do {
			do {
				System.out.print("L�mite inferior.........: ? ");
				codMin = t.leerInt();
			}while(codMin == Integer.MIN_VALUE);
			do {
				System.out.print("L�mite superior..........: ? ");
				codMax = t.leerInt();
			}while(codMax == Integer.MAX_VALUE);			
		}while(codMin > codMax);
		RandomAccessFile fich = new RandomAccessFile(ruta+"articulosM2.dat","r");
		fich.seek(codMin * a.tamano());
		do {
			fin = a.leerDeArchivo(fich);
		}while(a.getCodigo()==0 && !fin);		
		do {				
			while(!fin && cl < LINEAS){			
				a.mostrarDatos(0);
				cl++;				
				do {
					fin = a.leerDeArchivo(fich);
				}while(a.getCodigo()==0 && !fin);
				if(a.getCodigo() > codMax){	
					fin = true;
				}
			}
			if (cl==LINEAS){
				System.out.println("\n\tPulse <Intro> para continuar....");
				t.leerSalto();
			}
			if(!fin){
				System.out.println("\n\n\tLISTADO ENTRE L�MITES\t\tPag.: "+ ++cp+"\n\t=====================\n");
				System.out.println("C�digo\tDenominaci�n\t\tStock actual\tStock m�nimo\tStock m�ximo\tPrecio\tAviso");
				System.out.println("---------------------------------------------------------------------------------------------");
				cl=0;
			}
		}while(!fin);
		fich.close();
		System.out.println("\n\n\t\tFIN DEL LISTADO \n");
		System.out.println("\n\tPulse <Intro> para continuar....");
		t.leerSalto();									
	}
	void listadoPedidos() throws IOException{
		Articulo a = new Articulo(0," ",0,0,0,0,' ');
		Teclado t = new Teclado();
		boolean fin = false;
		double unidadesPedidas = 0;
		float precioPedido = 0, totalPagina = 0, sumaSigue = 0;
		final int LINEAS  = 4;
		int cp = 0, cl = LINEAS+1;
		RandomAccessFile fich = new RandomAccessFile(ruta+"articulosM2.dat","r");
		do {
			fin = a.leerDeArchivo(fich);
		}while(a.getCodigo()==0 && !fin);
		do {
			while(!fin && cl < LINEAS) {
				if(a.getAviso() == 'B') {
					unidadesPedidas = a.getStockMax() - a.getStockAct();
					precioPedido = (float) unidadesPedidas * a.getPrecio();
					lineasDetalle(a.getCodigo(), a.getDenominacion(), unidadesPedidas, a.getPrecio(), precioPedido);				
					totalPagina += precioPedido;
					cl++;
				}
				do {
					fin = a.leerDeArchivo(fich);
				}while(a.getCodigo()==0 && !fin);
			}
			if(cl == LINEAS) {
				sumaSigue += totalPagina;
				System.out.println("\n\t\t\tTotal p�gina.............................: "+totalPagina);
				System.out.println("\t\t\tSuma y sigue...........................: "+sumaSigue);
				totalPagina = 0;
				System.out.println("\n\tPulse <Intro> para continuar....");
				t.leerSalto();
			}
			if(!fin) {
				System.out.println("\t\t\t\tLISTADO PEDIDOS\t\t\t\tPag.: "+ ++cp+"\n\t\t\t\t===============\n");
				System.out.println("C�digo\tDenominaci�n\t\tUds Pedidas\tPrecio\tPrecio Pedido");
				System.out.println("---------------------------------------------------------------------");
				cl=0;
			}
		}while(!fin);
		sumaSigue += totalPagina;
		System.out.println("\n\t\t\tTotal p�gina.............................: "+totalPagina);
		System.out.println("\t\t\tSuma y sigue...........................: "+sumaSigue);
		fich.close();
		System.out.println("\n\n\tFIN DEL LISTADO \n");
		System.out.println("\n\tPulse <Intro> para continuar....");
		t.leerSalto();		
	}
	static void lineasDetalle(int codigo,String denominacion,double unidadesPedidas,float precio,float precioPedido) {
		System.out.println(codigo+"\t"+denominacion+"\t"+unidadesPedidas+"\t\t"+precio+"\t"+precioPedido);
	}

	public static void main(String[] args)throws IOException{
		MantArtCompleto_02B ma = new MantArtCompleto_02B();
		String opcion;
		boolean fin=false;
		RandomAccessFile fs = new RandomAccessFile (ruta+"articulosM2.dat","rw");
		fs.close();
		while (!fin){
			opcion = ma.menu(0);
			if(opcion.equalsIgnoreCase("A")) ma.altas();
			if(opcion.equalsIgnoreCase("B")) ma.bajas();
			if(opcion.equalsIgnoreCase("M")) ma.modificaciones();
			if(opcion.equalsIgnoreCase("C")) ma.consultas();
			if(opcion.equalsIgnoreCase("L")) ma.menu(1);	
			if(opcion.equalsIgnoreCase("F")){
				ma.fin();
				fin=true;
			}
		}
	}
}