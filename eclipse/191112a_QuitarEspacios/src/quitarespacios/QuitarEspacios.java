package quitarespacios;

import java.util.Scanner;

public class QuitarEspacios {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		String a = "", b = "";
		int i = 0;
		System.out.print("Frase: ");

		a = entrada.nextLine();
		entrada.close();
		
		b = "";
		for (i = 0; i < a.length(); i++) 
			if (a.charAt(i) != ' ') b += a.charAt(i);
		System.out.println("Frase sin espacios: " + b);

	}

}
