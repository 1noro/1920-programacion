package tablamultiplicar_20191028a;

public class TablaMultiplicar_20191028a {
	public static void main(String[] args) {
		int num = 7, min = 0, max = 10, i;
		for (i = min; i <= max; i++) {
			System.out.print(num + " * " + i + " = " + (num * i) + "\n");
		}
	}
}
