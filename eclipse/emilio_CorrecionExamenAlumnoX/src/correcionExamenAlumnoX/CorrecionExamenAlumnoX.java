package correcionExamenAlumnoX;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class CorrecionExamenAlumnoX {
	// static final String ruta = "C:\\Datos"; // faltan las barras del final
	static final String ruta = "/opt/jarchivos/"; 
	public static void main(String args[]) {
		Scanner entrada = new Scanner(System.in);
		int opcion = 0;
		do {
			opcion = menu(entrada);
			switch(opcion) {
			case 1:
				altas(entrada);
				break;
			case 2:
				visualizar_fichero();
				break;
			case 3:
				ficheros_sexo();
				break;
			default:
				fin();
			}
		}while(opcion!=4);
		entrada.close();
	}
	
	static int menu(Scanner e) {
		int op = 0;
		System.out.println("\n\tMENU\n\t====\n");
		System.out.println("1.- Altas.");
		System.out.println("2.- Visualizar fichero.");
		System.out.println("3.- Crear ficheros por sexo.");
		System.out.println("4.- Fin");
		do {
			System.out.print("\tTeclee una opción (1-4)!");
			op = e.nextInt();
		}while(op<1 || op>4);
		e.nextLine();
		return op;
	}
	static void altas(Scanner e) {
		String nombre=null,edad=null,sexo=null;
		System.out.println("\n\tALTAS\n\t=====");
		try {
			BufferedWriter fichero = new BufferedWriter(new FileWriter(ruta+"personas.txt",true));
			do {
				System.out.print("Nombre (Pulse xxx para finalizar ) ");
				nombre = e.nextLine();
				nombre.toUpperCase();
			}while (!nombre.equalsIgnoreCase("XXX"));{  
				System.out.print("Introduzca la edad: ");
				edad = e.next();		
				e.nextLine();
				do {
					System.out.println("Introduzca el sexo:");
					sexo=e.nextLine();
					sexo.toUpperCase();
				}while (!sexo.equalsIgnoreCase("V||M"));{ 
				
				fichero.write(nombre);
				fichero.newLine();
				fichero.write(edad);
				fichero.newLine();
				fichero.write(sexo);
				fichero.newLine();
				do {
					System.out.print("Nombre (Pulse xxx para finalizar ) ");
					nombre = e.nextLine();
					nombre.toUpperCase();
				}while (!nombre.equalsIgnoreCase("XXX"));
			}
			fichero.close();
			}
		}catch(IOException ioe) {}
			}
	static String tabular(String cad) {
		String t = "\t\t";
		if (cad.length()<8)
			t = "\t\t\t";
		return t;
	}
	static void visualizar_fichero() {
		String nombre = null, edad = null,sexo=null;
		System.out.println("\n\tLISTADO\n\t=======\n");
		System.out.println("Nombre\t\t\tEdad\t\t\tSexo\n----------------------------");
		try {
			BufferedReader leer = new BufferedReader(new FileReader(ruta+"personas.txt"));
			nombre = leer.readLine();
			while(nombre != null) {
				edad = leer.readLine();
				sexo = leer.readLine();
				System.out.println(nombre+tabular(nombre)+edad+tabular(edad)+sexo+tabular(sexo));
			}
			leer.close();
		}catch(IOException ioe) {}
	}

	static void ficheros_sexo() {
		String sexo=null,edad=null,nombre=null;
		try {
			BufferedReader leer = new BufferedReader(new FileReader(ruta+"personas.txt"));
			sexo=leer.readLine();
			if(sexo.equalsIgnoreCase("M")){
				do {
					try {
						BufferedWriter fichero = new BufferedWriter(new FileWriter(ruta+"mujeres.txt",true));
						nombre=leer.readLine();
						edad=leer.readLine();
						sexo=leer.readLine();
						fichero.write(nombre);
						fichero.newLine();
						fichero.write(edad);
						fichero.newLine();
						fichero.write(sexo);
						fichero.newLine();
						
						leer.close();
						fichero.close();
					}catch(IOException ioe) {}
				}while(nombre!=null);
					
			}else {
				do {
					try {
						BufferedWriter fichero = new BufferedWriter(new FileWriter(ruta+"hombres.txt",true));
						nombre=leer.readLine();
						edad=leer.readLine();
						sexo=leer.readLine();
						fichero.write(nombre);
						fichero.newLine();
						fichero.write(edad);
						fichero.newLine();
						fichero.write(sexo);
						fichero.newLine();
						
						leer.close();
						fichero.close();
					}catch(IOException ioe) {}
				}while(nombre!=null);
			}
		}catch(IOException ioe) {}
	
		}
		
		static void fin() {
			System.out.println("\n\n\n\tFINAL DEL PROGRAMA\n\t==================\n\n");
						}
}