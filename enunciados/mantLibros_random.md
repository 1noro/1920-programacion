# Mantenimiento Libros (Archivos Random)

> Entrega el sábado 20200530 ántes de  las 20:00.

## Libro (Clase)

- Número (int)
- ISBN (13 char justos, ni menos ni mas)
- Título (máx 25 char)
- Autor (max 25 char)
- Edición (int)
- Precio (float)

## Acciones

- Altas
    - Cada vez que se introduce un Libro confirmar antes de guaradar.
    - Confirmar antes de continuar.
- Bajas
- Modificaciones
- Listado

> Cuantos más controles mejor, en todo (p.e., control de páginas).